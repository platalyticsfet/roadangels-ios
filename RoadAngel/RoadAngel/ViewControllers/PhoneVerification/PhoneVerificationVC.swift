//
//  PhoneVerificationVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 22/02/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import UIKit

class PhoneVerificationVC: UIViewController , ResponceCallback {
    
    @IBOutlet weak var phoneIcon: UIImageView!
    @IBOutlet weak var phoneEt: UITextField!
    @IBOutlet weak var submitBtn: UILabel!
    
    let navView = UIView()
    var navController: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneIcon.image = phoneIcon.image!.withRenderingMode(.alwaysTemplate)
        phoneIcon.tintColor = Colors.THEME_BLUE
        applyTapGestures()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        setNavigationsBar()
    }
    
    // MARK: Custome Functions
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "icon_back_arrow"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(backButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 8, y: 4, width: 35, height: 35)
        
        let title = UILabel.init()
        title.text = "Verify Number"
        title.font = UIFont(name: "Calibri", size: 24.0)
        title.textColor = Colors.WHITE
        title.textAlignment = NSTextAlignment.center
        title.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        //navView.addSubview(button)
        navView.addSubview(title)
        self.navigationController!.navigationBar.addSubview(navView)
    }
    
    func applyTapGestures()
    {
        let submitClick = UITapGestureRecognizer(target: self, action: #selector(submitButtonClicked))
        submitBtn.addGestureRecognizer(submitClick)
    }
    
    //MARK: Custom Action
    @objc func backButtonClicked(sender: AnyObject?)
    {
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
    
    @objc func submitButtonClicked()
    {
        if (phoneEt.text?.trimmed.isEmpty)!
        {
            Helper.showFailureDialog(title: "Error", message: "Please Enter Mobile Number", duration: 0.5);
        }
        else
        {
            Helper.showLoadingDialog(message: "Sending SMS...")
            ServerCalls.verifyNumber(type: "\(SharedClass.accountType)", phone: (phoneEt.text?.trimmed)!, responceCallback: self)
        }
    }
    
    // MARK : Response Callback Functions
    func onSuccessResponce(obj: Any, resonseType: Int, apiName: String, status: Int) {
        if resonseType == Constants.API
        {
            Helper.hideLoadingDialog()
            let viewController = CodeVerificationVC(nibName: "CodeVerificationVC",bundle: nil)
            viewController.phoneNumber = (phoneEt.text?.trimmed)!
            navController = UINavigationController(rootViewController: viewController)
            navController?.interactivePopGestureRecognizer?.isEnabled = false
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = navController
            UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
        }
    }
    
    func onFailureResponce(obj: Any, resonseType: Int, apiName: String, status: Int) {
        Helper.hideLoadingDialog()
        if resonseType == Constants.API
        {
            if status == Constants.STATUS_300
            {
                Helper.showFailureDialog(title: "Error", message: "Authentication Failed", duration : 1.0)
            }
            else //if status == Constants.STATUS_0 || status == Constants.STATUS_400 || status == Constants.STATUS_500
            {
                Helper.hideLoadingDialog()
                let error = obj as! String
                print("Failure : \(error)")
                Helper.showFailureDialog(title: "Error", message: error, duration : 1.0)
            }
        }
        else if resonseType == Constants.NET_ISSUE
        {
            Helper.showFailureDialog(title: "Faiure", message: "Net Connection Not Found", duration : 1.0)
        }
    }
}
