//
//  LoginVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 18/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import GoogleSignIn

class LoginVC: UIViewController,GIDSignInUIDelegate , GIDSignInDelegate, ResponceCallback{

    @IBOutlet weak var loginBtn: UILabel!
    @IBOutlet weak var forgetPasswordBtn: UILabel!
    @IBOutlet weak var fbLoginBtn: UIImageView!
    @IBOutlet weak var googleLoginBtn: UIImageView!
    @IBOutlet weak var emailtext: UITextField!
    @IBOutlet weak var passwordtext: UITextField!
    var email : String?
    var password : String?
    var navController: UINavigationController?
    let navView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyGestures()
        configGoogleCredentials()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initializeNavigationsBar()
    }
    
    func configGoogleCredentials()
    {
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().clientID = "260761231393-06pcbrde73u85os0pkbqerhmh2ilsirp.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    }
    
    func initializeNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "icon_back_arrow"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(backButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 8, y: 4, width: 35, height: 35)
        
        let title = UILabel.init()
        title.text = "Login"
        title.font = UIFont(name: "Calibri", size: 24.0)
        title.textColor = Colors.WHITE
        title.textAlignment = NSTextAlignment.center
        title.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        navView.addSubview(button)
        navView.addSubview(title)
        self.navigationController!.navigationBar.addSubview(navView)
        
    }
    
    func applyGestures()
    {
        let loginclick = UITapGestureRecognizer(target: self, action: #selector(loginButtonClicked))
        loginBtn.addGestureRecognizer(loginclick)
        
        let forgetPasswordclick = UITapGestureRecognizer(target: self, action: #selector(forgetPassButtonClicked))
        forgetPasswordBtn.addGestureRecognizer(forgetPasswordclick)
        
        let fbclick = UITapGestureRecognizer(target: self, action: #selector(facebookButtonClicked))
        fbLoginBtn.addGestureRecognizer(fbclick)
        
        let googleclick = UITapGestureRecognizer(target: self, action: #selector(googlePlusButtonClicked))
        googleLoginBtn.addGestureRecognizer(googleclick)
    }
    
    func goToMapController()
    {
        let viewController: UIViewController
        if(SharedClass.accountType.isEqual(to : Constants.END_USER))
        {
            viewController = UserHomeVC()
            navController = UINavigationController(rootViewController: viewController)
            navController?.interactivePopGestureRecognizer?.isEnabled = false
            let mainViewController = UserRootVC()
            mainViewController.rootViewController = navController
            mainViewController.setupNavigationController()
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = mainViewController
            UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
        }
        else{
            viewController = VendorHomeVC()
            navController = UINavigationController(rootViewController: viewController)
            navController?.interactivePopGestureRecognizer?.isEnabled = false
            let mainViewController = VendorRootVC()
            mainViewController.rootViewController = navController
            mainViewController.setupNavigationController()
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = mainViewController
            UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
        }
    }
    
    func goToVerificationController()
    {
        let viewController: UIViewController
        viewController = PhoneVerificationVC()
        navController = UINavigationController(rootViewController: viewController)
        navController?.interactivePopGestureRecognizer?.isEnabled = false
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navController
        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
        
    }
    
    //MARK: Custom Action
    @objc func backButtonClicked(sender: AnyObject?)
    {
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
    
    @objc func loginButtonClicked()
    {
        email = emailtext.text
        password = passwordtext.text
        
        if(email?.isEmpty)!
        {
            Helper.showAlertDialog(viewController: self, title: "Error", message: "Email can not be Empty")
        }
        else if(password?.isEmpty)!
        {
            Helper.showAlertDialog(viewController: self, title: "Error", message: "Password can not be Empty")
        }
        else
        {
            login()
        }
    }
    
    @objc func forgetPassButtonClicked()
    {
        navView.removeFromSuperview()
        let vc = ForgetPasswordVC(nibName: "ForgetPasswordVC",bundle: nil)
        navigationController?.pushViewController(vc,animated: true)
    }
    
    @objc func facebookButtonClicked(sender: AnyObject?)
    {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [ReadPermission.publicProfile], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print("Logged in!")
                let connection = GraphRequestConnection()
                connection.add(MyProfileRequest()) { response, result in
                    switch result {
                    case .success(let response):
                        print("Custom Graph Request Succeeded: \(response)")
                        //print("My facebook id is \(response.dictionaryValue?["id"])")
                        //print("My name is \(response.dictionaryValue?["name"])")
                    case .failed(let error):
                        print("Custom Graph Request Failed: \(error)")
                    }
                }
                connection.start()
            }
        }
    }
    
    @objc func googlePlusButtonClicked(sender: AnyObject?)
    {
        GIDSignIn.sharedInstance().signIn()
    }
    
    // MARK : Google Handler
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,withError error: Error!) {
        if let error = error {
            print("Error : \(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            email = user.profile.email
            password = user.userID
            /*let idToken = user.authentication.idToken
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let iamge = user.profile.imageURL(withDimension: 120)*/
            login()
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("Disconnect")
    }
    
    func login()
    {
        Helper.showLoadingDialog(message: "Please Wait...")
        ServerCalls.signIn(email: email!, password: password!, responceCallback: self)
    }
    
    // MARK : Response Callback Functions
    func onSuccessResponce(obj: Any , resonseType : Int , apiName : String , status : Int) {
        if resonseType == Constants.API
        {
            Helper.hideLoadingDialog()
            do
            {
                let dictionary = obj as! Dictionary<String, AnyObject>
                let data = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
                print("Success : \(data))")
                SharedClass.user = try JSONDecoder().decode(User.self, from: data!)
                if SharedClass.user.verified
                {
                    UserDefault.loginAs()
                    UserDefault.saveEmailAddress(email: email!)
                    UserDefault.savePassword(password: password!)
                    Helper.showSuccessDialog(title: "Success", message: "Login Successfully" , duration : 1.0 )
                    goToMapController()
                }
                else
                {
                    goToVerificationController()
                }
            }
            catch let jsonErr{
                print(jsonErr)
            }
        }
    }
    
    func onFailureResponce(obj: Any , resonseType : Int , apiName : String , status : Int) {
        Helper.hideLoadingDialog()
        if resonseType == Constants.API
        {
            Helper.hideLoadingDialog()
            if status == Constants.STATUS_300
            {
                Helper.showFailureDialog(title: "Error", message: "Authentication Failed", duration : 1.0)
            }
            else //if status == Constants.STATUS_0 || status == Constants.STATUS_400 || status == Constants.STATUS_500
            {
                let error = obj as! String
                print("Failure : \(error)")
                Helper.showFailureDialog(title: "Error", message: error, duration : 1.0)
            }
        }
        else if resonseType == Constants.NET_ISSUE
        {
            Helper.showFailureDialog(title: "Faiure", message: "Net Connection Not Found", duration : 1.0)
        }
    }


}
