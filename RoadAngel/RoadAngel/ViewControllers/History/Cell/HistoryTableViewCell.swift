//
//  HistoryTableViewCell.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 27/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit
import MaterialCard

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var container: MaterialCard!
    @IBOutlet weak var mapImage: UIImageView!
    @IBOutlet weak var dateText: UILabel!
    @IBOutlet weak var priceText: UILabel!
    @IBOutlet weak var fromAddress: UILabel!
    @IBOutlet weak var toAddress: UILabel!
    @IBOutlet weak var vendorName: UILabel!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var serviceIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
