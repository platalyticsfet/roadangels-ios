//
//  HistoryVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 20/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit

extension UIView
{
    func roundCorners(corners:UIRectCorner, radius: CGFloat)
    {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

class HistoryVC: UIViewController , UITableViewDelegate , UITableViewDataSource , ResponceCallback{
    
    var navController: UINavigationController?
    let navView = UIView()
    @IBOutlet weak var tableView: UITableView!
    var tripsList : [TripModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.isLeftViewEnabled = false
        initTableView()
        Helper.showLoadingDialog(message: "Please Wait...")
        ServerCalls.getPreviousTrips(type: SharedClass.accountType.description, id: SharedClass.user._id!, authKey: SharedClass.user.auth_key!, responceCallback: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationsBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initTableView()
    {
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "HistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    }
    
    // MARK: Tableview Delegates
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tripsList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 360.0;//Choose your custom row height
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let myCell = tableView.dequeueReusableCell(withIdentifier: "cell")as! HistoryTableViewCell
        myCell.container.cornerRadius = 10.0
        myCell.mapImage.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
        myCell.mapImage.sd_setImage(with: URL(string: tripsList[indexPath.row].map_image == nil ? "" : tripsList[indexPath.row].map_image), placeholderImage: UIImage(named: "image_placeholder.png"))
        myCell.dateText.text = Helper.getFormatedData_dd_MM_yyyy__hh_mm__AA(dateInMilliseconds: CLong(tripsList[indexPath.row].start_time)!)
        myCell.profilePic.maskCircle(anyImage: myCell.profilePic.image!)
        if SharedClass.accountType == Constants.END_USER
        {
            myCell.vendorName.text = tripsList[indexPath.row].vendor_name
            myCell.profilePic.sd_setImage(with: URL(string: tripsList[indexPath.row].vendor_profilepic == nil ? "" : tripsList[indexPath.row].vendor_profilepic), placeholderImage: UIImage(named: "user_placeholder"))
        }
        else
        {
            myCell.vendorName.text = tripsList[indexPath.row].user_name
            print(tripsList[indexPath.row].user_profilepic)
            myCell.profilePic.sd_setImage(with: URL(string: tripsList[indexPath.row].user_profilepic == nil ? "" : tripsList[indexPath.row].user_profilepic), placeholderImage: UIImage(named: "user_placeholder"))
        }
        myCell.fromAddress.text = tripsList[indexPath.row].destination_address
        myCell.toAddress.text = tripsList[indexPath.row].source_address
        myCell.serviceName.text = tripsList[indexPath.row].service_name
        myCell.priceText.text = tripsList[indexPath.row].service_price + "$"
        myCell.serviceIcon.sd_setImage(with: URL(string: tripsList[indexPath.row].service_icon), placeholderImage: UIImage(named: "user_placeholder"))
        return myCell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        navView.removeFromSuperview()
        let vc = ServiceDetailVC(nibName: "ServiceDetailVC",bundle: nil)
        vc.tripModel = tripsList[indexPath.row]
        navigationController?.pushViewController(vc,animated: true)
    }
    
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "icon_back_arrow"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(backButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 8, y: 4, width: 35, height: 35)
        
        let title = UILabel.init()
        title.text = "History"
        title.font = UIFont(name: "Calibri", size: 24.0)
        title.textColor = Colors.WHITE
        title.textAlignment = NSTextAlignment.center
        title.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        navView.addSubview(button)
        navView.addSubview(title)
        self.navigationController!.navigationBar.addSubview(navView)
        
    }

    //MARK: Custom Action
    
    @objc func backButtonClicked(sender: AnyObject?)
    {
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: Response Callback Delegate
    func onSuccessResponce(obj: Any, resonseType: Int, apiName: String , status : Int) {
        if resonseType == Constants.API
        {
            if apiName == Urls.GET_PREVIOUS_TRIPS
            {
                do
                {
                    let dictionary = obj as! Dictionary<String, AnyObject>
                    //let data = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
                    let array = dictionary["array"] as! NSArray
                    print("Success : \(array))")
                    for trip: Any in array
                    {
                        let json = trip as! Dictionary<String, AnyObject>
                        let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                        let tripModel : TripModel = try JSONDecoder().decode(TripModel.self, from: data!)
                        tripsList.append(tripModel)
                    }
                    DispatchQueue.main.async(execute: {() -> Void in
                        Helper.hideLoadingDialog()
                        if self.tripsList.count > 0
                        {
                            self.tableView.reloadData()
                        }
                    })
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }
        }
    }
    
    func onFailureResponce(obj: Any, resonseType: Int, apiName: String , status : Int) {
        Helper.hideLoadingDialog()
        if resonseType == Constants.API
        {
            if status == Constants.STATUS_300
            {
                Helper.showFailureDialog(title: "Error", message: "Authentication Failed", duration : 1.0)
            }
            else //if status == Constants.STATUS_0 || status == Constants.STATUS_400 || status == Constants.STATUS_500
            {
                let error = obj as! String
                print("Failure : \(error)")
                Helper.showFailureDialog(title: "Error", message: error, duration : 1.0)
            }
        }
        else if resonseType == Constants.NET_ISSUE
        {
            Helper.showFailureDialog(title: "Faiure", message: "Net Connection Not Found", duration : 1.0)
        }
    }
}
