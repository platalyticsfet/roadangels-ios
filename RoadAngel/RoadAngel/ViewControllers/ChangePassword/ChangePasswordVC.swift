//
//  ChangePasswordVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 22/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController , ResponceCallback {

    let navView = UIView()
    var navController: UINavigationController?
    
    @IBOutlet weak var oldPassword: UITextField!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var updateBtn: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.isLeftViewEnabled = false
        setNavigationsBar()
        applyGestures()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func applyGestures()
    {
        let updateclick = UITapGestureRecognizer(target: self, action: #selector(updateButtonClicked))
        updateBtn.addGestureRecognizer(updateclick)
    }
    
    // MARK: Custome Functions
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "icon_back_arrow"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(backButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 8, y: 4, width: 35, height: 35)
        
        let title = UILabel.init()
        title.text = "Change Password"
        title.font = UIFont(name: "Calibri", size: 24.0)
        title.textColor = Colors.WHITE
        title.textAlignment = NSTextAlignment.center
        title.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        navView.addSubview(button)
        navView.addSubview(title)
        self.navigationController!.navigationBar.addSubview(navView)
    }
    
    //MARK: Custom Action
    @objc func backButtonClicked(sender: AnyObject?)
    {
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
    
    @objc func updateButtonClicked()
    {
        var oldPass : String = oldPassword.text!
        var newPass : String = newPassword.text!
        var confirmPass : String = confirmPassword.text!
        if oldPass.isEmpty
        {
            Helper.showFailureDialog(title: "Error", message: "Enter Current Password", duration: 0.5)
        }
        else if newPass.isEmpty
        {
            Helper.showFailureDialog(title: "Error", message: "Enter New Password", duration: 0.5)
        }
        else if confirmPass.isEmpty
        {
            Helper.showFailureDialog(title: "Error", message: "Enter Confirm Password", duration: 0.5)
        }
        else if newPass != confirmPass
        {
            Helper.showFailureDialog(title: "Error", message: "New & Confirm Password should be same", duration: 0.5)
        }
        else
        {
            Helper.showLoadingDialog(message: "Please Wait...")
            ServerCalls.changePassword(oldpassword: oldPass, newpassword: newPass, responceCallback: self)
        }
    }
    
    // MARK: Response Callback Delegates
    func onSuccessResponce(obj: Any, resonseType: Int, apiName: String , status : Int) {
        if resonseType == Constants.API
        {
            if apiName == Urls.CHANGE_PASSWORD
            {
                Helper.hideLoadingDialog()
                DispatchQueue.main.async(execute: {() -> Void in
                    Helper.showSuccessDialog(title: "Success", message: "Password Updated Successfully", duration : 1.0)
                    self.navView.removeFromSuperview()
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }
    }
    
    func onFailureResponce(obj: Any, resonseType: Int, apiName: String , status : Int)
    {
        Helper.hideLoadingDialog()
        if resonseType == Constants.API
        {
            if status == Constants.STATUS_300
            {
                Helper.showFailureDialog(title: "Error", message: "Authentication Failed", duration : 1.0)
            }
            else //if status == Constants.STATUS_0 || status == Constants.STATUS_400 || status == Constants.STATUS_500
            {
                let error = obj as! String
                print("Failure : \(error)")
                Helper.showFailureDialog(title: "Error", message: error, duration : 1.0)
            }
        }
        else if resonseType == Constants.NET_ISSUE
        {
            Helper.showFailureDialog(title: "Faiure", message: "Net Connection Not Found", duration : 1.0)
        }
    }
    
}
