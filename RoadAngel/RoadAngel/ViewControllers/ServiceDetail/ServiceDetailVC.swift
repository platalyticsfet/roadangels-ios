//
//  ServiceDetailVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 20/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit
import Cosmos

class ServiceDetailVC: UIViewController {

    var navController: UINavigationController?
    let navView = UIView()
    var tripModel : TripModel!
    @IBOutlet weak var mapImage: UIImageView!
    @IBOutlet weak var dateText: UILabel!
    @IBOutlet weak var servicePrice: UILabel!
    @IBOutlet weak var fromAddress: UILabel!
    @IBOutlet weak var toAddress: UILabel!
    @IBOutlet weak var serviceIcon: UIImageView!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var ratingBar: CosmosView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.isLeftViewEnabled = false
        setNavigationsBar()
        populateData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "icon_back_arrow"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(backButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 8, y: 4, width: 35, height: 35)
        
        let title = UILabel.init()
        title.text = "Service Detail"
        title.font = UIFont(name: "Calibri", size: 24.0)
        title.textColor = Colors.WHITE
        title.textAlignment = NSTextAlignment.center
        title.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        navView.addSubview(button)
        navView.addSubview(title)
        self.navigationController!.navigationBar.addSubview(navView)
        
    }

    func populateData()
    {
        profilePic.maskCircle(anyImage: profilePic.image!)
        mapImage.sd_setImage(with: URL(string: tripModel.map_image == nil ? "" : tripModel.map_image), placeholderImage: UIImage(named: "image_placeholder.png"))
        dateText.text = Helper.getFormatedData_dd_MM_yyyy__hh_mm__AA(dateInMilliseconds: CLong(tripModel.start_time)!)
        if SharedClass.accountType == Constants.END_USER
        {
            profilePic.sd_setImage(with: URL(string: tripModel.vendor_profilepic), placeholderImage: UIImage(named: "user_placeholder"))
            if tripModel.vendor_rating==nil || tripModel.vendor_rating==""
            {
                username.text = "Your Service by "+tripModel.vendor_name
                ratingBar.isHidden = true
            }
            else
            {
                username.text = "You rate "+tripModel.vendor_name
                ratingBar.rating = Double(tripModel.vendor_rating)!
            }
        }
        else
        {
            profilePic.sd_setImage(with: URL(string: tripModel.user_profilepic), placeholderImage: UIImage(named: "user_placeholder"))
            if tripModel.vendor_rating==nil || tripModel.vendor_rating==""
            {
                username.text = "Your Service by "+tripModel.user_name
                ratingBar.isHidden = true
            }
            else
            {
                username.text = "You rate "+tripModel.user_name
                ratingBar.rating = Double(tripModel.user_rating)!
            }
        }
        fromAddress.text = tripModel.destination_address
        toAddress.text = tripModel.source_address
        serviceName.text = tripModel.service_name
        servicePrice.text = tripModel.service_price + "$"
        serviceIcon.sd_setImage(with: URL(string: tripModel.service_icon), placeholderImage: UIImage(named: "user_placeholder"))
    }
    
    //MARK: Custom Action
    
    @objc func backButtonClicked(sender: AnyObject?)
    {
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
}
