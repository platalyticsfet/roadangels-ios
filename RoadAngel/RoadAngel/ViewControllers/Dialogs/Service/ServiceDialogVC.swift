//
//  ServiceDialogVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 03/01/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import UIKit
import MaterialCard

class ServiceDialogVC: UIViewController {

    var selectedIndex : Int!
    var nearEstimateTime : String!
    var farEstimateTime : String!
    @IBOutlet weak var alertView: MaterialCard!
    @IBOutlet weak var serviceIcon: UIImageView!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var serviceDetail: UILabel!
    @IBOutlet weak var clockIcon: UIImageView!
    @IBOutlet weak var moneyIcon: UIImageView!
    @IBOutlet weak var timeText: UILabel!
    @IBOutlet weak var fareText: UILabel!
    @IBOutlet weak var cancelBtn: UILabel!
    @IBOutlet weak var confirmBtn: UILabel!
    @IBOutlet weak var descriptionTF: UITextView!
    var delegate: ServiceDialogDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.BLACK.withAlphaComponent(0.4)
        
        alertView.cornerRadius = 5.0
        clockIcon.image = clockIcon.image!.withRenderingMode(.alwaysTemplate)
        clockIcon.tintColor = Colors.THEME_BLUE
        applyTapGestures()
        populateData()
    }
    
    func populateData()
    {
        let serviceModel : ServicesModel = SharedClass.allServices[selectedIndex]
        serviceIcon.sd_setImage(with: URL(string: (serviceModel.service_image)!), placeholderImage: UIImage(named: "image_placeholder.png"))
        serviceName.text = serviceModel.service_name
        serviceDetail.text = serviceModel.service_detail
        timeText.text = "\(nearEstimateTime!) - \(farEstimateTime!)"
        fareText.text = "\(serviceModel.service_price!)$"
        
        serviceIcon.image = serviceIcon.image!.withRenderingMode(.alwaysTemplate)
        serviceIcon.tintColor = Colors.THEME_BLUE
        
        clockIcon.image = clockIcon.image!.withRenderingMode(.alwaysTemplate)
        clockIcon.tintColor = Colors.THEME_BLUE
        
        descriptionTF.placeholderText = "Enter More Description"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func applyTapGestures()
    {
        let confirmClick = UITapGestureRecognizer(target: self, action: #selector(confirmButtonClicked))
        let cancleClick = UITapGestureRecognizer(target: self, action: #selector(cancleButtonClicked))
        
        confirmBtn.addGestureRecognizer(confirmClick)
        cancelBtn.addGestureRecognizer(cancleClick)
    }
    
    @objc func confirmButtonClicked()
    {
        if descriptionTF.text.isEmpty
        {
            delegate?.confirmButtonTapped(note: "")
        }
        else
        {
            delegate?.confirmButtonTapped(note: descriptionTF.text)
        }
    }
    
    @objc func cancleButtonClicked()
    {
        delegate?.cancelButtonTapped()
    }
}
