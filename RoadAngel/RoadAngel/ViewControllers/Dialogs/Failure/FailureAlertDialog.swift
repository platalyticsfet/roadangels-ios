//
//  FailureAlertDialog.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 04/01/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import UIKit

class FailureAlertDialog: UIViewController {

    @IBOutlet weak var titleIcon: UIImageView!
    @IBOutlet weak var message: UILabel!
    var delegate: AlertDialogDelegate?
    var messageText : String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.BLACK.withAlphaComponent(0.4)
        titleIcon.image = titleIcon.image!.withRenderingMode(.alwaysTemplate)
        titleIcon.tintColor = Colors.WHITE
        
        if messageText != ""
        {
            message.text = messageText
        }
    }
    
    @IBAction func okClick(_ sender: Any) {
        delegate?.okButtonTapped(message: "")
    }

}
