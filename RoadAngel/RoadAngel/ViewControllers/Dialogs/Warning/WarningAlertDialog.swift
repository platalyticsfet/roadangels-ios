//
//  WarningAlertDialog.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 04/01/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import UIKit

class WarningAlertDialog: UIViewController {

    @IBOutlet weak var iconColor: UIImageView!
    var delegate: AlertDialogDelegate?
    var messageText : String! = ""
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var header: UILabel!
    var titleText : String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = Colors.BLACK.withAlphaComponent(0.4)
        iconColor.image = iconColor.image!.withRenderingMode(.alwaysTemplate)
        iconColor.tintColor = Colors.WHITE
        if titleText != ""
        {
            header.text = titleText
        }
        if messageText != ""
        {
            message.text = messageText
        }
    }
    @IBAction func okClick(_ sender: Any) {
        delegate?.okButtonTapped(message: "")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
