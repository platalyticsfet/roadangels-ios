//
//  ResetPsswordVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 22/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit

class ResetPsswordVC: UIViewController , ResponceCallback {

    var navController: UINavigationController?
    let navView = UIView()
    var email : String!
    @IBOutlet weak var passIcon1: UIImageView!
    @IBOutlet weak var passIcon2: UIImageView!
    @IBOutlet weak var newPassText: UITextField!
    @IBOutlet weak var confirmPassText: UITextField!
    @IBOutlet weak var updateBtn: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationsBar()
        applyGestures()
        modifyUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func modifyUI()
    {
        passIcon1.image = passIcon1.image?.withRenderingMode(.alwaysTemplate)
        passIcon1.tintColor = Colors.THEME_BLUE
        passIcon2.image = passIcon2.image?.withRenderingMode(.alwaysTemplate)
        passIcon2.tintColor = Colors.THEME_BLUE
    }
    
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "icon_back_arrow"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(backButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 8, y: 4, width: 35, height: 35)
        
        let title = UILabel.init()
        title.text = "Reset Password"
        title.font = UIFont(name: "Calibri", size: 24.0)
        title.textColor = Colors.WHITE
        title.textAlignment = NSTextAlignment.center
        title.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        navView.addSubview(button)
        navView.addSubview(title)
        self.navigationController!.navigationBar.addSubview(navView)
        
    }
    
    func applyGestures()
    {
        let updateclick = UITapGestureRecognizer(target: self, action: #selector(updateButtonClicked))
        updateBtn.addGestureRecognizer(updateclick)
    }
    
    //MARK: Custom Action
    
    @objc func backButtonClicked(sender: AnyObject?)
    {
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
    
    @objc func updateButtonClicked()
    {
        if (newPassText.text?.isEmpty)!
        {
            Helper.showFailureDialog(title: "Error", message: "Enter New Password", duration: 1.0)
        }
        else if (confirmPassText.text?.isEmpty)!
        {
            Helper.showFailureDialog(title: "Error", message: "Enter Confirm Password", duration: 1.0)
        }
        else if newPassText.text != confirmPassText.text
        {
            Helper.showFailureDialog(title: "Error", message: "New & Confirm password should matched", duration: 1.0)
        }
        else
        {
            Helper.showLoadingDialog(message: "Please Wait...")
            ServerCalls.updatePassword(email: email, type: SharedClass.accountType.stringValue, password: confirmPassText.text!, responceCallback: self)
        }
    }
    
    // MARK: Response Callback Delegates
    func onSuccessResponce(obj: Any, resonseType: Int, apiName: String , status : Int) {
        Helper.hideLoadingDialog()
        Helper.showSuccessDialog(title: "Code Sent", message: "Code Update Successfully", duration : 2.0)
        
        var nc: UINavigationController?
        var rootVC: UIViewController!
        rootVC = UserTypeVC(nibName: "UserTypeVC", bundle: Bundle.main)
        nc = UINavigationController(rootViewController: rootVC)
        nc?.interactivePopGestureRecognizer?.isEnabled = false
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = nc
        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    func onFailureResponce(obj: Any, resonseType: Int, apiName: String , status : Int) {
        Helper.hideLoadingDialog()
        if resonseType == Constants.API
        {
            if status == Constants.STATUS_300
            {
                Helper.showFailureDialog(title: "Error", message: "Authentication Failed", duration : 1.0)
            }
            else //if status == Constants.STATUS_0 || status == Constants.STATUS_400 || status == Constants.STATUS_500
            {
                let error = obj as! String
                print("Failure : \(error)")
                Helper.showFailureDialog(title: "Error", message: error, duration : 1.0)
            }
        }
        else if resonseType == Constants.NET_ISSUE
        {
            Helper.showFailureDialog(title: "Faiure", message: "Net Connection Not Found", duration : 1.0)
        }
    }

}
