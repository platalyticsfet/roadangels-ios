//
//  ReviewServiceVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 20/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit
import MaterialCard
import Cosmos

protocol ReviewServiceProtocol {
    func reviewServiceDone()
}

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}

class ReviewServiceVC: UIViewController , UITextViewDelegate , ResponceCallback{

    var navController: UINavigationController?
    let navView = UIView()
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var userInfoMCView: MaterialCard!
    @IBOutlet weak var addressesMCView: MaterialCard!
    @IBOutlet weak var hollowCircle: UIImageView!
    @IBOutlet weak var pin: UIImageView!
    @IBOutlet weak var mapImageMCView: MaterialCard!
    @IBOutlet weak var ratingMCView: MaterialCard!
    var tripModel : TripModel? = nil
    var delegate:ReviewServiceProtocol?
    
    @IBOutlet weak var servicePrice: UILabel!
    @IBOutlet weak var serviceIcon: UIImageView!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var serviceDate: UILabel!
    @IBOutlet weak var userProfilePic: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var fromAddress: UILabel!
    @IBOutlet weak var toAddress: UILabel!
    @IBOutlet weak var mapImage: UIImageView!
    @IBOutlet weak var ratingBar: CosmosView!
    @IBOutlet weak var reviewTV: UITextView!
    @IBOutlet weak var rateNowBtn: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.isLeftViewEnabled = false
        setNavigationsBar()
        ModifyUI()
        reviewTV.text = "Please write your reviews"
        reviewTV.textColor = Colors.LIGHT_GRAY
        reviewTV.delegate = self
        populateData()
        if SharedClass.accountType == Constants.VENDOR
        {
            updateServiceMapUrl()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "icon_back_arrow"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(backButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 8, y: 4, width: 35, height: 35)
        
        let title = UILabel.init()
        title.text = "Review Service"
        title.font = UIFont(name: "Calibri", size: 24.0)
        title.textColor = Colors.WHITE
        title.textAlignment = NSTextAlignment.center
        title.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        navView.addSubview(button)
        navView.addSubview(title)
        self.navigationController!.navigationBar.addSubview(navView)
        
    }
    
    func ModifyUI()
    {
        headerView.backgroundColor = Colors.THEME_BLUE
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        if(screenWidth==320.0 && screenHeight==568.0)
        {
            scrollView.contentInset=UIEdgeInsets(top:0.0,left:0.0,bottom:140.0,right:0.0);
        }
        else if(screenWidth==375.0 && screenHeight==667.0)
        {
            scrollView.contentInset=UIEdgeInsets(top:0.0,left:0.0,bottom:40.0,right:0.0);
        }
        userInfoMCView.cornerRadius = 5.0
        addressesMCView.cornerRadius = 5.0
        mapImageMCView.cornerRadius = 5.0
        ratingMCView.cornerRadius = 5.0
        
        hollowCircle.image = hollowCircle.image!.withRenderingMode(.alwaysTemplate)
        pin.image = pin.image!.withRenderingMode(.alwaysTemplate)
        hollowCircle.tintColor = Colors.LIGHT_GREEN
        pin.tintColor = Colors.THEME_BLUE
        
        userProfilePic.maskCircle(anyImage: userProfilePic.image!)
    }
    
    //MARK: Custom Action
    
    @objc func backButtonClicked(sender: AnyObject?)
    {
        delegate?.reviewServiceDone()
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Please write your reviews" {
            textView.text = nil
            textView.textColor = Colors.THEME_BLUE
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Please write your reviews"
            textView.textColor = Colors.LIGHT_GRAY
        }
        else{
            textView.textColor = Colors.THEME_BLUE
        }
    }
    
    func populateData()
    {
        servicePrice.text = (tripModel?.service_price)!+"$"
        serviceIcon.sd_setImage(with: URL(string: (tripModel?.service_icon)!), placeholderImage: UIImage(named: "image_placeholder.png"))
        serviceIcon.image = serviceIcon.image!.withRenderingMode(.alwaysTemplate)
        serviceIcon.tintColor = Colors.WHITE
        
        serviceName.text = tripModel?.service_name!
        let time : String = Helper.getFormatedData_MMMM_DD_YYYY_HH_MM_A(dateInMilliseconds: CLong(tripModel!.end_time)!)
        serviceDate.text = time
        if SharedClass.accountType == Constants.END_USER
        {
            userProfilePic.sd_setImage(with: URL(string: (tripModel?.vendor_profilepic!)!), placeholderImage: UIImage(named: "user_placeholder.png"))
            userName.text = tripModel?.vendor_name
        }
        else
        {
            userProfilePic.sd_setImage(with: URL(string: (tripModel?.user_profilepic)!), placeholderImage: UIImage(named: "user_placeholder.png"))
            userName.text = tripModel?.user_name
        }
        fromAddress.text = tripModel?.destination_address
        toAddress.text = tripModel?.source_address
        mapImage.sd_setImage(with: URL(string: (tripModel?.map_image)!), placeholderImage: UIImage(named: "image_placeholder.png"))
    }
    
    @IBAction func rateNowBtn(_ sender: Any) {
        if reviewTV.text.isEmpty || reviewTV.text == "Please write your reviews"
        {
            Helper.showYesNoDialog(viewController: self, title: "Alert", message: "Want to proceed with No Review", responseCallback: self)
        }
        else
        {
            setRating(rating: ratingBar.rating.description, review: reviewTV.text)
        }
    }
    
    func setRating(rating:String , review:String)
    {
        ServerCalls.setRating(model: tripModel!, rating: rating, review: review, responceCallback: self)
    }
    
    func updateServiceMapUrl()
    {
        ServerCalls.updateService(id: (tripModel?._id)!, url: (tripModel?.map_image)!, responceCallback: self)
    }
    
    // MARK: Reponse Callback Delegates
    func onSuccessResponce(obj: Any, resonseType: Int, apiName: String , status : Int) {
        if resonseType == Constants.API
        {
            if apiName == Urls.UPDATE_SERVICE
            {
                DispatchQueue.main.async(execute: {() -> Void in
                    Helper.showToast(controller: self, message: "Url Updated")
                        })
            }
            else if apiName == Urls.SET_RATING
            {
                DispatchQueue.main.async(execute: {() -> Void in
                    Helper.showSuccessDialog(title: "Done", message: "Review Submitted", duration : 1.0)
                            self.ratingMCView.isHidden = true
                            self.rateNowBtn.isHidden = true
                            
                        })
            }
        }
        else if resonseType == Constants.DIALOG
        {
            setRating(rating: ratingBar.rating.description, review: "")
        }
    }
    
    func onFailureResponce(obj: Any, resonseType: Int, apiName: String , status : Int) {
        Helper.hideLoadingDialog()
        if resonseType == Constants.API
        {
            if status == Constants.STATUS_300
            {
                Helper.showFailureDialog(title: "Error", message: "Authentication Failed", duration : 1.0)
            }
            else //if status == Constants.STATUS_0 || status == Constants.STATUS_400 || status == Constants.STATUS_500
            {
                let error = obj as! String
                print("Failure : \(error)")
                Helper.showFailureDialog(title: "Error", message: error, duration : 1.0)
            }
        }
        else if resonseType == Constants.NET_ISSUE
        {
            Helper.showFailureDialog(title: "Faiure", message: "Net Connection Not Found", duration : 1.0)
        }
    }
}
