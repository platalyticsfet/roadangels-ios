//
//  SettingVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 20/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit
import Stripe

class SettingVC: UIViewController , UITableViewDelegate , UITableViewDataSource , ResponceCallback {
    
    @IBOutlet weak var backBtn: UIView!
    @IBOutlet weak var tableView: UITableView!
    var titles = ["Change Password","Payment Method","Logout"]
    var icons = ["password_icon","earning","logout"]
    let navView = UIView()
    var navController: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.isLeftViewEnabled = false
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "SettingTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        if SharedClass.accountType == Constants.END_USER
        {
            titles.remove(at: 2)
            icons.remove(at: 2)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationsBar()
    }
    
    // MARK: Tableview Delegates
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (titles.count)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70.0;//Choose your custom row height
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let myCell = tableView.dequeueReusableCell(withIdentifier: "cell")as! SettingTableViewCell
        myCell.title.text = titles[indexPath.row]
        myCell.icon.tintColor = Colors.THEME_BLUE
        myCell.icon.image = UIImage(named: icons[indexPath.row])!.withRenderingMode(.alwaysTemplate)
        return myCell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        navView.removeFromSuperview()
        if(indexPath.row==0)
        {
            let vc = ChangePasswordVC(nibName: "ChangePasswordVC",bundle: nil)
            navigationController?.pushViewController(vc,animated: true)
        }
        else if(indexPath.row==1)
        {
            let vc = PaymentMethodVC(nibName: "PaymentMethodVC",bundle: nil)
            navigationController?.pushViewController(vc,animated: true)
        }
        else if(indexPath.row==2)
        {
            logout()
        }
    }
    
    // MARK: Custome Functions
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "icon_back_arrow"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(backButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 8, y: 4, width: 35, height: 35)
        
        let title = UILabel.init()
        title.text = "Setting"
        title.font = UIFont(name: "Calibri", size: 24.0)
        title.textColor = Colors.WHITE
        title.textAlignment = NSTextAlignment.center
        title.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        navView.addSubview(button)
        navView.addSubview(title)
        self.navigationController!.navigationBar.addSubview(navView)
    }
    
    func logout()
    {
        let refreshAlert = UIAlertController(title: "Logout", message: "You really want to logout?", preferredStyle: UIAlertControllerStyle.alert)
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            
            ServerCalls.logout(type: "\(SharedClass.accountType.stringValue)", id: "\(SharedClass.user._id!)", responceCallback: self)
            
        }))
        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
    
    //MARK: Custom Action
    @objc func backButtonClicked(sender: AnyObject?)
    {
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: Response Call backs
    func onSuccessResponce(obj: Any, resonseType: Int, apiName: String, status: Int) {
        if resonseType == Constants.API
        {
            if apiName == Urls.LOGOUT
            {
                SharedClass.clearAll()
                UserDefault.clearAll()
                var nc: UINavigationController?
                var rootVC: UIViewController!
                rootVC = UserTypeVC(nibName: "UserTypeVC", bundle: Bundle.main)
                nc = UINavigationController(rootViewController: rootVC)
                nc?.interactivePopGestureRecognizer?.isEnabled = false
                let window = UIApplication.shared.delegate!.window!!
                window.rootViewController = nc
                UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
            }
        }
    }
    
    func onFailureResponce(obj: Any, resonseType: Int, apiName: String, status: Int) {
        Helper.hideLoadingDialog()
        if resonseType == Constants.API
        {
            if status == Constants.STATUS_300
            {
                Helper.showFailureDialog(title: "Error", message: "Authentication Failed", duration : 1.0)
            }
            else 
            {
                let error = obj as! String
                print("Failure : \(error)")
                Helper.showFailureDialog(title: "Error", message: error, duration : 1.0)
            }
            
        }
        else if resonseType == Constants.NET_ISSUE
        {
            Helper.showFailureDialog(title: "Faiure", message: "Net Connection Not Found", duration : 1.0)
        }
    }
}
