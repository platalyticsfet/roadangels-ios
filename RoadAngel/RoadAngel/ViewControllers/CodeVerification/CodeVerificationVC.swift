//
//  CodeVerificationVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 22/02/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import UIKit

class CodeVerificationVC: UIViewController , ResponceCallback {

    let navView = UIView()
    var navController: UINavigationController?
    @IBOutlet weak var codeEt: UITextField!
    @IBOutlet weak var submitBtn: UILabel!
    var phoneNumber : String! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyTapGestures()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationsBar()
    }
    
    // MARK: Custome Functions
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "icon_back_arrow"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(backButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 8, y: 4, width: 35, height: 35)
        
        let title = UILabel.init()
        title.text = "Verify Code"
        title.font = UIFont(name: "Calibri", size: 24.0)
        title.textColor = Colors.WHITE
        title.textAlignment = NSTextAlignment.center
        title.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        //navView.addSubview(button)
        navView.addSubview(title)
        self.navigationController!.navigationBar.addSubview(navView)
    }
    
    func applyTapGestures()
    {
        let submitClick = UITapGestureRecognizer(target: self, action: #selector(submitButtonClicked))
        submitBtn.addGestureRecognizer(submitClick)
    }
    
    //MARK: Custom Action
    @objc func backButtonClicked(sender: AnyObject?)
    {
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
    
    func goToNextController()
    {
        let viewController: UIViewController
        if(SharedClass.accountType.isEqual(to : Constants.END_USER))
        {
            viewController = UserHomeVC()
            navController = UINavigationController(rootViewController: viewController)
            navController?.interactivePopGestureRecognizer?.isEnabled = false
            let mainViewController = UserRootVC()
            mainViewController.rootViewController = navController
            mainViewController.setupNavigationController()
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = mainViewController
            UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
        }
        else{
            viewController = VendorHomeVC()
            navController = UINavigationController(rootViewController: viewController)
            navController?.interactivePopGestureRecognizer?.isEnabled = false
            let mainViewController = VendorRootVC()
            mainViewController.rootViewController = navController
            mainViewController.setupNavigationController()
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = mainViewController
            UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
        }
    }
    
    @objc func submitButtonClicked()
    {
        if (codeEt.text?.trimmed.isEmpty)!
        {
            Helper.showFailureDialog(title: "Error", message: "Please Enter Code", duration: 0.5);
        }
        else
        {
            Helper.showLoadingDialog(message: "Verifying Code...")
            ServerCalls.verifyCode(type: "\(SharedClass.accountType)", phone: phoneNumber, code: (codeEt.text?.trimmed)!, responceCallback: self)
        }
    }
    
    // MARK : Response Callback Functions
    func onSuccessResponce(obj: Any, resonseType: Int, apiName: String, status: Int) {
        if resonseType == Constants.API
        {
            Helper.hideLoadingDialog()
            do
            {
                // Converting Dictionary to Data
                let json = obj as! Dictionary<String, AnyObject>
                let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                SharedClass.user = try JSONDecoder().decode(User.self, from: data!)
                UserDefault.loginAs()
                UserDefault.saveEmailAddress(email: SharedClass.user.email)
                Helper.showSuccessDialog(title: "Success", message: "Register Successfully", duration : 1.0)
                goToNextController()
            }
            catch let jsonErr{
                print(jsonErr)
            }
        }
    }
    
    func onFailureResponce(obj: Any, resonseType: Int, apiName: String, status: Int) {
        Helper.hideLoadingDialog()
        if resonseType == Constants.API
        {
            if status == Constants.STATUS_300
            {
                Helper.showFailureDialog(title: "Error", message: "Authentication Failed", duration : 1.0)
            }
            else //if status == Constants.STATUS_0 || status == Constants.STATUS_400 || status == Constants.STATUS_500
            {
                Helper.hideLoadingDialog()
                let error = obj as! String
                print("Failure : \(error)")
                Helper.showFailureDialog(title: "Error", message: error, duration : 1.0)
            }
        }
        else if resonseType == Constants.NET_ISSUE
        {
            Helper.showFailureDialog(title: "Faiure", message: "Net Connection Not Found", duration : 1.0)
        }
    }

}
