//
//  ViewController.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 20/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit
import LGSideMenuController.LGSideMenuController
import LGSideMenuController.UIViewController_LGSideMenuController

class UserRootVC: LGSideMenuController {

    func setupNavigationController()
    {
        leftViewController = UserNavDrawerVC()
        leftViewWidth = UIScreen.main.bounds.width * 0.75
        leftViewBackgroundColor = Colors.LIGHT_GRAY_2
        leftViewPresentationStyle = .scaleFromBig
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
