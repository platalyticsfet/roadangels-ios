//
//  NavDrawerVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 21/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit
import SDWebImage

class UserNavDrawerVC: UIViewController {

    @IBOutlet weak var paymentBtn: UIView!
    @IBOutlet weak var historyBtn: UIView!
    @IBOutlet weak var settingBtn: UIView!
    @IBOutlet weak var helpBtn: UIView!
    @IBOutlet weak var logoutBtn: UIView!
    
    @IBOutlet weak var paymentIcon: UIImageView!
    @IBOutlet weak var historyIcon: UIImageView!
    @IBOutlet weak var settingIcon: UIImageView!
    @IBOutlet weak var helpIcon: UIImageView!
    @IBOutlet weak var logoutIcon: UIImageView!
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var ratingText: UILabel!
    @IBOutlet weak var ratingStar: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modifyObjects()
        applyTapGestures()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setData()
    }
    
    func setData()
    {
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
        if SharedClass.user != nil
        {
            if SharedClass.user?.profile_pic != nil {
                profilePic.sd_setImage(with: URL(string: (SharedClass.user?.profile_pic)!), placeholderImage: UIImage(named: "user_placeholder.png"))
            }
            username.text = (SharedClass.user?.first_name)! + " " + (SharedClass.user?.last_name)!
            ratingText.text = SharedClass.user?.rating
            if SharedClass.user?.rating==nil || SharedClass.user?.rating == ""
            {
                ratingStar.alpha = 0.0
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func modifyObjects()
    {
        paymentIcon.image = paymentIcon.image?.withRenderingMode(.alwaysTemplate)
        historyIcon.image = historyIcon.image?.withRenderingMode(.alwaysTemplate)
        settingIcon.image = settingIcon.image?.withRenderingMode(.alwaysTemplate)
        helpIcon.image = helpIcon.image?.withRenderingMode(.alwaysTemplate)
        ratingStar.image = ratingStar.image?.withRenderingMode(.alwaysTemplate)
        logoutIcon.image = logoutIcon.image?.withRenderingMode(.alwaysTemplate)
        
        paymentIcon.tintColor = Colors.BLACK
        historyIcon.tintColor = Colors.BLACK
        settingIcon.tintColor = Colors.BLACK
        helpIcon.tintColor = Colors.BLACK
        ratingStar.tintColor = Colors.BLACK
        logoutIcon.tintColor = Colors.BLACK
        profilePic.maskCircle(anyImage: profilePic.image!)
    }

    func applyTapGestures()
    {
        let profileClick = UITapGestureRecognizer(target: self, action: #selector(profileButtonClicked))
        let paymentClick = UITapGestureRecognizer(target: self, action: #selector(paymentButtonClicked))
        let historyClick = UITapGestureRecognizer(target: self, action: #selector(historyButtonClicked))
        let settingClick = UITapGestureRecognizer(target: self, action: #selector(settingButtonClicked))
        let helpClick = UITapGestureRecognizer(target: self, action: #selector(helpButtonClicked))
        let logoutClick = UITapGestureRecognizer(target: self, action: #selector(logoutButtonClicked))
        
        profilePic.addGestureRecognizer(profileClick)
        paymentBtn.addGestureRecognizer(paymentClick)
        historyBtn.addGestureRecognizer(historyClick)
        settingBtn.addGestureRecognizer(settingClick)
        helpBtn.addGestureRecognizer(helpClick)
        logoutBtn.addGestureRecognizer(logoutClick)
    
    }
    
    @objc func profileButtonClicked()
    {
        print("Home Clicked")
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as! UINavigationController
        let rootViewController = navigationController.topViewController as! UserHomeVC
        rootViewController.goToProfile()
    }
    
    @objc func paymentButtonClicked()
    {
        print("Payment Clicked")
    }
    
    @objc func historyButtonClicked()
    {
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as! UINavigationController
        let rootViewController = navigationController.topViewController as! UserHomeVC
        rootViewController.goToHistory()
    }
    
    @objc func settingButtonClicked()
    {
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as! UINavigationController
        let rootViewController = navigationController.topViewController as! UserHomeVC
        rootViewController.goToSetting()
    }
    
    @objc func helpButtonClicked()
    {
        print("Help Clicked")
    }
    
    @objc func logoutButtonClicked()
    {
        print("Logout Clicked")
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as! UINavigationController
        let rootViewController = navigationController.topViewController as! UserHomeVC
        rootViewController.logout()
    }
}
