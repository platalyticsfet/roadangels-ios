//
//  ServicesVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 20/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit

protocol ServiceProtocol {
    func selectedService(selectedIndex: Int,note : String)
}

class ServicesVC: UIViewController  , UITableViewDelegate , UITableViewDataSource ,  ResponceCallback , ServiceDialogDelegate{

    
    var rootVC: UIViewController!
    var navController: UINavigationController?
    @IBOutlet weak var tableView: UITableView!
    let navView = UIView()
    
    var delegate:ServiceProtocol?
    
    var nearestVendor : NearByVendorModel!
    var farestVendor : NearByVendorModel!
    var nearEstimateTime : String! = nil
    var farEstimateTime : String! = nil
    var selectedIndex : Int! = -1
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setNavigationsBar()
        sideMenuController?.isLeftViewEnabled = false
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "ServiceTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        let origion : String = "\(SharedClass.latitude),\(SharedClass.longitude)"
        let destination : String = "\(nearestVendor!.latitude!),\(nearestVendor!.longitude!)"
        Helper.showLoadingDialog(message: "Please Wait...")
        getEstimatedTime(origion: origion,destination: destination)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Custom Function
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "icon_back_arrow"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(backButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 8, y: 4, width: 35, height: 35)
        
        let title = UILabel.init()
        title.text = "Services"
        title.font = UIFont(name: "Calibri", size: 24.0)
        title.textColor = Colors.WHITE
        title.textAlignment = NSTextAlignment.center
        title.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        navView.addSubview(button)
        navView.addSubview(title)
        self.navigationController!.navigationBar.addSubview(navView)
        
    }
    
    func ShowConfirmDialog()
    {
        let serviceModel : ServicesModel = SharedClass.allServices[selectedIndex]
        if nearEstimateTime == nil
        {
            nearEstimateTime = "N/A";
        }
        if farEstimateTime == nil
        {
            farEstimateTime = "N/A";
        }
        let customAlert = ServiceDialogVC()
        customAlert.selectedIndex = selectedIndex
        customAlert.nearEstimateTime = nearEstimateTime
        customAlert.farEstimateTime = farEstimateTime
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        self.present(customAlert, animated: true, completion: nil)
    }
    
    //MARK: Custom Action
    @IBAction func backButtonClicked(sender: AnyObject?)
    {
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: Tableview Delegates
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return SharedClass.allServices.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 77.0;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        selectedIndex = indexPath.row
        ShowConfirmDialog()
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let myCell = tableView.dequeueReusableCell(withIdentifier: "cell")as! ServiceTableViewCell
        myCell.serviceIcon.sd_setImage(with: URL(string: (SharedClass.allServices[indexPath.row].service_image)!), placeholderImage: UIImage(named: "image_placeholder.png"))
        myCell.serviceName.text = SharedClass.allServices[indexPath.row].service_name
        myCell.serviceDetail.text = SharedClass.allServices[indexPath.row].service_detail
        return myCell
    }

    // MARK: API Calling Functions
    
    func getEstimatedTime(origion:String , destination:String)
    {
        ServerCalls.getTimeDuration(origion: origion, destination: destination, responceCallback: self)
    }
    
    func getAllServices()
    {
        ServerCalls.getAllServices(responceCallback: self)
    }
    
    // MARK : Response Callback Functions
    
    func onSuccessResponce(obj: Any, resonseType: Int , apiName : String , status : Int) {
        if resonseType == Constants.API
        {
            if apiName == Urls.GET_ALL_SERVICES
            {
                Helper.hideLoadingDialog()
                do
                {
                    let dictionary = obj as! Dictionary<String, AnyObject>
                    let data = dictionary["array"] as! NSArray
                    print("Services Success : \(data))")
                    for service: Any in data
                    {
                        let json = service as! Dictionary<String, AnyObject>
                        let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                        let servicesModel : ServicesModel = try JSONDecoder().decode(ServicesModel.self, from: data!)
                        SharedClass.allServices.append(servicesModel)
                        self.tableView.reloadData()
                    }
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }
            else if apiName == Urls.GET_TIME_DURATION
            {
                do
                {
                    let json = obj as! Dictionary<String, AnyObject>
                    let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                    print("Success : \(data))")
                    let googleDistanceModel : GoogleDistanceModel = try JSONDecoder().decode(GoogleDistanceModel.self, from: data!)
                    if googleDistanceModel.status == "OK"
                    {
                        if nearEstimateTime == nil
                        {
                            nearEstimateTime = (googleDistanceModel.rows[0].elements[0].duration!.text)!
                            print(nearEstimateTime!)
                            nearEstimateTime = nearEstimateTime?.replacingOccurrences(of: "hours", with: "h", options: .literal, range: nil)
                            nearEstimateTime = nearEstimateTime?.replacingOccurrences(of: "hour", with: "h", options: .literal, range: nil)
                            /*if let idx = nearEstimateTime?.characters.index(of:"h")
                            {
                                let hIndex = nearEstimateTime?.index(idx, offsetBy: -1)
                                nearEstimateTime?.remove(at: hIndex!)
                            }*/
                            nearEstimateTime = nearEstimateTime?.replacingOccurrences(of: "mins", with: "m", options: .literal, range: nil)
                            nearEstimateTime = nearEstimateTime?.replacingOccurrences(of: "min", with: "m", options: .literal, range: nil)
                            /*if let idx = nearEstimateTime?.characters.index(of:"m")
                            {
                                let hIndex = nearEstimateTime?.index(idx, offsetBy: -1)
                                nearEstimateTime?.remove(at: hIndex!)
                            }*/
                            print(nearEstimateTime!)
                            let origion : String = "\(SharedClass.latitude),\(SharedClass.longitude)"
                            let destination : String = "\(farestVendor!.latitude!),\(farestVendor!.longitude!)"
                            getEstimatedTime(origion: origion,destination: destination)
                        }
                        else
                        {
                            farEstimateTime = (googleDistanceModel.rows[0].elements[0].duration!.text)!
                            print(farEstimateTime!)
                            farEstimateTime = farEstimateTime?.replacingOccurrences(of: "hours", with: "h", options: .literal, range: nil)
                            farEstimateTime = farEstimateTime?.replacingOccurrences(of: "hour", with: "h", options: .literal, range: nil)
                            /*if let idx = farEstimateTime?.characters.index(of:"h")
                            {
                                let hIndex = farEstimateTime?.index(idx, offsetBy: -1)
                                farEstimateTime?.remove(at: hIndex!)
                            }*/
                            farEstimateTime = farEstimateTime?.replacingOccurrences(of: "mins", with: "m", options: .literal, range: nil)
                            farEstimateTime = farEstimateTime?.replacingOccurrences(of: "min", with: "m", options: .literal, range: nil)
                            /*if let idx = farEstimateTime?.characters.index(of:"m")
                            {
                                let hIndex = farEstimateTime?.index(idx, offsetBy: -1)
                                farEstimateTime?.remove(at: hIndex!)
                            }*/
                            print(farEstimateTime!)
                            if SharedClass.allServices.count == 0
                            {
                                getAllServices()
                            }
                            else
                            {
                                Helper.hideLoadingDialog()
                                self.tableView.reloadData()
                            }
                        }
                        
                    }
                }
                catch let jsonErr{
                    Helper.hideLoadingDialog()
                    print(jsonErr)
                }
            }
        }
    }
    
    func onFailureResponce(obj: Any, resonseType: Int , apiName : String , status : Int) {
        Helper.hideLoadingDialog()
        if resonseType == Constants.API
        {
            if status == Constants.STATUS_300
            {
                Helper.showFailureDialog(title: "Error", message: "Authentication Failed", duration : 1.0)
            }
            else //if status == Constants.STATUS_0 || status == Constants.STATUS_400 || status == Constants.STATUS_500
            {
                let error = obj as! String
                print("Failure : \(error)")
                Helper.showFailureDialog(title: "Error", message: error, duration : 1.0)
            }
        }
        else if resonseType == Constants.NET_ISSUE
        {
            Helper.showFailureDialog(title: "Faiure", message: "Net Connection Not Found", duration : 1.0)
        }
    }
    
    // MARK: Service Dialog Delegate
    
    func confirmButtonTapped(note : String) {
        delegate?.selectedService(selectedIndex: selectedIndex,note: note)
        self.dismiss(animated: true, completion: nil)
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
    
    func cancelButtonTapped() {
        self.dismiss(animated: true, completion: nil)
    }
}
