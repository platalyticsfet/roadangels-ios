//
//  UserHomeVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 20/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import MaterialCard
import MarqueeLabel
import Ripple
import NVActivityIndicatorView
import Cosmos
import Floaty

class UserHomeVC: UIViewController,
                GMSMapViewDelegate,
                GMSAutocompleteViewControllerDelegate,
                SocketMessageListerner,
                ResponceCallback,
                ServiceProtocol ,
                AlertDialogDelegate ,
                ReviewServiceProtocol {

    // MARK: Widgets Objects Declaration
    @IBOutlet weak var mapParentView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var mapmarker: UIView!
    @IBOutlet weak var searchBar: MaterialCard!
    @IBOutlet weak var globIcon: UIImageView!
    @IBOutlet weak var searchIcon: UIImageView!
    @IBOutlet weak var chooseserviceBtn: UIButton!
    @IBOutlet weak var cancelrequestBtn: UIButton!
    @IBOutlet weak var addressLabel: MarqueeLabel!
    @IBOutlet var rippleView: UIView!
    @IBOutlet weak var markerTimeText: UILabel!
    @IBOutlet weak var markerIndicator: UIView!
    @IBOutlet weak var emptyBg: UIView!
    @IBOutlet weak var searchingRadar: UIView!
    @IBOutlet weak var vendorDetailView: MaterialCard!
    @IBOutlet weak var vendorProfilePic: UIImageView!
    @IBOutlet weak var vendorName: UILabel!
    @IBOutlet weak var vendorRating: CosmosView!
    @IBOutlet weak var vendorRatingNumber: UILabel!
    @IBOutlet weak var vendorAddress: UILabel!
    @IBOutlet weak var vendorDate: UILabel!
    @IBOutlet weak var floatButton: Floaty!
    
    // MARK: Navigation Objects Declaration
    var nc: UINavigationController?
    var rootVC: UIViewController!
    let navView = UIView()
    let chatVC = ChatVC(nibName: "ChatVC",bundle: nil)
    
    // MARK: Others Objects Declaration
    let geocoder = GMSGeocoder()
    var range : Int?
    var zoomPower : Float = 12;
    var nearByVendors = [NearByVendorModel]()
    var nearByVendorsMarker = [GMSMarker]()
    var markerIndicatorView : NVActivityIndicatorView!
    var searchingRadarView : NVActivityIndicatorView!
    var requestModel : RequestModel! = nil
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let REQUEST_WAITING_TIME : Double = 30.0
    var timer : Timer? = nil
    var vendor : User! = nil
    var tripModel : TripModel! = nil
    var userMarker : GMSMarker! = nil
    var vendorMarker : GMSMarker! = nil
    var mPathPolygonPoints : GMSPolyline! = nil
    var tripRoute : String!
    var previousLatLng : LatLng! = nil
    var currentLatLng : LatLng! = nil
    var requestAccepted : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        range = 10
        requestAccepted = false
        modifyUIWidgets()
        applyTapGestures()
        loadGoogleMap()
        initSocketManager()
        showRippleEffect()
        setNavigationsBar()
        initMaterialFab()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sideMenuController?.isLeftViewEnabled = true
        navView.isHidden = false
    }
    
    // MARK: UI Setup
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "menu"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(showLeftView(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 8, y: 4, width: 25, height: 25)
        navView.addSubview(button)
        self.navigationController!.navigationBar.addSubview(navView)
        sideMenuController?.isLeftViewEnabled = true
        self.navigationController?.navigationBar.barTintColor = Colors.THEME_BLUE
        self.navigationController?.navigationBar.tintColor = Colors.THEME_BLUE
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        BarButtonItemAppearance.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: Colors.CLEAR], for: .normal)
    }
    
    func modifyUIWidgets()
    {
        addressLabel.fadeLength = 10.0
        searchBar.cornerRadius = 5.0
        globIcon.image = globIcon.image!.withRenderingMode(.alwaysTemplate)
        searchIcon.image = searchIcon.image!.withRenderingMode(.alwaysTemplate)
        globIcon.tintColor = Colors.THEME_BLUE
        searchIcon.tintColor = Colors.THEME_BLUE
    }
    
    func showRippleEffect()
    {
        rippleView.backgroundColor = Colors.CLEAR
        let w = UIScreen.main.bounds.width - 8
        let point : CGPoint = CGPoint(x: w / 2, y: 30)
        ripple(point , view: rippleView, times: 5)
    }
    
    func addIndicators()
    {
        markerIndicator.backgroundColor = Colors.CLEAR
        markerIndicatorView = NVActivityIndicatorView(frame: markerIndicator.frame,type: NVActivityIndicatorType(rawValue: 32)!)
        markerIndicatorView.color = Colors.LIGHT_GRAY_1
        markerIndicatorView.padding = 8.0
        self.markerIndicator.addSubview(markerIndicatorView)
        
        searchingRadar.backgroundColor = Colors.CLEAR
        searchingRadarView = NVActivityIndicatorView(frame: CGRect(x:0,y:-60,width:searchingRadar.frame.width,height:searchingRadar.frame.height),type: NVActivityIndicatorType(rawValue: 16)!)
        
        searchingRadarView.color = Colors.LIGHT_GRAY_1
        self.searchingRadar.insertSubview(searchingRadarView, at: 10)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Google Map Loader
    func loadGoogleMap()
    {
        // Loading Map
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
        self.mapView.delegate = self
        let camera = GMSCameraPosition.camera(withLatitude: SharedClass.latitude, longitude: SharedClass.longitude, zoom: 13.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        self.mapView.camera = camera
        self.mapParentView = mapView
        addIndicators()
        // Start Marker Loading Animation
        markerIndicatorView.startAnimating()
        
        // Server Calling
        getNearyByVendors()
        getAdress(lat: camera.target.latitude,lng: camera.target.longitude)
        if SharedClass.user.trip_id != nil && SharedClass.user.trip_id.count > 0
        {
            requestAccepted = true
            getTrip()
        }
    }
    
    // MARK: Socket Initializer
    func initSocketManager()
    {
        if appDelegate.socketManager == nil
        {
            appDelegate.socketManager = MySocketManager()
        }
        appDelegate.socketManager?.setListener(mylistener: self)
        appDelegate.socketManager?.connectSocketManager()
    }
    
    // MARK: Selectors
    func applyTapGestures()
    {
        let searchClick = UITapGestureRecognizer(target: self, action: #selector(searchButtonClicked))
        searchIcon.addGestureRecognizer(searchClick)
    }
    
    @objc func searchButtonClicked()
    {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        UINavigationBar.appearance().barTintColor = Colors.THEME_BLUE
        UINavigationBar.appearance().tintColor = Colors.WHITE
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @objc func showLeftView(sender: AnyObject?) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    // MARK: Button Listener
    @IBAction func chooseServicebtn(_ sender: Any) {
        
        var nearestVendor : NearByVendorModel!
        var farestVendor : NearByVendorModel!
        var minDistance : Double = 0.0
        var maxDistance : Double = 0.0
        for i in 0...nearByVendors.count-1
        {
            let source : LatLng = LatLng.init(latitude: SharedClass.latitude,longitude: SharedClass.longitude)
            let vendor : LatLng = LatLng.init(latitude: Double(nearByVendors[i].latitude)!,longitude: Double(nearByVendors[i].longitude)!)
            var tempDistance : Double = Helper.getDistance(source: source, vendor: vendor)
            if i == 0 {
                minDistance =  Helper.getDistance(source: source, vendor: vendor)
                maxDistance = minDistance
                nearestVendor = nearByVendors[0]
                farestVendor = nearByVendors[0]
            }
            if tempDistance < minDistance
            {
                minDistance = tempDistance;
                nearestVendor = nearByVendors[i]
            }
            if tempDistance > maxDistance {
                maxDistance = tempDistance;
                farestVendor = nearByVendors[i]
            }
        }
        
        navView.isHidden = true
        let vc = ServicesVC(nibName: "ServicesVC",bundle: nil)
        vc.nearestVendor = nearestVendor
        vc.farestVendor = farestVendor
        vc.delegate = self
        navigationController?.pushViewController(vc,animated: true)
    }
    
    @IBAction func cancelRequestBtn(_ sender: Any) {
        Helper.showYesNoDialog(viewController: self, title: "Cancle Request", message: "You really want to cancle the request ?", responseCallback: self)
    }
    
    //MARK: Google Auto Complete Places Delegates
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: "+place.name)
        print("Place address: "+place.formattedAddress!)
        print("Place Lat/Lng: "+place.coordinate.latitude.description+" , " + place.coordinate.longitude.description)
        SharedClass.latitude = place.coordinate.latitude
        SharedClass.longitude = place.coordinate.longitude
        let myLoc : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: SharedClass.latitude,longitude: SharedClass.longitude)
        self.mapView.animate(with: GMSCameraUpdate.setTarget(myLoc, zoom: zoomPower+3))
        addressLabel.text = place.formattedAddress
        dismiss(animated: false, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error){
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //MARK: Google Map Camera Delegates
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
    }
    
    func didTapMyLocationButton(for mapView : GMSMapView) -> Bool
    {
        SharedClass.latitude = mapView.camera.target.latitude;
        SharedClass.longitude = mapView.camera.target.longitude;
        getAdress(lat: SharedClass.latitude,lng: SharedClass.longitude)
        UserDefault.saveUserLocation(lat: SharedClass.latitude.description , lng: SharedClass.longitude.description)
        return false;
    }
    
    func mapView(_ mapView: GMSMapView, idleAt cameraPosition: GMSCameraPosition) {
        geocoder.reverseGeocodeCoordinate(cameraPosition.target) { (response, error) in
            guard error == nil else {
                return
            }
            if let result = response?.firstResult()
            {
                self.markerIndicator.alpha = 1.0
                self.markerTimeText.text = "?"
                SharedClass.latitude = cameraPosition.target.latitude
                SharedClass.longitude = cameraPosition.target.longitude
                self.getNearestVendor()
                self.getAdress(lat: SharedClass.latitude, lng: SharedClass.longitude)
            }
        }
    }
    
    // MARK: Navigation Drawer Functions
    func goToProfile()
    {
        navView.isHidden = true
        sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        let vc = ProfileVC(nibName: "ProfileVC",bundle: nil)
        navigationController?.pushViewController(vc,animated: true)
    }
    
    func goToSetting()
    {
        navView.isHidden = true
        sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        let vc = SettingVC(nibName: "SettingVC",bundle: nil)
        navigationController?.pushViewController(vc,animated: true)
    }
    
    func goToHistory()
    {
        navView.isHidden = true
        sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        let vc = HistoryVC(nibName: "HistoryVC",bundle: nil)
        navigationController?.pushViewController(vc,animated: true)
    }
    
    func logout()
    {
        sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        let refreshAlert = UIAlertController(title: "Logout", message: "You really want to logout?", preferredStyle: UIAlertControllerStyle.alert)
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            
            ServerCalls.logout(type: "\(SharedClass.accountType.stringValue)", id: "\(SharedClass.user._id!)", responceCallback: self)
            
        }))
        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
    
    // MARK: API calling Functions
    func getNearyByVendors()
    {
        ServerCalls.getNearByVendors(lat: SharedClass.latitude.description, lng: SharedClass.longitude.description, range: range!.description, responceCallback: self)
    }
    
    func getAdress(lat : Double , lng : Double)
    {
        ServerCalls.getAddress(lat: lat.description, lng: lng.description, responceCallback: self)
    }
    
    func getNearestVendor()
    {
        ServerCalls.getNearestVendor(lat: SharedClass.latitude.description, lng: SharedClass.longitude.description, range: (range?.description)!, responceCallback: self)
    }
    
    func getEstimatedTime(desLet:String , desLng:String)
    {
        let origion : String = "\(SharedClass.latitude),\(SharedClass.longitude)"
        let destination : String = "\(desLet),\(desLng)"
        ServerCalls.getTimeDuration(origion: origion, destination: destination, responceCallback: self)
    }
    
    func sendServiceRequest(service : ServicesModel , note: String)
    {
        ServerCalls.sendRequest(serviceId: service._id,note: note, lat: "\(SharedClass.latitude)", lng: "\(SharedClass.longitude)", address: SharedClass.address!, requestTime: "\(Helper.getCurrentMillis())", range: "\(range!)", responceCallback: self)
    }
    
    func getTrip()
    {
        ServerCalls.getPreviousTrips(type: Constants.TRIP.stringValue, id: SharedClass.user.trip_id!, authKey: "0", responceCallback: self)
    }
    
    func getVendor(vendorId: String)
    {
        ServerCalls.getVendor(vendorId: vendorId, responceCallback: self)
    }
    
    // MARK: Socket Protocol Delagates
    func onGetMessageAck(event: String, objectType: Int, obj: Any) {
        if event == Constants.ACCEPT_REQUEST
        {
            let data = obj as! NSArray
            let arr : NSArray = data[0] as! NSArray
            var requestID : String = arr[0] as! String
            if requestModel != nil && requestID == requestModel._id
            {
                if vendor==nil
                {
                    vendor = User()
                }
                if timer != nil
                {
                    timer?.invalidate()
                    timer = nil
                    DispatchQueue.main.async(execute: {() -> Void in
                        Helper.showToast(controller: self, message: "Waiting Timer Closed")
                    })
                }
                requestAccepted = true;
                var jsonObject = arr[1] as! Dictionary<String, AnyObject>
                vendor._id = jsonObject["_id"] as! String
                vendor.first_name = jsonObject["first_name"] as! String
                vendor.last_name = jsonObject["last_name"] as! String
                vendor.email = jsonObject["email"] as! String
                vendor.phone = jsonObject["phone"] as! String
                vendor.profile_pic = jsonObject["profile_pic"] as! String
                vendor.rating = jsonObject["rating"] as! String
                
                jsonObject = arr[2] as! Dictionary<String, AnyObject>
                vendor.latitude = (jsonObject["longitude"] as? NSString)?.doubleValue
                vendor.longitude = (jsonObject["longitude"] as? NSString)?.doubleValue
                vendor.address = jsonObject["address"] as! String
            }
        }
        else if event == Constants.FINISH_TRIP
        {
            if requestAccepted
            {
                finishService(obj: obj);
            }
        }
        else if event == Constants.TRIP_ROUTE
        {
            if requestAccepted
            {
                do
                {
                    let arr = obj as! Array<Any>
                    let data = arr[0] as! Dictionary<String, AnyObject>
                    let pathDic = data["object"] as! Dictionary<String, AnyObject>
                    let routeArray = pathDic["routes"] as! Array<Any>
                    var routes : Dictionary<String, AnyObject>
                    routes = routeArray[0] as! Dictionary<String, AnyObject>
                    let overviewPolylines = routes["overview_polyline"] as! Dictionary<String, AnyObject>
                    
                    self.tripRoute = overviewPolylines["points"] as! String
                    var tripId : String = data["tripId"] as! String
                    var vendorId : String = data["vendor_id"] as! String
                    if vendor._id == vendorId
                    {
                        SharedClass.user.trip_id = tripId
                        var userLatLng : LatLng = LatLng.init(latitude: SharedClass.latitude, longitude: SharedClass.longitude)
                        var vendorLatLng : LatLng = LatLng.init(latitude: vendor.latitude, longitude: vendor.longitude)
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.drawPathBetweenUserVendor(encodingPath: self.tripRoute, userLoc: userLatLng, vendorLoc: vendorLatLng)
                        })
                    }
                }
                catch let Jsonerr
                {
                    print(Jsonerr)
                }
            }
        }
        else if event == Constants.GET_VENDOR_LOCATION
        {
            if requestAccepted
            {
                do
                {
                    let arr = obj as! Array<Any>
                    var data = arr[0] as! Dictionary<String, AnyObject>
                    var vendorId : String = data["vendor_id"] as! String
                    if vendor != nil && vendor._id == vendorId
                    {
                        //var jsonObj : Dictionary<String, AnyObject>
                        //jsonObj = data["venLoc"] as! Dictionary<String, AnyObject>
                        switch(data["latitude"])
                        {
                            case is Double:
                                let lat : Double = data["latitude"] as! Double
                                let lng : Double = data["longitude"] as! Double
                                if  previousLatLng == nil
                                {
                                    previousLatLng = LatLng.init(latitude: lat, longitude: lng)
                                    currentLatLng = LatLng.init(latitude: lat, longitude: lng)
                                } else {
                                    previousLatLng = currentLatLng;
                                    currentLatLng = LatLng.init(latitude: lat, longitude:lng)
                                }
                                break;
                            default:
                                let lat : Double = (data["latitude"] as! NSString).doubleValue
                                let lng : Double = (data["longitude"] as! NSString).doubleValue
                                if  previousLatLng == nil
                                {
                                    previousLatLng = LatLng.init(latitude: lat, longitude: lng)
                                    currentLatLng = LatLng.init(latitude: lat, longitude: lng)
                                } else {
                                    previousLatLng = currentLatLng;
                                    currentLatLng = LatLng.init(latitude: lat, longitude:lng)
                                }
                                break;
                        }
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.animateVendorMove(startPosition: self.previousLatLng, LatLng: self.currentLatLng);
                        })
                    }
                }
                catch let Jsonerr
                {
                    print(Jsonerr)
                }
            }
        }
        else if event == Constants.RECIEVE_NEW_MESSAGE
        {
            do
            {
                let arr = obj as! Array<Any>
                let json = arr[0] as! Dictionary<String, AnyObject>
                print(json)
                let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                let chatModel : ChatModel = try JSONDecoder().decode(ChatModel.self, from: data!)
                chatVC.getNewChatMessage(chatModel: chatModel)
            }
            catch let jsonErr{
                print(jsonErr)
            }
        }
    }
    
    // MARK: Service Protocol Delagates
    func selectedService(selectedIndex: Int,note : String)
    {
        UpdateButtonViewonRequest()
        var servicesModel = ServicesModel()
        servicesModel = SharedClass.allServices[selectedIndex]
        sendServiceRequest(service:servicesModel,note : note)
    }
    
    // MARK: Alert Dialog Delagates
    func okButtonTapped(message:String) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: ReviewService protocol Delegate
    func reviewServiceDone() {
        UpdateButtonViewonFinishTrip()
        getNearyByVendors()
    }
    
    // MARK: Custome Functions
    func UpdateButtonViewonRequest()
    {
        searchBar.isHidden = true
        chooseserviceBtn.isHidden = true
        cancelrequestBtn.isHidden = false
        
        chooseserviceBtn.alpha = 0.0
        cancelrequestBtn.alpha = 1.0
        
        emptyBg.isHidden = false
        searchingRadar.isHidden = false
        searchingRadarView.startAnimating()
    }
    
    func UpdateButtonViewonRequestCancel()
    {
        requestModel = nil
        searchBar.isHidden = false
        chooseserviceBtn.isHidden = false
        cancelrequestBtn.isHidden = true
        
        chooseserviceBtn.alpha = 1.0
        cancelrequestBtn.alpha = 0.0
        
        emptyBg.isHidden = true
        searchingRadarView.stopAnimating()
        searchingRadar.isHidden = true
    }
    
    func UpdateButtonViewonFinishTrip()
    {
        self.mapView.clear()
        //self.mapView.isMyLocationEnabled = true
        //self.mapView.settings.myLocationButton = true
        //self.mapView.reloadInputViews()
        vendor = nil
        tripModel = nil
        previousLatLng = nil;
        currentLatLng = nil;
        vendorMarker = nil;
        userMarker = nil;
        mPathPolygonPoints = nil
        mapmarker.isHidden = false
        searchBar.isHidden = false
        chooseserviceBtn.isHidden = false
        chooseserviceBtn.alpha = 1.0
        cancelrequestBtn.isHidden = true
        cancelrequestBtn.alpha = 0.0
        vendorDetailView.isHidden = true
        floatButton.isHidden = true
    }
    
    func ShowAlertDialog()
    {
        let warningAlert = WarningAlertDialog()
        warningAlert.providesPresentationContextTransitionStyle = true
        warningAlert.definesPresentationContext = true
        warningAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        warningAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        warningAlert.delegate = self
        self.present(warningAlert, animated: true, completion: nil)
    }
    
    func cancelRequest()
    {
        if requestModel != nil && requestModel._id != nil
        {
            ServerCalls.cancelRequest(requestId: requestModel._id!, responceCallback: self)
        }
        UpdateButtonViewonRequestCancel()
    }
    
    func drawPathBetweenUserVendor(encodingPath : String ,userLoc : LatLng ,vendorLoc : LatLng )
    {
        chooseserviceBtn.isHidden = true
        cancelrequestBtn.isHidden = true
        vendorDetailView.isHidden = false
        floatButton.isHidden = false
        let firstName : String = vendor.first_name == nil ? "N/A" : vendor.first_name
        let lastName : String = vendor.last_name == nil ? "N/A" : vendor.last_name
        vendorName.text = firstName + " " + lastName
        
        var time : String! = ""
        if requestModel == nil
        {
            if tripModel.start_time == nil
            {
                time = ""
            }
            else
            {
                time = tripModel.start_time
            }
        }
        else
        {
            if requestModel.request_time == nil
            {
                time = ""
            }
            else
            {
                time = requestModel.request_time
            }
        }
        
        if time == ""
        {
            vendorDate.text = time
        }
        else
        {
            vendorDate.text = Helper.getFormatedData_dd_MM_yyyy__hh_mm__AA(dateInMilliseconds: CLong(time!)!)
        }
        vendorAddress.text = vendor.address
        vendorProfilePic.sd_setImage(with: URL(string: (vendor?.profile_pic)!), placeholderImage: UIImage(named: "user_placeholder.png"))
        if vendor.rating != nil && vendor.rating != ""
        {
            vendorRatingNumber.text = vendor.rating
            vendorRating.rating = Double(vendor.rating)!
        }
        else
        {
            vendorRatingNumber.text = "N/A"
            vendorRating.rating = 0.0
        }
        self.mapView.clear()
        //self.mapView.isMyLocationEnabled = false
        //self.mapView.settings.myLocationButton = false
        emptyBg.isHidden = true
        searchingRadarView.stopAnimating()
        searchingRadar.isHidden = true
        
        if userMarker == nil
        {
            let position = CLLocationCoordinate2D(latitude: Double(userLoc.latitude), longitude: Double(userLoc.longitude))
            userMarker = GMSMarker(position: position)
            userMarker.icon = Helper.imageWithImage(image: UIImage(named: "user_marker")!, scaledToSize: CGSize(width: 30.0, height: 30.0))
            userMarker.map = self.mapView
        }
        else
        {
            userMarker.position = CLLocationCoordinate2D(latitude: Double(userLoc.latitude), longitude: Double(userLoc.longitude))
        }
        
        if vendorMarker == nil
        {
            let position = CLLocationCoordinate2D(latitude: Double(vendorLoc.latitude), longitude: Double(vendorLoc.longitude))
            vendorMarker = GMSMarker(position: position)
            vendorMarker.icon = Helper.imageWithImage(image: UIImage(named: "vendor_marker")!, scaledToSize: CGSize(width: 30.0, height: 30.0))
            vendorMarker.map = self.mapView
        }
        else
        {
            vendorMarker.position = CLLocationCoordinate2D(latitude: Double(vendorLoc.latitude), longitude: Double(vendorLoc.longitude))
        }
        let position = CLLocationCoordinate2D(latitude: Double(userLoc.latitude), longitude: Double(userLoc.longitude))
        self.mapView.animate(with: GMSCameraUpdate.setTarget(position, zoom: 13))
        mapmarker.isHidden = true
        
        mPathPolygonPoints = nil
        tripRoute = encodingPath
        mPathPolygonPoints = GMSPolyline(path: GMSPath.init(fromEncodedPath: encodingPath))
        mPathPolygonPoints.strokeColor = Colors.DARK_GREEN
        mPathPolygonPoints.strokeWidth = 3
        mPathPolygonPoints.geodesic = true
        mPathPolygonPoints.map = self.mapView
    }
    
    func finishService(obj: Any)
    {
        let arr = obj as! Array<Any>
        let data = arr[0] as! Dictionary<String, AnyObject>
        
        tripModel = nil
        tripModel = TripModel.init(dic: data)
        tripModel.trip_path = tripRoute
        if tripModel.vendor_id == vendor._id
        {
            vendor = nil
            requestAccepted = false
            SharedClass.user.trip_id = ""
            createStaticMapImageLink()
        }
        else
        {
            tripModel = nil
        }
    }
    
    func createStaticMapImageLink()
    {
        let url : String = "https://maps.googleapis.com/maps/api/staticmap?size=400x150&format=png&sensor=false&markers=color:0xfe9f00%7C"+tripModel.source_lat_lng!+"&markers=color:0x3CB371%7C"+tripModel.destination_lat_lng!+"&maptype=roadmap&style=element:labels|visibility:off&path=weight:4%7Ccolor:0xd68c0e%7Cenc:"+tripModel.trip_path!
        
        ServerCalls.shortenUrl(url: url, responceCallback: self)
    }
    
    func animateVendorMove(startPosition : LatLng , LatLng finalPosition : LatLng)
    {
        if vendorMarker != nil
        {
            print("VendorPos : \(finalPosition.latitude) , \(finalPosition.longitude)")
            
            /*var start : Int64 = Helper.getCurrentMillis()
            var elapsed : Int64 = Helper.getCurrentMillis() - start;
            var t : Float = Float(elapsed / 1000)
            
            let lat : Double = startPosition.latitude * (1 - t) + finalPosition.latitude * t
            let lng : Double = startPosition.longitude * (1 - t) + finalPosition.longitude * t*/
            
            let currentPosition : LatLng = LatLng.init(latitude: startPosition.latitude, longitude: startPosition.longitude)
            vendorMarker.position = CLLocationCoordinate2D(latitude: currentPosition.latitude,longitude: currentPosition.longitude)
        }
    }
    
    // MARK: Response Callback Delegates
    func onSuccessResponce(obj: Any, resonseType: Int , apiName : String , status : Int) {
        if resonseType == Constants.API
        {
            if apiName == Urls.GET_NEARBY_VENDOR
            {
                rippleView.alpha = 0.0
                do
                {
                    nearByVendors.removeAll()
                    nearByVendorsMarker.removeAll()
                    
                    let dictionary = obj as! Dictionary<String, AnyObject>
                    let data = dictionary["array"] as! NSArray
                    print("Success : \(data))")
                    for vendor: Any in data
                    {
                        let json = vendor as! Dictionary<String, AnyObject>
                        let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                        let nearByvendor : NearByVendorModel = try JSONDecoder().decode(NearByVendorModel.self, from: data!)
                        nearByVendors.append(nearByvendor)
                    }
                    if nearByVendors.count > 0
                    {
                        DispatchQueue.main.async(execute: {() -> Void in
                            
                            for vendor: NearByVendorModel in self.nearByVendors
                            {
                                let position = CLLocationCoordinate2D(latitude: Double(vendor.latitude)!, longitude: Double(vendor.longitude)!)
                                let marker = GMSMarker(position: position)
                                marker.icon = Helper.imageWithImage(image: UIImage(named: "vendors_place")!, scaledToSize: CGSize(width: 50.0, height: 50.0))
                                marker.map = self.mapView
                                self.nearByVendorsMarker.append(marker)
                            }
                            
                        })
                        
                    }
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }
            else if apiName == Urls.GET_ADDRESS
            {
                do
                {
                    let dictionary = obj as! Dictionary<String, AnyObject>
                    let data = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
                    print("Success : \(data))")
                    let address : GoogleAddressModel = try JSONDecoder().decode(GoogleAddressModel.self, from: data!)
                    if address.status == "OK"
                    {
                        let streetAddress : String = address.results[0].formatted_address
                        self.addressLabel.text = streetAddress
                        SharedClass.address = streetAddress
                    }
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }
            else if apiName == Urls.GET_NEAREST_VENDOR
            {
                do
                {
                    let dictionary = obj as! Dictionary<String, AnyObject>
                    let data = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
                    print("Success : \(data)")
                    let nearestVendor : NearByVendorModel = try JSONDecoder().decode(NearByVendorModel.self, from: data!)
                    getEstimatedTime(desLet:nearestVendor.latitude, desLng:nearestVendor.longitude);
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }
            else if apiName == Urls.GET_TIME_DURATION
            {
                do
                {
                    let json = obj as! Dictionary<String, AnyObject>
                    let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                    print("Success : \(data))")
                    let googleDistanceModel : GoogleDistanceModel = try JSONDecoder().decode(GoogleDistanceModel.self, from: data!)
                    if googleDistanceModel.status == "OK"
                    {
                        var estimateTime : String = (googleDistanceModel.rows[0].elements[0].duration!.text)!;
                        estimateTime = estimateTime.replacingOccurrences(of: "mins", with: "", options: .literal, range: nil)
                        estimateTime = estimateTime.replacingOccurrences(of: "min", with: "", options: .literal, range: nil)
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.markerIndicator.alpha = 0.0
                            self.markerTimeText.text = estimateTime
                        })
                    }
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }
            else if apiName == Urls.SEND_REQUEST
            {
                do
                {
                    let json = obj as! Dictionary<String, AnyObject>
                    print("Success : \(json))")
                    if requestModel == nil
                    {
                        requestModel = RequestModel()
                    }
                    requestModel.vendorsId.append(json["vendor_id"] as! String)
                    requestModel.request_status = json["request_status"] as! Bool
                    if requestModel.service == nil
                    {
                        requestModel.service = ServicesModel()
                    }
                    requestModel.service._id = json["service_id"] as! String
                    requestModel.service_note = json["service_note"] as! String
                    requestModel.request_time = json["request_time"] as! String
                    requestModel.user_lng = json["user_lng"] as! String
                    requestModel.user_lat = json["user_lat"] as! String
                    requestModel.user_id = json["user_id"] as! String
                    requestModel._id = json["_id"] as! String
                    print(requestModel)
                    appDelegate.socketManager?.SendRequestToVendor(requestId: "\(requestModel._id!)", serviceId: "\(requestModel.service._id!)", serviceNote: "\(requestModel.service_note!)", userId: "\(SharedClass.user._id!)", lat: "\(SharedClass.latitude)", lng: "\(SharedClass.longitude)", address: "\(SharedClass.address!)", requestTime: "\(Helper.getCurrentMillis())", range: "\(range!)")
                    if timer == nil
                    {
                        timer = Timer.scheduledTimer(withTimeInterval: REQUEST_WAITING_TIME, repeats: false) { (timer) in
                            if self.requestModel != nil
                            {
                                self.timer?.invalidate()
                                self.timer = nil
                                self.ShowAlertDialog()
                                self.cancelRequest()
                            }
                        }
                    }
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }
            else if apiName == Urls.CANCEL_REQUEST
            {
                timer?.invalidate()
                timer = nil
                DispatchQueue.main.async(execute: {() -> Void in
                    Helper.showToast(controller: self, message: "Request Cancled")
                })
            }
            else if apiName == Urls.GET_PREVIOUS_TRIPS
            {
                do
                {
                    let dictionary = obj as! Dictionary<String, AnyObject>
                    let data = dictionary["array"] as! NSArray
                    print("Success : \(data))")
                    for trip: Any in data
                    {
                        let json = trip as! Dictionary<String, AnyObject>
                        let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                        tripModel = try JSONDecoder().decode(TripModel.self, from: data!)
                        getVendor(vendorId:tripModel.vendor_id)
                    }
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }
            else if apiName == Urls.GET_VENDOR
            {
                do
                {
                    let dictionary = obj as! Dictionary<String, AnyObject>
                    let data = dictionary["array"] as! NSArray
                    print("Success : \(data))")
                    self.vendor = nil
                    self.vendor = User()
                    var json = data[0] as! Dictionary<String, AnyObject>
                    
                    self.vendor._id = json["_id"] as! String
                    self.vendor.first_name = json["first_name"] as! String
                    self.vendor.last_name = json["last_name"] as! String
                    self.vendor.email = json["email"] as! String
                    self.vendor.phone = json["phone"] as! String
                    self.vendor.profile_pic = json["profile_pic"] as! String
                    self.vendor.rating = json["rating"] as! String
                    
                    json = data[1] as! Dictionary<String, AnyObject>
                    
                    let location = json["location"] as! Dictionary<String, AnyObject>
                    
                    self.vendor.latitude = (location["lat"] as? NSString)?.doubleValue
                    self.vendor.longitude = (location["lon"] as? NSString)?.doubleValue
                    self.vendor.address = json["address"] as! String
                    
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.chooseserviceBtn.isHidden = true
                        let latlng : String = self.tripModel!.source_lat_lng!.description
                        print(latlng)
                        SharedClass.latitude = Double(self.tripModel!.source_lat_lng!.components(separatedBy: ",")[0])!
                        SharedClass.longitude = Double(self.tripModel!.source_lat_lng!.components(separatedBy: ",")[1])!
                        
                        var userLatLng : LatLng = LatLng.init(latitude: SharedClass.latitude, longitude: SharedClass.longitude)
                        var vendorLatLng : LatLng = LatLng.init(latitude: self.vendor.latitude, longitude: self.vendor.longitude)
                        
                        self.drawPathBetweenUserVendor(encodingPath: self.tripModel.trip_path!, userLoc: userLatLng, vendorLoc: vendorLatLng)
                    })
                    
                    
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }
            else if apiName == Urls.SHORTEN_URL
            {
                let json = obj as! Dictionary<String, AnyObject>
                let shortUrl : String = json["id"] as! String
                self.tripModel.map_image = shortUrl
                navView.isHidden = true
                let vc = ReviewServiceVC(nibName: "ReviewServiceVC",bundle: nil)
                vc.tripModel = self.tripModel
                vc.delegate = self
                navigationController?.pushViewController(vc,animated: true)
            }
            else if apiName == Urls.LOGOUT
            {
                SharedClass.clearAll()
                UserDefault.clearAll()
                var nc: UINavigationController?
                var rootVC: UIViewController!
                rootVC = UserTypeVC(nibName: "UserTypeVC", bundle: Bundle.main)
                nc = UINavigationController(rootViewController: rootVC)
                nc?.interactivePopGestureRecognizer?.isEnabled = false
                let window = UIApplication.shared.delegate!.window!!
                window.rootViewController = nc
                UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
            }
        }
        else if resonseType == Constants.DIALOG
        {
            cancelRequest()
        }
    }
    
    func onFailureResponce(obj: Any, resonseType: Int , apiName : String , status : Int) {
        Helper.hideLoadingDialog()
        if resonseType == Constants.API
        {
            rippleView.alpha = 0.0
            if status == Constants.STATUS_300
            {
                Helper.showFailureDialog(title: "Error", message: "Authentication Failed", duration : 1.0)
            }
            else //if status == Constants.STATUS_0 || status == Constants.STATUS_400 || status == Constants.STATUS_500
            {
                let error = obj as! String
                print("Failure : \(error)")
                if apiName == Urls.GET_TIME_DURATION || apiName == Urls.GET_ADDRESS
                {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.markerIndicator.alpha = 0.0
                        self.markerTimeText.text = "?"
                    })
                }
                else
                {
                    Helper.showFailureDialog(title: "Error", message: error, duration : 1.0)
                }
            }
            
        }
        else if resonseType == Constants.NET_ISSUE
        {
            Helper.showFailureDialog(title: "Faiure", message: "Net Connection Not Found", duration : 1.0)
        }
    }
    
    func initMaterialFab()
    {
        floatButton.addItem("Send Sms", icon: UIImage(named: "sms")!, handler: { item in
            let alert = UIAlertController(title: "Hey", message: "Send Sms...", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Me too", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            self.floatButton.close()
        })
        floatButton.addItem("Make a call", icon: UIImage(named: "phone")!, handler: { item in
            let alert = UIAlertController(title: "Hey", message: "Make a call...", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Me too", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            self.floatButton.close()
        })
        floatButton.addItem("Chat with him", icon: UIImage(named: "chat_icon")!, handler: { item in
            self.floatButton.close()
            self.navView.isHidden = true
            
            self.chatVC.otherUserId = self.vendor._id
            self.chatVC.otherUserName = self.vendor.first_name + " " + self.vendor.last_name
            self.chatVC.tripId = SharedClass.user.trip_id
            
            self.navigationController?.pushViewController(self.chatVC,animated: true)
        })
        floatButton.isHidden = true
    }
}
