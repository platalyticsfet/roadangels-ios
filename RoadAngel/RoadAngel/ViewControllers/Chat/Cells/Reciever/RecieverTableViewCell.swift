//
//  RecieverTableViewCell.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 28/02/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import UIKit

class RecieverTableViewCell: UITableViewCell {

    @IBOutlet weak var messageText: UITextView!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var chatBg: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
