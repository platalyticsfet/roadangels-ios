//
//  ChatVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 27/02/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import UIKit

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

class ChatVC: UIViewController, UITableViewDelegate , UITableViewDataSource ,  ResponceCallback , SocketMessageListerner {
    
    
    
    var rootVC: UIViewController!
    var navController: UINavigationController?
    let navView = UIView()
    @IBOutlet weak var chatTable: UITableView!
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var sendBtn: UIButton!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var chats = [ChatModel]()
    var chatMap : Dictionary<String,ChatModel>! = [:]
    
    var otherUserId : String! = nil
    var otherUserName : String! = nil
    var tripId : String! = nil
    var tableRegistered : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationsBar()
        sideMenuController?.isLeftViewEnabled = false
        chatTable.separatorStyle = .none
        
        let myColor : UIColor = Colors.THEME_BLUE
        messageTextField.layer.borderColor = myColor.cgColor
        messageTextField.layer.borderWidth = 1.0
        messageTextField.layer.cornerRadius = 10.0
        messageTextField.setLeftPaddingPoints(10)
        messageTextField.setRightPaddingPoints(10)
        
        sendBtn.layer.borderColor = myColor.cgColor
        sendBtn.layer.borderWidth = 1.0
        sendBtn.layer.cornerRadius = 10.0
        
        chatTable.register(UINib(nibName: "SenderTableViewCell", bundle: nil), forCellReuseIdentifier: "sender")
        chatTable.register(UINib(nibName: "RecieverTableViewCell", bundle: nil), forCellReuseIdentifier: "reciever")
        
        //populateDummyChat()
        getPreviousChatMessages()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Custom Function
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "icon_back_arrow"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(backButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 8, y: 4, width: 35, height: 35)
        
        let title = UILabel.init()
        title.text = "Chat"
        title.font = UIFont(name: "Calibri", size: 24.0)
        title.textColor = Colors.WHITE
        title.textAlignment = NSTextAlignment.center
        title.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        navView.addSubview(button)
        navView.addSubview(title)
        self.navigationController!.navigationBar.addSubview(navView)
    }
    
    func populateDummyChat()
    {
        chats.append(ChatModel.init(id: SharedClass.user._id, message: "Hello 1", date: Helper.getCurrentMillis().description, user_name: SharedClass.user.first_name, user_id: SharedClass.user._id, user_pic: SharedClass.user.profile_pic, tripId: "101"))
        chats.append(ChatModel.init(id: "1001", message: "Hello 2", date: Helper.getCurrentMillis().description, user_name: SharedClass.user.first_name, user_id: "1001",user_pic: SharedClass.user.profile_pic, tripId: "101"))
        chats.append(ChatModel.init(id: SharedClass.user._id, message: "Hello 3", date: Helper.getCurrentMillis().description, user_name: SharedClass.user.first_name, user_id: SharedClass.user._id, user_pic: SharedClass.user.profile_pic, tripId: "101"))
        chats.append(ChatModel.init(id: SharedClass.user._id, message: "Hello 4", date: Helper.getCurrentMillis().description, user_name: SharedClass.user.first_name, user_id: SharedClass.user._id, user_pic: SharedClass.user.profile_pic, tripId: "101"))
        chats.append(ChatModel.init(id: "1002", message: "Hello 5", date: Helper.getCurrentMillis().description, user_name: SharedClass.user.first_name, user_id: "1002", user_pic: SharedClass.user.profile_pic, tripId: "101"))
        chats.append(ChatModel.init(id: SharedClass.user._id, message: "Hello 6", date: Helper.getCurrentMillis().description, user_name: SharedClass.user.first_name, user_id: SharedClass.user._id, user_pic: SharedClass.user.profile_pic, tripId: "101"))
        chats.append(ChatModel.init(id: "1003", message: "Hello 7", date: Helper.getCurrentMillis().description, user_name: SharedClass.user.first_name, user_id: "1003", user_pic: SharedClass.user.profile_pic, tripId: "101"))
        chats.append(ChatModel.init(id: SharedClass.user._id, message: "Hello 8", date: Helper.getCurrentMillis().description, user_name: SharedClass.user.first_name, user_id: SharedClass.user._id, user_pic: SharedClass.user.profile_pic, tripId: "101"))
        chats.append(ChatModel.init(id: SharedClass.user._id, message: "Hello 9", date: Helper.getCurrentMillis().description, user_name: SharedClass.user.first_name, user_id: SharedClass.user._id, user_pic: SharedClass.user.profile_pic, tripId: "101"))
        chats.append(ChatModel.init(id: "1004", message: "Hello 10", date: Helper.getCurrentMillis().description, user_name: SharedClass.user.first_name, user_id: "1004", user_pic: SharedClass.user.profile_pic, tripId: "101"))
        chatTable.reloadData()
        scrollTable()
    }
    
    func getPreviousChatMessages()
    {
        Helper.showLoadingDialog(message: "Fetching Previous Messages...")
        ServerCalls.getPreviousChat(tripId: tripId, responceCallback: self)
    }
    
    //MARK: Custom Action
    @IBAction func backButtonClicked(sender: AnyObject?)
    {
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: TableView Delegates
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return chats.count
    }
    
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 77.0;
    }*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if chats[indexPath.row].From.id == SharedClass.user._id
        {
            let mySenderCell = tableView.dequeueReusableCell(withIdentifier: "sender")as! SenderTableViewCell
            mySenderCell.profilePic.sd_setImage(with: URL(string: (chats[indexPath.row].From.pic)!), placeholderImage: UIImage(named: "image_placeholder.png"))
            mySenderCell.messageText.text = chats[indexPath.row].message
            mySenderCell.dateTime.text = Helper.getFormatedData_HH_mm_ss(dateInMilliseconds: CLong(chats[indexPath.row].date)!)
            mySenderCell.viewBg.layer.cornerRadius = 10.0
            return mySenderCell
        }
        else
        {
            let myRecieverCell = tableView.dequeueReusableCell(withIdentifier: "reciever")as! RecieverTableViewCell
            myRecieverCell.profilePic.sd_setImage(with: URL(string: (chats[indexPath.row].From.pic)!), placeholderImage: UIImage(named: "image_placeholder.png"))
            myRecieverCell.messageText.text = chats[indexPath.row].message
            myRecieverCell.dateTime.text = Helper.getFormatedData_HH_mm_ss(dateInMilliseconds: CLong(chats[indexPath.row].date)!)
            myRecieverCell.chatBg.layer.cornerRadius = 10.0
            return myRecieverCell
        }
    }
    
    //MARK: Response Callbacks
    func onSuccessResponce(obj: Any, resonseType: Int, apiName: String, status: Int) {
        if resonseType == Constants.API
        {
            Helper.hideLoadingDialog()
            if apiName == Urls.GET_CHAT
            {
                do
                {
                    let dictionary = obj as! Dictionary<String, AnyObject>
                    let data = dictionary["array"] as! NSArray
                    print("Success : \(data))")
                    for vendor: Any in data
                    {
                        let json = vendor as! Dictionary<String, AnyObject>
                        let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                        let chatModel : ChatModel = try JSONDecoder().decode(ChatModel.self, from: data!)
                        chats.append(chatModel)
                        chatMap[chatModel._id] = chatModel
                    }
                    if chats.count > 0
                    {
                        self.chatTable.reloadData()
                        scrollTable()
                    }
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }
        }
    }
    
    func onFailureResponce(obj: Any, resonseType: Int, apiName: String, status: Int) {
        Helper.hideLoadingDialog()
        if resonseType == Constants.API
        {
            if status == Constants.STATUS_300
            {
                Helper.showFailureDialog(title: "Error", message: "Authentication Failed", duration : 1.0)
            }
            else //if status == Constants.STATUS_0 || status == Constants.STATUS_400 || status == Constants.STATUS_500
            {
                let error = obj as! String
                print("Failure : \(error)")
                Helper.showFailureDialog(title: "Error", message: error, duration : 1.0)
            }
        }
        else if resonseType == Constants.NET_ISSUE
        {
            Helper.showFailureDialog(title: "Faiure", message: "Net Connection Not Found", duration : 1.0)
        }
    }
    
    func getNewChatMessage(chatModel: ChatModel)
    {
        print("Other Id : \(otherUserId)")
        print("chatModel.From.id : \(chatModel.From.id)")
        
        if chatModel.From.id == otherUserId
        {
            if chatMap[chatModel._id] == nil
            {
                chats.append(chatModel)
                chatMap[chatModel._id] = chatModel
                self.chatTable.reloadData()
                scrollTable()
            }
        }
    }
    
    // MARK: Socket Callback
    func onGetMessageAck(event: String, objectType: Int, obj: Any) {
        if event == Constants.RECIEVE_NEW_MESSAGE
        {
            do
            {
                let arr = obj as! Array<Any>
                let json = arr[0] as! Dictionary<String, AnyObject>
                let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                let chatModel : ChatModel = try JSONDecoder().decode(ChatModel.self, from: data!)
                if(chatModel.From.id == otherUserId && chatMap[chatModel._id] != nil)
                {
                    chats.append(chatModel)
                    chatMap[chatModel._id] = chatModel
                    self.chatTable.reloadData()
                }
            }
            catch let jsonErr{
                print(jsonErr)
            }
        }
    }
    
    // MARK: Button Delegate
    @IBAction func sendBtnClick(_ sender: Any)
    {
        if messageTextField.text?.isEmpty == false
        {
            let time : String  = "\(Helper.getCurrentMillis())";
            var chat = ChatModel.init(id: time, message: messageTextField.text!, date: time, user_name: SharedClass.user.first_name + " " + SharedClass.user.last_name, user_id: SharedClass.user._id, user_pic: SharedClass.user.profile_pic, tripId: tripId)
            
            chats.append(chat)
            chatMap[chat._id] = chat
            chatTable.reloadData()
            
            scrollTable()
            
            self.appDelegate.socketManager?.SendChatMessage(senderId: SharedClass.user._id, recieverId: otherUserId, authKey: SharedClass.user.auth_key, message: messageTextField.text!, date: "\(Helper.getCurrentMillis())", tripId: tripId)
            
            messageTextField.text! = ""
        }

    }
    
    func scrollTable()
    {
        let numberOfSections = chatTable.numberOfSections
        let numberOfRows = chatTable.numberOfRows(inSection: numberOfSections-1)
        
        let indexPath = IndexPath(row: numberOfRows-1 , section: numberOfSections-1)
        chatTable.scrollToRow(at: indexPath, at: .bottom, animated: false)
        
    }
}
