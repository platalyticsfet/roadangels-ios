//
//  ResetPasswordCodeVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 22/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit

class ResetPasswordCodeVC: UIViewController , ResponceCallback {

    var navController: UINavigationController?
    let navView = UIView()
    var email : String!
    @IBOutlet weak var screenIcon: UIImageView!
    @IBOutlet weak var codeText: UITextField!
    @IBOutlet weak var submitBtn: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyGestures()
        modifyUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationsBar()
    }
    
    func modifyUI()
    {
        screenIcon.image = screenIcon.image?.withRenderingMode(.alwaysTemplate)
        screenIcon.tintColor = Colors.THEME_BLUE
    }
    
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "icon_back_arrow"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(backButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 8, y: 4, width: 35, height: 35)
        
        let title = UILabel.init()
        title.text = "Password Code"
        title.font = UIFont(name: "Calibri", size: 24.0)
        title.textColor = Colors.WHITE
        title.textAlignment = NSTextAlignment.center
        title.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        navView.addSubview(button)
        navView.addSubview(title)
        self.navigationController!.navigationBar.addSubview(navView)
        
    }
    
    func applyGestures()
    {
        let submitclick = UITapGestureRecognizer(target: self, action: #selector(submitButtonClicked))
        submitBtn.addGestureRecognizer(submitclick)
    }
    
    //MARK: Custom Action
    
    @objc func backButtonClicked(sender: AnyObject?)
    {
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
    
    @objc func submitButtonClicked()
    {
        if (codeText.text?.isEmpty)!
        {
            Helper.showFailureDialog(title: "Error", message: "Enter Code Please", duration: 1.0)
        }
        else
        {
            Helper.showLoadingDialog(message: "Please Wait...")
            ServerCalls.codeValidation(email: email, type: SharedClass.accountType.stringValue, code: codeText.text!, responceCallback: self)
        }
    }
    
    // MARK: Response Callback Delegates
    func onSuccessResponce(obj: Any, resonseType: Int, apiName: String , status : Int) {
        Helper.hideLoadingDialog()
        DispatchQueue.main.async(execute: {() -> Void in
            Helper.showSuccessDialog(title: "Code Sent", message: "Code Varification Successfully", duration : 2.0)
            self.navView.removeFromSuperview()
            let vc = ResetPsswordVC(nibName: "ResetPsswordVC",bundle: nil)
            vc.email = self.email
            self.navigationController?.pushViewController(vc,animated: true)
        })
    }
    
    func onFailureResponce(obj: Any, resonseType: Int, apiName: String , status : Int) {
        Helper.hideLoadingDialog()
        if resonseType == Constants.API
        {
            if status == Constants.STATUS_300
            {
                Helper.showFailureDialog(title: "Error", message: "Authentication Failed", duration : 1.0)
            }
            else //if status == Constants.STATUS_0 || status == Constants.STATUS_400 || status == Constants.STATUS_500
            {
                let error = obj as! String
                print("Failure : \(error)")
                Helper.showFailureDialog(title: "Error", message: error, duration : 1.0)
            }
        }
        else if resonseType == Constants.NET_ISSUE
        {
            Helper.showFailureDialog(title: "Faiure", message: "Net Connection Not Found", duration : 1.0)
        }
    }
    
}
