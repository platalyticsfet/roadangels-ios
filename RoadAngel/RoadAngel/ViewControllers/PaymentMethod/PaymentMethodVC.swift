//
//  PaymentMethodVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 18/01/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import UIKit
import Stripe

class PaymentMethodVC: UIViewController , STPPaymentContextDelegate  , STPAddCardViewControllerDelegate , STPPaymentMethodsViewControllerDelegate , STPShippingAddressViewControllerDelegate{

    let navView = UIView()
    var navController: UINavigationController?
    var theme = STPTheme.default()
    
    @IBOutlet weak var paymentCardField: STPPaymentCardTextField!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var doneBtn: UIButton!
    
    enum Demo: Int {
        static let count = 5
        case STPPaymentCardTextField
        case STPAddCardViewController
        case STPPaymentMethodsViewController
        case STPShippingInfoViewController
        case ChangeTheme
        
        var title: String {
            switch self {
            case .STPPaymentCardTextField: return "Card Field"
            case .STPAddCardViewController: return "Card Form with Billing Address"
            case .STPPaymentMethodsViewController: return "Payment Method Picker"
            case .STPShippingInfoViewController: return "Shipping Info Form"
            case .ChangeTheme: return "Change Theme"
            }
        }
        
        var detail: String {
            switch self {
            case .STPPaymentCardTextField: return "STPPaymentCardTextField"
            case .STPAddCardViewController: return "STPAddCardViewController"
            case .STPPaymentMethodsViewController: return "STPPaymentMethodsViewController"
            case .STPShippingInfoViewController: return "STPShippingInfoViewController"
            case .ChangeTheme: return ""
            }
        }
    }
    let customerContext = MockCustomerContext()
    let themeViewController = ThemeViewController()
    
    var viewType : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.isLeftViewEnabled = false
        paymentCardField.isHidden = true
        container.backgroundColor = .clear
        doneBtn.isHidden = true
        viewType = 0
        
        if viewType == 0
        {
            paymentCardField.isHidden = false
            container.backgroundColor = Colors.DARK_GRAY
            doneBtn.isHidden = false
            modifyPaymentCardView()
        }
        else if viewType == 1
        {
            let config = STPPaymentConfiguration()
            config.requiredBillingAddressFields = .full
            let viewController = STPAddCardViewController(configuration: config, theme: theme)
            viewController.delegate = self
            let navigationController = UINavigationController(rootViewController: viewController)
            present(navigationController, animated: true, completion: nil)
        }
        else if viewType == 2
        {
            let config = STPPaymentConfiguration()
            config.additionalPaymentMethods = .all
            config.requiredBillingAddressFields = .none
            let viewController = STPPaymentMethodsViewController(configuration: config,
                                                                 theme: themeViewController.theme.stpTheme,
                                                                 customerContext: self.customerContext,
                                                                 delegate: self)
            let navigationController = UINavigationController(rootViewController: viewController)
            present(navigationController, animated: true, completion: nil)
        }
        else if viewType == 3
        {
            let config = STPPaymentConfiguration()
            config.requiredShippingAddressFields = [.postalAddress]
            let viewController = STPShippingAddressViewController(configuration: config,
                                                                  theme: theme,
                                                                  currency: "usd",
                                                                  shippingAddress: nil,
                                                                  selectedShippingMethod: nil,
                                                                  prefilledInformation: nil)
            viewController.delegate = self
            let navigationController = UINavigationController(rootViewController: viewController)
            present(navigationController, animated: true, completion: nil)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationsBar()
    }
    
    // MARK: CardView Init
    func modifyPaymentCardView()
    {
        paymentCardField.backgroundColor = theme.secondaryBackgroundColor
        paymentCardField.textColor = theme.primaryForegroundColor
        paymentCardField.placeholderColor = theme.secondaryForegroundColor
        paymentCardField.borderColor = theme.accentColor
        paymentCardField.borderWidth = 1.0
        paymentCardField.textErrorColor = theme.errorColor
        paymentCardField.postalCodeEntryEnabled = true
    }
    
    @IBAction func doneClick(_ sender: Any) {
        do
        {
            if paymentCardField.cardNumber == nil ||  (paymentCardField.cardNumber?.isEmpty)!
            {
                Helper.showFailureDialog(title: "Error", message: "Card Numer is missing", duration: 1.0)
            }
            else if paymentCardField.expirationYear.description.isEmpty
            {
                Helper.showFailureDialog(title: "Error", message: "Expiry Year is missing", duration: 1.0)
            }
            else if paymentCardField.expirationMonth.description.isEmpty
            {
                Helper.showFailureDialog(title: "Error", message: "Expiry Month is missing", duration: 1.0)
            }
            else if paymentCardField.cvc == nil ||  (paymentCardField.cvc?.isEmpty)!
            {
                Helper.showFailureDialog(title: "Error", message: "CVC Numer is missing", duration: 1.0)
            }
            else if paymentCardField.postalCode == nil ||  (paymentCardField.postalCode?.isEmpty)!
            {
                Helper.showFailureDialog(title: "Error", message: "Postal Code is missing", duration: 1.0)
            }
            else
            {
                Helper.showSuccessDialog(title: "Success", message: "All OK", duration: 1.0)
            }
        }
        catch let error
        {
            print(error)
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    // MARK: Custome Functions
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let backButton = UIButton.init(type: .custom)
        backButton.setImage(UIImage(named: "icon_back_arrow"), for: UIControlState.normal)
        backButton.addTarget(self, action: #selector(backButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        backButton.frame = CGRect(x: 8, y: 4, width: 35, height: 35)
        
        let title = UILabel.init()
        title.text = "Payment Method"
        title.font = UIFont(name: "Calibri", size: 24.0)
        title.textColor = Colors.WHITE
        title.textAlignment = NSTextAlignment.center
        title.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        navView.addSubview(backButton)
        navView.addSubview(title)
        self.navigationController!.navigationBar.addSubview(navView)
    }
    
    //MARK: Custom Action
    @objc func backButtonClicked(sender: AnyObject?)
    {
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: Payment Method Delegates
    func paymentContext(_ paymentContext: STPPaymentContext, didFailToLoadWithError error: Error) {
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
    
    func paymentContextDidChange(_ paymentContext: STPPaymentContext) {
        /*self.activityIndicator.animating = paymentContext.loading
        self.paymentButton.enabled = paymentContext.selectedPaymentMethod != nil
        self.paymentLabel.text = paymentContext.selectedPaymentMethod?.label
        self.paymentIcon.image = paymentContext.selectedPaymentMethod?.image*/
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext, didCreatePaymentResult paymentResult: STPPaymentResult, completion: @escaping STPErrorBlock) {
        /*myAPIClient.createCharge(paymentResult.source.stripeID, completion: { (error: Error?) in
            if let error = error {
                completion(error)
            } else {
                completion(nil)
            }
        })*/
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext, didFinishWith status: STPPaymentStatus, error: Error?) {
        /*switch status {
            case .error:
                self.showError(error)
            case .success:
                self.showReceipt()
            case .userCancellation:
                return // Do nothing
        }*/
    }

    // MARK: Payment Method Delegates
    func paymentMethodsViewController(_ paymentMethodsViewController: STPPaymentMethodsViewController, didFailToLoadWithError error: Error) {
        dismiss(animated: true, completion: nil)
    }
    
    func paymentMethodsViewControllerDidFinish(_ paymentMethodsViewController: STPPaymentMethodsViewController) {
        paymentMethodsViewController.navigationController?.popViewController(animated: true)
    }
    
    func paymentMethodsViewControllerDidCancel(_ paymentMethodsViewController: STPPaymentMethodsViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: Shipping Address View Delegates
    func shippingAddressViewControllerDidCancel(_ addressViewController: STPShippingAddressViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func shippingAddressViewController(_ addressViewController: STPShippingAddressViewController, didEnter address: STPAddress, completion: @escaping STPShippingMethodsCompletionBlock) {
        let upsGround = PKShippingMethod()
        upsGround.amount = 0
        upsGround.label = "UPS Ground"
        upsGround.detail = "Arrives in 3-5 days"
        upsGround.identifier = "ups_ground"
        let upsWorldwide = PKShippingMethod()
        upsWorldwide.amount = 10.99
        upsWorldwide.label = "UPS Worldwide Express"
        upsWorldwide.detail = "Arrives in 1-3 days"
        upsWorldwide.identifier = "ups_worldwide"
        let fedEx = PKShippingMethod()
        fedEx.amount = 5.99
        fedEx.label = "FedEx"
        fedEx.detail = "Arrives tomorrow"
        fedEx.identifier = "fedex"
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
            if address.country == nil || address.country == "US" {
                completion(.valid, nil, [upsGround, fedEx], fedEx)
            }
            else if address.country == "AQ" {
                let error = NSError(domain: "ShippingError", code: 123, userInfo: [NSLocalizedDescriptionKey: "Invalid Shipping Address",
                                                                                   NSLocalizedFailureReasonErrorKey: "We can't ship to this country."])
                completion(.invalid, error, nil, nil)
            }
            else {
                fedEx.amount = 20.99
                completion(.valid, nil, [upsWorldwide, fedEx], fedEx)
            }
        }
    }
    
    func shippingAddressViewController(_ addressViewController: STPShippingAddressViewController, didFinishWith address: STPAddress, shippingMethod method: PKShippingMethod?) {
        self.customerContext.updateCustomer(withShippingAddress: address, completion: nil)
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: Add Card View Delegates
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        dismiss(animated: true, completion: nil)
    }
    
}
