//
//  RegistrationVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 19/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import GoogleSignIn

class RegistrationVC: UIViewController,GIDSignInUIDelegate , GIDSignInDelegate, ResponceCallback {

    @IBOutlet weak var scrollView: UIScrollView!
    var navController: UINavigationController?
    let navView = UIView()
    @IBOutlet weak var facebookBtn: UIImageView!
    @IBOutlet weak var googlePlusBtn: UIImageView!
    @IBOutlet weak var nextBtn: UILabel!
    @IBOutlet weak var firstnameTxt: UITextField!
    @IBOutlet weak var lastnameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var mobileTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    var firstName : String?
    var lastName : String?
    var email : String?
    var mobile : String?
    var password : String?
    var profilePicLink : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyGestures()
        setNavigationsBar()
        setUIperResolution()
        configGoogleCredentials()
        if SharedClass.accountType == Constants.END_USER
        {
            profilePicLink = "https://i.imgur.com/NZNb3HG.png"
        }
        else
        {
            profilePicLink = "https://i.imgur.com/oTqxmAK.png"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setUIperResolution()
    {
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        if(screenWidth==320.0 && screenHeight==568.0)
        {
            scrollView.contentInset=UIEdgeInsets(top:0.0,left:0.0,bottom:160.0,right:0.0);
        }
        else if(screenWidth==375.0 && screenHeight==667.0)
        {
            scrollView.contentInset=UIEdgeInsets(top:0.0,left:0.0,bottom:60.0,right:0.0);
        }
    }
    
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "icon_back_arrow"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(backButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 8, y: 4, width: 35, height: 35)
        
        let title = UILabel.init()
        title.text = "Register"
        title.font = UIFont(name: "Calibri", size: 24.0)
        title.textColor = Colors.WHITE
        title.textAlignment = NSTextAlignment.center
        title.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        navView.addSubview(button)
        navView.addSubview(title)
        self.navigationController!.navigationBar.addSubview(navView)
    }
    
    func applyGestures()
    {
        let nextclick = UITapGestureRecognizer(target: self, action: #selector(nextButtonClicked))
        nextBtn.addGestureRecognizer(nextclick)
        
        let fbclick = UITapGestureRecognizer(target: self, action: #selector(facebookButtonClicked))
        facebookBtn.addGestureRecognizer(fbclick)
        
        let googleclick = UITapGestureRecognizer(target: self, action: #selector(googlePlusButtonClicked))
        googlePlusBtn.addGestureRecognizer(googleclick)
    }
    
    func configGoogleCredentials()
    {
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().clientID = "260761231393-06pcbrde73u85os0pkbqerhmh2ilsirp.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    }
    
    func goToNextController()
    {
        let viewController: UIViewController
        if(SharedClass.accountType.isEqual(to : Constants.END_USER))
        {
            viewController = UserHomeVC()
            navController = UINavigationController(rootViewController: viewController)
            navController?.interactivePopGestureRecognizer?.isEnabled = false
            let mainViewController = UserRootVC()
            mainViewController.rootViewController = navController
            mainViewController.setupNavigationController()
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = mainViewController
            UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
        }
        else{
            viewController = VendorHomeVC()
            navController = UINavigationController(rootViewController: viewController)
            navController?.interactivePopGestureRecognizer?.isEnabled = false
            let mainViewController = VendorRootVC()
            mainViewController.rootViewController = navController
            mainViewController.setupNavigationController()
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = mainViewController
            UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
        }
    }
    
    //MARK: Custom Action
    @objc func backButtonClicked(sender: AnyObject?)
    {
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
    
    @objc func nextButtonClicked()
    {
        firstName = firstnameTxt.text
        lastName = lastnameTxt.text
        email = emailTxt.text
        mobile = mobileTxt.text
        password = passwordTxt.text
        
        if(firstName?.isEmpty)!
        {
            Helper.showAlertDialog(viewController: self, title: "Error", message: "First name can not be Empty")
        }
        else if(lastName?.isEmpty)!
        {
            Helper.showAlertDialog(viewController: self, title: "Error", message: "Last name can not be Empty")
        }
        else if(email?.isEmpty)!
        {
            Helper.showAlertDialog(viewController: self, title: "Error", message: "Email can not be Empty")
        }
        else if(mobile?.isEmpty)!
        {
            Helper.showAlertDialog(viewController: self, title: "Error", message: "Mobile number can not be Empty")
        }
        else if(password?.isEmpty)!
        {
            Helper.showAlertDialog(viewController: self, title: "Error", message: "Password can not be Empty")
        }
        else
        {
            Helper.showYesNoDialog(viewController: self, title: mobile!, message: "Is it correct number ?", responseCallback: self)
        }
    }
    
    func register()
    {
        Helper.showLoadingDialog(message: "Please Wait...")
        ServerCalls.register(firstname: firstName!, lastname: lastName!, email: email!, mobile: mobile!, password: password!, picUrl: profilePicLink!, responceCallback: self)
    }
    
    @objc func facebookButtonClicked()
    {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [ReadPermission.publicProfile], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print("Logged in!")
                let connection = GraphRequestConnection()
                connection.add(MyProfileRequest()) { response, result in
                    switch result {
                    case .success(let response):
                        print("Custom Graph Request Succeeded: \(response)")
                        //print("My facebook id is \(response.dictionaryValue?["id"])")
                    //print("My name is \(response.dictionaryValue?["name"])")
                    case .failed(let error):
                        print("Custom Graph Request Failed: \(error)")
                    }
                }
                connection.start()
            }
        }
    }
    
    @objc func googlePlusButtonClicked()
    {
        GIDSignIn.sharedInstance().signIn()
    }
    
    // MARK : Google Handler
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,withError error: Error!) {
        if let error = error {
            print("Error : \(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            firstName = user.profile.name
            if (firstName?.contains(" "))! {
                let fullNameArr = firstName?.components(separatedBy: " ")
                firstName = fullNameArr?[0];
                lastName = fullNameArr?[1];
            } else {
                lastName = "";
            }
            email = user.profile.email
            password = user.userID
            firstnameTxt.text = firstName
            lastnameTxt.text = lastName
            emailTxt.text = email
            passwordTxt.text = password
            passwordTxt.isEnabled = false
            profilePicLink = user.profile.imageURL(withDimension: 120).absoluteString
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("Disconnect")
    }
    
    func vertifyMobile(mobile:String)
    {
        ServerCalls.verifyNumber(type: "\(SharedClass.accountType)", phone: mobile, responceCallback: self)
    }
    
    // MARK : Response Callback Functions
    func onSuccessResponce(obj: Any , resonseType : Int , apiName : String , status : Int) {
        if resonseType == Constants.API
        {
            Helper.hideLoadingDialog()
            if apiName == Urls.REGISTER
            {
                UserDefault.savePassword(password: password!)
                Helper.showLoadingDialog(message: "Sending SMS...")
                vertifyMobile(mobile: mobile!)
            }
            else if apiName == Urls.VERIFY_MOBILE
            {
                let viewController = CodeVerificationVC(nibName: "CodeVerificationVC",bundle: nil)
                viewController.phoneNumber = mobile
                navController = UINavigationController(rootViewController: viewController)
                navController?.interactivePopGestureRecognizer?.isEnabled = false
                let window = UIApplication.shared.delegate!.window!!
                window.rootViewController = navController
                UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
            }
        }
        else{
            register()
        }
    }
    
    func onFailureResponce(obj: Any , resonseType: Int , apiName : String , status : Int) {
        Helper.hideLoadingDialog()
        if resonseType == Constants.API
        {
            Helper.hideLoadingDialog()
            if status == Constants.STATUS_300
            {
                Helper.showFailureDialog(title: "Error", message: "Authentication Failed", duration : 1.0)
            }
            else //if status == Constants.STATUS_0 || status == Constants.STATUS_400 || status == Constants.STATUS_500
            {
                let error = obj as! String
                print("Failure : \(error)")
                Helper.showFailureDialog(title: "Error", message: error, duration : 1.0)
            }
        }
        else if resonseType == Constants.NET_ISSUE
        {
            Helper.showFailureDialog(title: "Faiure", message: "Net Connection Not Found", duration : 1.0)
        }
    }
}
