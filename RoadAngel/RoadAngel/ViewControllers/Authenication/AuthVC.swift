//
//  AuthenticationVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 01/01/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import UIKit

class AuthVC: UIViewController , ResponceCallback {

    let navView = UIView()
    var navController: UINavigationController?
    var rootVC: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationsBar()
        authenticationBegin()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Private Methods
    
    func authenticationBegin()
    {
        var email : String = UserDefault.getEmailAddress()
        var password : String = UserDefault.getPassword()
        SharedClass.accountType = UserDefault.getLoginAs() as NSNumber
        ServerCalls.signIn(email: email, password: password, responceCallback: self)
    }
    
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        navView.backgroundColor = Colors.TRANSPARENT
        let title = UILabel.init()
        title.text = "Athentication"
        title.font = UIFont(name: "Calibri", size: 24.0)
        title.textColor = Colors.WHITE
        title.textAlignment = NSTextAlignment.center
        title.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        navView.addSubview(title)
        self.navigationController!.navigationBar.addSubview(navView)
        
    }
    
    func onCompleteAuthentication()
    {
        let viewController: UIViewController
        if(SharedClass.accountType.isEqual(to : Constants.END_USER))
        {
            viewController = UserHomeVC()
            navController = UINavigationController(rootViewController: viewController)
            navController?.interactivePopGestureRecognizer?.isEnabled = false
            let mainViewController = UserRootVC()
            mainViewController.rootViewController = navController
            mainViewController.setupNavigationController()
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = mainViewController
            UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
        }
        else{
            viewController = VendorHomeVC()
            navController = UINavigationController(rootViewController: viewController)
            navController?.interactivePopGestureRecognizer?.isEnabled = false
            let mainViewController = VendorRootVC()
            mainViewController.rootViewController = navController
            mainViewController.setupNavigationController()
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = mainViewController
            UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
        }
        
    }
    
    // MARK : Response Callback Functions
    
    func onSuccessResponce(obj: Any , resonseType : Int , apiName : String , status : Int) {
        if resonseType == Constants.API
        {
            Helper.hideLoadingDialog()
            do
            {
                Helper.showSuccessDialog(title: "Success", message: "Authentication Successfull", duration : 1.0)
                let dictionary = obj as! Dictionary<String, AnyObject>
                let data = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
                print("Success : \(data))")
                SharedClass.user = try JSONDecoder().decode(User.self, from: data!)
                UserDefault.loginAs()
                onCompleteAuthentication()
            }
            catch let jsonErr{
                print(jsonErr)
            }
        }
    }
    
    func onFailureResponce(obj: Any , resonseType : Int , apiName : String , status : Int) {
        Helper.hideLoadingDialog()
        if resonseType == Constants.API
        {
            let error = obj as! String
            print("Failure : \(error)")
            Helper.showFailureDialog(title: "Error", message: error, duration : 1.0)
        }
        else if resonseType == Constants.NET_ISSUE
        {
            Helper.showFailureDialog(title: "Authentication Failed", message: "Net Connection Not Found", duration : 1.0)
        }
        navView.removeFromSuperview()
        rootVC = UserTypeVC(nibName: "UserTypeVC", bundle: Bundle.main)
        navController = UINavigationController(rootViewController: rootVC)
        navController?.interactivePopGestureRecognizer?.isEnabled = false
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navController
        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }

}
