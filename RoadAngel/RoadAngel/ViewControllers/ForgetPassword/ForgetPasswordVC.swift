//
//  ForgetPasswordVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 20/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit

class ForgetPasswordVC: UIViewController , ResponceCallback {

    var navController: UINavigationController?
    let navView = UIView()
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var submitBtn: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyGestures()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationsBar()
    }
    
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "icon_back_arrow"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(backButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 8, y: 4, width: 35, height: 35)
        
        let title = UILabel.init()
        title.text = "Forget Password"
        title.font = UIFont(name: "Calibri", size: 24.0)
        title.textColor = Colors.WHITE
        title.textAlignment = NSTextAlignment.center
        title.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        navView.addSubview(button)
        navView.addSubview(title)
        self.navigationController!.navigationBar.addSubview(navView)
        
    }
    
    func applyGestures()
    {
        let submitclick = UITapGestureRecognizer(target: self, action: #selector(submitButtonClicked))
        submitBtn.addGestureRecognizer(submitclick)
    }
    
    //MARK: Custom Action
    
    @objc func backButtonClicked(sender: AnyObject?)
    {
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
    
    @objc func submitButtonClicked()
    {
        if (emailText.text?.isEmpty)!
        {
            Helper.showFailureDialog(title: "Error", message: "Enter Email Address", duration : 1.0)
        }
        else
        {
            Helper.showLoadingDialog(message: "Please Wait...")
            ServerCalls.forgetPassword(email: emailText.text!, type: "\(SharedClass.accountType.stringValue)", responceCallback: self)
        }
    }
    
    // MARK: Response Callback Delegates
    func onSuccessResponce(obj: Any, resonseType: Int, apiName: String , status : Int) {
        Helper.hideLoadingDialog()
        Helper.showSuccessDialog(title: "Email Sent", message: "An Email with Code send to your given email address", duration : 2.0)
        self.navView.removeFromSuperview()
        let vc = ResetPasswordCodeVC(nibName: "ResetPasswordCodeVC",bundle: nil)
        vc.email = self.emailText.text
        self.navigationController?.pushViewController(vc,animated: true)
    }
    
    func onFailureResponce(obj: Any, resonseType: Int, apiName: String , status : Int) {
        Helper.hideLoadingDialog()
        if resonseType == Constants.API
        {
            Helper.hideLoadingDialog()
            if status == Constants.STATUS_300
            {
                Helper.showFailureDialog(title: "Error", message: "Authentication Failed", duration : 1.0)
            }
            else //if status == Constants.STATUS_0 || status == Constants.STATUS_400 || status == Constants.STATUS_500
            {
                let error = obj as! String
                print("Failure : \(error)")
                Helper.showFailureDialog(title: "Error", message: error, duration : 1.0)
            }
        }
        else if resonseType == Constants.NET_ISSUE
        {
            Helper.showFailureDialog(title: "Faiure", message: "Net Connection Not Found", duration : 1.0)
        }
    }

}
