//
//  UserTypeVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 16/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit
import MaterialCard

class UserTypeVC: UIViewController{

    @IBOutlet weak var userBtn: MaterialCard!
    @IBOutlet weak var vendorBtn: MaterialCard!
    
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var vendorLabel: UILabel!
    
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var vendorIcon: UIImageView!
    
    @IBOutlet weak var registerContainer: MaterialCard!
    @IBOutlet weak var loginContainer: MaterialCard!
    
    let navView = UIView()
    var navController: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modifyUIWidgets()
        applyTapGestures()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Private Methods
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let title = UILabel.init()
        title.text = "Account Selection"
        title.font = UIFont(name: "Calibri", size: 24.0)
        title.textColor = Colors.WHITE
        title.textAlignment = NSTextAlignment.center
        title.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        navView.addSubview(title)
        self.navigationController!.navigationBar.addSubview(navView)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationsBar()
        
        SharedClass.accountType = Constants.END_USER
        userBtn.backgroundColor = Colors.THEME_BLUE
        userLabel.backgroundColor = Colors.THEME_BLUE
        userIcon.image = userIcon.image!.withRenderingMode(.alwaysTemplate)
        userIcon.tintColor = Colors.WHITE
        userLabel.textColor = Colors.WHITE
        
        vendorBtn.backgroundColor = Colors.WHITE
        vendorLabel.backgroundColor = Colors.WHITE
        vendorIcon.image = vendorIcon.image!.withRenderingMode(.alwaysTemplate)
        vendorIcon.tintColor = Colors.BLACK
        vendorLabel.textColor = Colors.BLACK
    }
    
    func modifyUIWidgets()
    {
        userBtn.cornerRadius = 10.0
        vendorBtn.cornerRadius = 10.0
        registerContainer.cornerRadius = 5.0
        loginContainer.cornerRadius = 5.0

        userIcon.image = userIcon.image!.withRenderingMode(.alwaysTemplate)
        userIcon.tintColor = Colors.WHITE
    }
    
    func applyTapGestures()
    {
        let loginClick = UITapGestureRecognizer(target: self, action: #selector(loginButtonClicked))
        let registerClick = UITapGestureRecognizer(target: self, action: #selector(registerButtonClicked))
        let userClick = UITapGestureRecognizer(target: self, action: #selector(userButtonClicked))
        let vendorClick = UITapGestureRecognizer(target: self, action: #selector(vendorButtonClicked))

        loginContainer.addGestureRecognizer(loginClick)
        registerContainer.addGestureRecognizer(registerClick)
        userBtn.addGestureRecognizer(userClick)
        vendorBtn.addGestureRecognizer(vendorClick)
    }
    
    //MARK: Custom Action
    @objc func loginButtonClicked()
    {
        navView.removeFromSuperview()
        let vc = LoginVC(nibName: "LoginVC",bundle: nil)
        navigationController?.pushViewController(vc,animated: true)
    }
    
    @objc func registerButtonClicked()
    {
        navView.removeFromSuperview()
        let vc = RegistrationVC(nibName: "RegistrationVC",bundle: nil)
        navigationController?.pushViewController(vc,animated: true)
    }
    
    @objc func userButtonClicked()
    {
        SharedClass.accountType = Constants.END_USER
        userBtn.backgroundColor = Colors.THEME_BLUE
        userLabel.backgroundColor = Colors.THEME_BLUE
        userIcon.image = userIcon.image!.withRenderingMode(.alwaysTemplate)
        userIcon.tintColor = Colors.WHITE
        userLabel.textColor = Colors.WHITE

        vendorBtn.backgroundColor = Colors.WHITE
        vendorLabel.backgroundColor = Colors.WHITE
        vendorIcon.image = vendorIcon.image!.withRenderingMode(.alwaysTemplate)
        vendorIcon.tintColor = Colors.BLACK
        vendorLabel.textColor = Colors.BLACK
    }
    
    @objc func vendorButtonClicked()
    {
        SharedClass.accountType = Constants.VENDOR
        vendorBtn.backgroundColor = Colors.THEME_BLUE
        vendorLabel.backgroundColor = Colors.THEME_BLUE
        vendorIcon.image = vendorIcon.image!.withRenderingMode(.alwaysTemplate)
        vendorIcon.tintColor = Colors.WHITE
        vendorLabel.textColor = Colors.WHITE

        userBtn.backgroundColor = Colors.WHITE
        userLabel.backgroundColor = Colors.WHITE
        userIcon.image = userIcon.image!.withRenderingMode(.alwaysTemplate)
        userIcon.tintColor = Colors.BLACK
        userLabel.textColor = Colors.BLACK

    }
    
}
