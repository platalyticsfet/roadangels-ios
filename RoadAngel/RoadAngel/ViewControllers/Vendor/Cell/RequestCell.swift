//
//  RequestCell.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 05/01/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import UIKit
import MaterialCard

class RequestCell: UICollectionViewCell {

    @IBOutlet weak var cellView: MaterialCard!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var serviceIcon: UIImageView!
    @IBOutlet weak var minDistanceText: UILabel!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var addressText: UILabel!
    @IBOutlet weak var acceptBtn: UILabel!
    @IBOutlet weak var noteIconBg: UIImageView!
    @IBOutlet weak var noteIcon: UIImageView!
    
    
    var delegate: RequestProtocol?
    var requestId : String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        applyGestures()
        cellView.cornerRadius = 5.0
    }

    func applyGestures()
    {
        let acceptClick = UITapGestureRecognizer(target: self, action: #selector(acceptButtonClicked))
        acceptBtn.addGestureRecognizer(acceptClick)
        
        let noteBtnClick = UITapGestureRecognizer(target: self, action: #selector(NoteButtonClicked))
        noteIconBg.addGestureRecognizer(noteBtnClick)
        noteIcon.addGestureRecognizer(noteBtnClick)
    }
    
    @objc func acceptButtonClicked()
    {
        delegate?.acceptButtonTapped(message: requestId)
    }
    
    @objc func NoteButtonClicked()
    {
        //delegate?.noteButtonTapped(message: requestId)
    }
    
}
