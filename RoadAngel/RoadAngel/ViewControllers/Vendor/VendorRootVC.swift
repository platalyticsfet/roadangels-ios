//
//  RootViewController.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 26/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import Foundation

import UIKit
import LGSideMenuController.LGSideMenuController
import LGSideMenuController.UIViewController_LGSideMenuController

class VendorRootVC: LGSideMenuController {
    
    func setupNavigationController()
    {
        leftViewController = VendorNavDrawerVC()
        leftViewWidth = UIScreen.main.bounds.width * 0.75
        leftViewBackgroundColor = Colors.LIGHT_PURPLE
        leftViewPresentationStyle = .scaleFromBig
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
