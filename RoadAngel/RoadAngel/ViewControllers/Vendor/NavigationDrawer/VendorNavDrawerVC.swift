//
//  NavDrawerVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 26/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit
import SDWebImage

class VendorNavDrawerVC: UIViewController {

    @IBOutlet weak var historyBtn: UIView!
    @IBOutlet weak var earningBtn: UIView!
    @IBOutlet weak var ratingBtn: UIView!
    @IBOutlet weak var settingBtn: UIView!
    @IBOutlet weak var helpBtn: UIView!
    
    @IBOutlet weak var historyIcon: UIImageView!
    @IBOutlet weak var earningIcon: UIImageView!
    @IBOutlet weak var ratingIcon: UIImageView!
    @IBOutlet weak var settingIcon: UIImageView!
    @IBOutlet weak var helpIcon: UIImageView!
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var ratingText: UILabel!
    @IBOutlet weak var ratingStar: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modifyObjects()
        applyTapGestures()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setData()
    {
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
        if SharedClass.user != nil
        {
            if SharedClass.user?.profile_pic != nil
            {
                profilePic.sd_setImage(with: URL(string: (SharedClass.user?.profile_pic)!), placeholderImage: UIImage(named: "user_placeholder.png"))
            }
            username.text = (SharedClass.user?.first_name)! + " " + (SharedClass.user?.last_name)!
            ratingText.text = SharedClass.user?.rating
            if SharedClass.user?.rating==nil || SharedClass.user?.rating == ""
            {
                ratingStar.alpha = 0.0
            }
        }
    }
    
    func modifyObjects()
    {
        historyIcon.image = historyIcon.image?.withRenderingMode(.alwaysTemplate)
        earningIcon.image = earningIcon.image?.withRenderingMode(.alwaysTemplate)
        ratingIcon.image = ratingIcon.image?.withRenderingMode(.alwaysTemplate)
        settingIcon.image = settingIcon.image?.withRenderingMode(.alwaysTemplate)
        helpIcon.image = helpIcon.image?.withRenderingMode(.alwaysTemplate)
        
        earningIcon.tintColor = Colors.BLACK
        historyIcon.tintColor = Colors.BLACK
        ratingIcon.tintColor = Colors.BLACK
        settingIcon.tintColor = Colors.BLACK
        helpIcon.tintColor = Colors.BLACK
        profilePic.maskCircle(anyImage: profilePic.image!)
    }
    
    func applyTapGestures()
    {
        let profileClick = UITapGestureRecognizer(target: self, action: #selector(profileButtonClicked))
        let historyClick = UITapGestureRecognizer(target: self, action: #selector(historyButtonClicked))
        let earningClick = UITapGestureRecognizer(target: self, action: #selector(earningButtonClicked))
        let ratingClick = UITapGestureRecognizer(target: self, action: #selector(ratingButtonClicked))
        let settingClick = UITapGestureRecognizer(target: self, action: #selector(settingButtonClicked))
        let helpClick = UITapGestureRecognizer(target: self, action: #selector(helpButtonClicked))
        
        profilePic.addGestureRecognizer(profileClick)
        historyBtn.addGestureRecognizer(historyClick)
        earningBtn.addGestureRecognizer(earningClick)
        ratingBtn.addGestureRecognizer(ratingClick)
        settingBtn.addGestureRecognizer(settingClick)
        helpBtn.addGestureRecognizer(helpClick)
    }
    
    @objc func profileButtonClicked()
    {
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as! UINavigationController
        let rootViewController = navigationController.topViewController as! VendorHomeVC
        rootViewController.goToProfile()
    }
    
    @objc func historyButtonClicked()
    {
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as! UINavigationController
        let rootViewController = navigationController.topViewController as! VendorHomeVC
        rootViewController.goToHistory()
    }
    
    @objc func earningButtonClicked()
    {
        print("Payment Clicked")
    }
    
    @objc func ratingButtonClicked()
    {
        print("Rating Clicked")
    }
    
    @objc func settingButtonClicked()
    {
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as! UINavigationController
        let rootViewController = navigationController.topViewController as! VendorHomeVC
        rootViewController.goToSetting()
    }

    @objc func helpButtonClicked()
    {
        print("Help Clicked")
    }
}
