//
//  VendorHomeVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 20/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit
import GoogleMaps
import MaterialCard
import Cosmos
import Material
import AVFoundation
import Alamofire
import Floaty

extension UIImage {
    
    func tint(with color: UIColor) -> UIImage
    {
        UIGraphicsBeginImageContext(self.size)
        guard let context = UIGraphicsGetCurrentContext() else { return self }
        
        // flip the image
        context.scaleBy(x: 1.0, y: -1.0)
        context.translateBy(x: 0.0, y: -self.size.height)
        
        // multiply blend mode
        context.setBlendMode(.multiply)
        
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        context.clip(to: rect, mask: self.cgImage!)
        color.setFill()
        context.fill(rect)
        
        // create UIImage
        guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else { return self }
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
}

class VendorHomeVC: UIViewController,
                    GMSMapViewDelegate,
                    SocketMessageListerner,
                    UICollectionViewDelegate,
                    UICollectionViewDataSource,
                    ResponceCallback,
                    CLLocationManagerDelegate,
                    RequestProtocol,
                    ReviewServiceProtocol{
    
    // MARK: Widgets Objects Declaration
    @IBOutlet weak var mapParentView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var userDetailView: MaterialCard!
    @IBOutlet weak var userProfilePic: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userDate: UILabel!
    @IBOutlet weak var userAddress: UILabel!
    @IBOutlet weak var userRating: CosmosView!
    @IBOutlet weak var userRatingText: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var finishServiceBtn: UILabel!
    @IBOutlet weak var floatButton: Floaty!
    
    // MARK: Navigation Objects Declaration
    var nc: UINavigationController?
    var rootVC: UIViewController!
    let navView = UIView()
    
    // MARK: Others Objects Declaration
    let switchLabel = UILabel.init()
    static let sharedInstance = VendorHomeVC()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var ZOOM_POWER : Int = 10
    var userMarker : GMSMarker! = nil
    var vendorMarker : GMSMarker! = nil
    var mPathPolygonPoints : GMSPolyline! = nil
    var currentLatLng : LatLng! = nil
    var previousLatLng : LatLng! = nil
    var tripModel : TripModel! = nil
    var user : User! = nil
    var requestMarkersMap : Dictionary<String,GMSMarker>! = nil
    var requestLists : [RequestModel]? = []
    var selectedRequestId : String! = nil
    var maxColourIndex : Int = 0
    var pathIndex : Int! = 0
    var tripPath : Dictionary<String, AnyObject>! = nil
    var pathString : String! = nil
    var myTimer : Timer? = nil
    let REQUEST_WAITING_TIME : Double = 1.0
    var prevLatLng : LatLng! = nil
    var currLatLng : LatLng! = nil
    var acceptedRequestID : String! = nil
    var player = AVAudioPlayer()
    let chatVC = ChatVC(nibName: "ChatVC",bundle: nil)
    
    private let fabMenuSize = CGSize(width: 56, height: 56)
    private let bottomInset: CGFloat = 24
    private let rightInset: CGFloat = 24
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationsBar()
        loadGoogleMap()
        initSocketManager()
        loadTable()
        initMaterialFab()
        let finishClick = UITapGestureRecognizer(target: self, action: #selector(finishServiceButtonClicked))
        finishServiceBtn.addGestureRecognizer(finishClick)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navView.isHidden = false
        sideMenuController?.isLeftViewEnabled = true
        self.navigationController?.navigationBar.barTintColor = Colors.THEME_BLUE
        self.navigationController?.navigationBar.tintColor = Colors.THEME_BLUE
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        BarButtonItemAppearance.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: Colors.CLEAR], for: .normal)
    }
    
    @objc func finishServiceButtonClicked()
    {
        finishService()
    }
    
    // MARK: UI Setup
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "menu"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(showLeftView(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 8, y: 4, width: 25, height: 25)
        navView.addSubview(button)
        
        let statusSwitch = Switch.init()
        statusSwitch.frame = CGRect(x: CGFloat(navView.frame.size.width - (statusSwitch.frame.size.width+15)), y: CGFloat(0), width: CGFloat(45), height: self.navigationController!.navigationBar.bounds.height)
        statusSwitch.addTarget(self, action: #selector(switchChanged), for: UIControlEvents.valueChanged)
        statusSwitch.buttonOffColor = Colors.DARK_RED
        statusSwitch.buttonOnColor = Colors.DARK_GREEN
        if SharedClass.user != nil && SharedClass.user.status
        {
            statusSwitch.isOn = true
        }
        else
        {
            statusSwitch.isOn = false
        }
        navView.addSubview(statusSwitch)
        
        switchLabel.text = "Online"
        switchLabel.font = UIFont(name: "Calibri", size: 20.0)
        switchLabel.textColor = Colors.WHITE
        switchLabel.textAlignment = .center
        switchLabel.frame = CGRect(x: CGFloat(statusSwitch.frame.origin.x - (statusSwitch.frame.size.width*1.6)), y: CGFloat(0), width: CGFloat(100), height: self.navigationController!.navigationBar.bounds.height)
        navView.addSubview(switchLabel)
        
        self.navigationController!.navigationBar.addSubview(navView)
    }
    
    @objc func showLeftView(sender: AnyObject?) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    @objc func switchChanged(mySwitch: Switch) {
        let value = mySwitch.isOn
        if(value)
        {
            print("On")
            ServerCalls.changeVendorStatus(vendorId: "\(SharedClass.user._id!)", status: true, responceCallback: self)
        }
        else{
            print("Off")
            ServerCalls.changeVendorStatus(vendorId: "\(SharedClass.user._id!)", status: false, responceCallback: self)
        }
    }
    
    // MARK: Google Map Loader
    func loadGoogleMap()
    {
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
        self.mapView.delegate = self
        let camera = GMSCameraPosition.camera(withLatitude: SharedClass.latitude, longitude: SharedClass.longitude, zoom: 13.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        self.mapView.camera = camera
        self.mapParentView = mapView
        
        updateVendorLocation()
        if SharedClass.user.trip_id != nil && SharedClass.user.trip_id.count > 0
        {
            pathIndex = UserDefault.getIndex()
            getTrip()
        }
    }
    
    //MARK: Google Map Camera Delegates
    func didTapMyLocationButton(for mapView : GMSMapView) -> Bool
    {
        SharedClass.latitude = mapView.camera.target.latitude;
        SharedClass.longitude = mapView.camera.target.longitude;
        updateVendorLocation()
        UserDefault.saveUserLocation(lat: SharedClass.latitude.description , lng: SharedClass.longitude.description)
        return false;
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if requestMarkersMap != nil
        {
            for (key,value) in requestMarkersMap {
                var tempMarker : GMSMarker = requestMarkersMap[key]!
                if marker.isEqual(tempMarker)
                {
                    for (index,element) in (requestLists?.enumerated())! {
                        if(requestLists![index]._id == key)
                        {
                            DispatchQueue.main.async(execute: {() -> Void in
                                Helper.showToast(controller: self, message: "Marker at : \(index)")
                            })
                            break;
                        }
                    }
                }
            }
        }
        return false
    }
    
    // MARK: Navigation Drawer Functions
    
    func goToProfile()
    {
        navView.isHidden = true
        sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        let vc = ProfileVC(nibName: "ProfileVC",bundle: nil)
        navigationController?.pushViewController(vc,animated: true)
    }
    
    func goToSetting()
    {
        print("Go To Setting")
        navView.isHidden = true
        sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        let vc = SettingVC(nibName: "SettingVC",bundle: nil)
        navigationController?.pushViewController(vc,animated: true)
    }
    
    func goToHistory()
    {
        navView.isHidden = true
        sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        let vc = HistoryVC(nibName: "HistoryVC",bundle: nil)
        navigationController?.pushViewController(vc,animated: true)
    }

    // MARK: ReviewService protocol Delegate
    func reviewServiceDone() {
        
    }
    
    // MARK: Socket Initializer
    func initSocketManager()
    {
        if appDelegate.socketManager == nil
        {
            appDelegate.socketManager = MySocketManager()
        }
        appDelegate.socketManager?.setListener(mylistener: self)
        appDelegate.socketManager?.connectSocketManager()
    }
    
    // MARK: Socket Protocol Delagates
    func onGetMessageAck(event: String, objectType: Int, obj: Any) {
        if event == Constants.SERVICE_REQUEST && SharedClass.user.status && SharedClass.user.service_status == "Free"
        {
            onGetServiceRequest(obj: obj)
        }
        else if event == Constants.CANCEL_REQUEST && SharedClass.user.status && SharedClass.user.service_status == "Free"
        {
            let array = obj as! NSArray
            let data = array[0] as! String
            cancelRequest(requestId: data)
        }
        else if event == Constants.ACCEPT_REQUEST && SharedClass.user.status && SharedClass.user.service_status == "Free"
        {
            let data = obj as! NSArray
            let array : NSArray = data[0] as! NSArray
            if array.count > 0
            {
                acceptedRequestID = array[0] as! String
            }
        }
        else if event == Constants.RECIEVE_NEW_MESSAGE
        {
            do
            {
                let arr = obj as! Array<Any>
                let json = arr[0] as! Dictionary<String, AnyObject>
                let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                let chatModel : ChatModel = try JSONDecoder().decode(ChatModel.self, from: data!)
                chatVC.getNewChatMessage(chatModel: chatModel)
            }
            catch let jsonErr{
                print(jsonErr)
            }
        }
    }
 
    // MARK: Custome Functions
    
    func loadTable()
    {
        collectionView.backgroundColor = Colors.CLEAR
        collectionView.register(UINib(nibName: "RequestCell", bundle: nil), forCellWithReuseIdentifier: "cell")
    }
    
    func StopGettingRequest()   // Function not calling anywhere
    {
        self.mapView.clear()
        if requestMarkersMap != nil
        {
            requestMarkersMap.removeAll()
            requestMarkersMap = nil
        }
        if requestLists != nil
        {
            requestLists?.removeAll()
        }
        hideRequestListView();
    }
    
    func hideRequestListView()
    {
        collectionView.isHidden = true
    }
    
    func finishService()
    {
        unbindLocationService()
        mapView.clear();
        pathIndex = 0;
        user = nil
        requestMarkersMap = nil
        requestLists?.removeAll()
        collectionView.reloadData()
        hideRequestListView()
        userMarker = nil
        vendorMarker = nil
        mPathPolygonPoints = nil
        previousLatLng = nil
        currentLatLng = nil
        //mapView.isMyLocationEnabled = true
        //mapView.settings.myLocationButton = true
        finishServiceBtn.isHidden = true
        finishTrip()
    }
    
    func finishTrip()
    {
        if myTimer != nil && SharedClass.isInTestingMode!
        {
            myTimer?.invalidate()
        }
        SharedClass.user.service_status = "Free"
        SharedClass.user.trip_id = ""
        ServerCalls.finishTrip(tripId: tripModel._id!, responceCallback: self)
    }
    
    func updateVendorLocation()
    {
        getAdress(lat: SharedClass.latitude,lng: SharedClass.longitude)
    }
    
    func getTrip()
    {
        ServerCalls.getPreviousTrips(type: Constants.TRIP.stringValue, id: SharedClass.user.trip_id!, authKey: "0", responceCallback: self)
    }
    
    func getAdress(lat : Double , lng : Double)
    {
        ServerCalls.getAddress(lat: lat.description, lng: lng.description, responceCallback: self)
    }
    
    func getUser(userId: String)
    {
        ServerCalls.getUser(userId: userId, responceCallback: self)
    }
    
    func drawPathBetweenUserVendor(tripPath:String,userLoc: LatLng,vendorLoc: LatLng , user: User , showAlternativePath: Bool )
    {
        userDetailView.isHidden = false
        floatButton.isHidden = false
        let firstName : String = user.first_name == nil ? "N/A" : user.first_name
        let lastName : String = user.last_name == nil ? "N/A" : user.last_name
        userName.text = firstName + " " + lastName
        if requestLists != nil
        {
            if requestLists!.count > 0
            {
                userDate.text = Helper.getFormatedData_dd_MM_yyyy__hh_mm__AA(dateInMilliseconds: CLong(requestLists![0].request_time!)!)
            }
        }
        else
        {
            userDate.text = Helper.getFormatedData_dd_MM_yyyy__hh_mm__AA(dateInMilliseconds: CLong(tripModel.start_time!)!)
        }
        userAddress.text = user.address!
        userProfilePic.sd_setImage(with: URL(string: (user.profile_pic)!), placeholderImage: UIImage(named: "user_placeholder.png"))
        
        if user.rating != nil && user.rating != ""
        {
            userRatingText.text = user.rating!
            userRating.rating = Double(user.rating!)!
        }
        else
        {
            userRatingText.text = "N/A"
            userRating.rating = 0.0
        }
        //mapView.isMyLocationEnabled = false
        //mapView.settings.myLocationButton = false
        if userMarker == nil
        {
            let position = CLLocationCoordinate2D(latitude: Double(userLoc.latitude), longitude: Double(userLoc.longitude))
            userMarker = GMSMarker(position: position)
            userMarker.icon = Helper.imageWithImage(image: UIImage(named: "user_marker")!, scaledToSize: CGSize(width: 30.0, height: 30.0))
            userMarker.map = self.mapView
        }
        else
        {
            userMarker.position = CLLocationCoordinate2D(latitude: Double(userLoc.latitude), longitude: Double(userLoc.longitude))
        }
        if vendorMarker == nil
        {
            let position = CLLocationCoordinate2D(latitude: Double(vendorLoc.latitude), longitude: Double(vendorLoc.longitude))
            vendorMarker = GMSMarker(position: position)
            vendorMarker.icon = Helper.imageWithImage(image: UIImage(named: "vendor_marker")!, scaledToSize: CGSize(width: 30.0, height: 30.0))
            vendorMarker.map = self.mapView
        }
        else {
            userMarker.position = CLLocationCoordinate2D(latitude: Double(vendorLoc.latitude), longitude: Double(vendorLoc.longitude))
        }
        let position = CLLocationCoordinate2D(latitude: Double(vendorLoc.latitude), longitude: Double(vendorLoc.longitude))
        self.mapView.animate(with: GMSCameraUpdate.setTarget(position, zoom: 13))
        
        var origion : String = "\(vendorLoc.latitude.description) , \(vendorLoc.longitude.description)"
        var destination : String = "\(userLoc.latitude.description) , \(userLoc.longitude.description)"
        mPathPolygonPoints = nil
        if tripPath != nil && tripPath != ""
        {
            mPathPolygonPoints = GMSPolyline(path: GMSPath.init(fromEncodedPath: tripPath))
            mPathPolygonPoints.strokeColor = Colors.DARK_GREEN
            mPathPolygonPoints.strokeWidth = 3
            mPathPolygonPoints.geodesic = true
            mPathPolygonPoints.map = self.mapView
            
            DispatchQueue.main.async(execute: {() -> Void in
                self.finishServiceBtn.isHidden = false
                Helper.showToast(controller: self, message: "Trip Resumed")
                if SharedClass.isInTestingMode!
                {
                    self.launchSandboxTesting()
                }
            })
        }
        else
        {
            ServerCalls.getPathFromGoogle(origion: origion, destination: destination, showAlternativePath: showAlternativePath, responceCallback: self)
        }
    }
    
    func launchSandboxTesting()
    {
        self.myTimer = Timer.scheduledTimer(withTimeInterval: REQUEST_WAITING_TIME, repeats: true) { (timer) in
            UserDefault.saveIndex(index: self.pathIndex)
            var latitude : Double = (self.mPathPolygonPoints.path?.coordinate(at: UInt(self.pathIndex)).latitude)!
            var longitude : Double = (self.mPathPolygonPoints.path?.coordinate(at: UInt(self.pathIndex)).longitude)!
            if self.prevLatLng == nil
            {
                self.prevLatLng = LatLng.init(latitude: latitude, longitude: longitude)
                self.currLatLng = LatLng.init(latitude: latitude, longitude: longitude)
            }
            else
            {
                let lat : Double = self.currentLatLng!.latitude!
                let lng : Double = self.currentLatLng!.longitude!
                self.prevLatLng = LatLng.init(latitude: lat, longitude: lng)
                self.currLatLng = LatLng.init(latitude: latitude, longitude: longitude)
                if self.prevLatLng == nil
                {
                    print("nil 1")
                }
                if self.currLatLng == nil
                {
                    print("nil 2")
                }
            }
            self.pathIndex = self.pathIndex + 1
            self.animateVendorMove(startPosition: self.prevLatLng!,finalPosition: self.currLatLng!)
            self.appDelegate.socketManager?.SendVendorPosition(vendorId: SharedClass.user._id!, latitude: "\(latitude)", longitude: "\(longitude)")
            if self.pathIndex >= Int(self.mPathPolygonPoints.path!.count())
            {
                self.myTimer?.invalidate()
                self.myTimer = nil
                let val : Int = Int(self.mPathPolygonPoints.path!.count())
                self.pathIndex = val - 1
                latitude = (self.mPathPolygonPoints.path?.coordinate(at: UInt(self.pathIndex)).latitude)!
                longitude = (self.mPathPolygonPoints.path?.coordinate(at: UInt(self.pathIndex)).longitude)!
                self.previousLatLng = self.currentLatLng;
                self.currentLatLng = LatLng.init(latitude: latitude, longitude: longitude)
                self.animateVendorMove(startPosition: self.prevLatLng,finalPosition: self.currLatLng)
                self.finishService();
            }
        }
    }
    
    func animateVendorMove(startPosition : LatLng ,finalPosition : LatLng)
    {
        if vendorMarker != nil
        {
            print("VendorPos : \(finalPosition.latitude) , \(finalPosition.longitude)")
            
            /*var start : Int64 = Helper.getCurrentMillis()
             var elapsed : Int64 = Helper.getCurrentMillis() - start;
             var t : Float = Float(elapsed / 1000)
             
             let lat : Double = startPosition.latitude * (1 - t) + finalPosition.latitude * t
             let lng : Double = startPosition.longitude * (1 - t) + finalPosition.longitude * t*/
            
            let currentPosition : LatLng = LatLng.init(latitude: startPosition.latitude, longitude: startPosition.longitude)
            vendorMarker.position = CLLocationCoordinate2D(latitude: currentPosition.latitude,longitude: currentPosition.longitude)
        }
    }
    
    func createStaticMapImageLink(pathString : String)
    {
        var sourceLatLng : String! = ""
        var destinationLatLng : String! = ""
        if requestLists==nil || requestLists?.count==0
        {
            sourceLatLng =  tripModel.source_lat_lng!
            destinationLatLng = tripModel.destination_lat_lng!
        }
        else
        {
            sourceLatLng = "\(requestLists![0].user.latitude!),\(requestLists![0].user.longitude!)"
            destinationLatLng = "\(SharedClass.latitude),\(SharedClass.longitude)"
        }
        let url : String = "https://maps.googleapis.com/maps/api/staticmap?size=400x150&format=png&sensor=false&markers=color:0xfe9f00%7C"+sourceLatLng+"&markers=color:0x3CB371%7C"+destinationLatLng+"&maptype=roadmap&style=element:labels|visibility:off&path=weight:4%7Ccolor:0xd68c0e%7Cenc:"+pathString
        
        ServerCalls.shortenUrl(url: url, responceCallback: self)
    }
    
    func creatTrip(path : Dictionary<String, AnyObject>, pathString : String)
    {
        var model : TripModel! = nil
        model = TripModel()
        model!.__v = 0;
        model!.end_time = ""
        model!.start_time = Helper.getCurrentMillis().description
        model!.vendor_id = SharedClass.user._id!
        model!.destination_lat_lng = "\(SharedClass.latitude),\(SharedClass.longitude)"
        model!.destination_address = SharedClass.address!
        model!.vendor_name = "\(SharedClass.user.first_name!) \(SharedClass.user.last_name!)"
        model!.vendor_profilepic = SharedClass.user.profile_pic!
        print(requestLists!.count.description)
        model!.user_id = requestLists![0].user._id
        model!.source_lat_lng = "\(requestLists![0].user.latitude!),\(requestLists![0].user.longitude!)"
        model!.source_address = requestLists![0].user.address
        model!.user_name = "\(requestLists![0].user.first_name!) \(requestLists![0].user.last_name!)"
        model!.user_profilepic = requestLists![0].user.profile_pic
        model!.service_id = requestLists![0].service._id
        model!.service_name = requestLists![0].service.service_name
        model!.service_price = requestLists![0].service.service_price
        model!.service_icon = requestLists![0].service.service_image
        model!.trip_path = pathString;
        model!.vendor_rating = ""
        model!.user_rating = ""
        model!.auth_key = SharedClass.user.auth_key!
        model!.map_image = ""
        tripPath = path
        self.pathString = pathString
        ServerCalls.createTrip(tripModel: model!, responceCallback: self)
    }
    
    func sendTripPathToUser(tripId : String)
    {
        ServerCalls.setTripRoute(tripId: tripId, routeObj: tripPath as AnyObject, responceCallback: self)
    }
    
    func onGetServiceRequest(obj: Any)
    {
        let array = obj as! NSArray
        let object = array[0] as! Dictionary<String, AnyObject>
        var userJsonObj : Dictionary<String, AnyObject> = object["user"] as! Dictionary<String, AnyObject>
        var serviceJsonObj : Dictionary<String, AnyObject> = object["services"] as! Dictionary<String, AnyObject>
        var requestJsonObj : Dictionary<String, AnyObject> = object["serviceRequests"] as! Dictionary<String, AnyObject>
        var vendorIdsJsonObj : NSArray = object["vendorIds"] as! NSArray
        
        var user : User! = nil
        user = User()
        user._id = userJsonObj["_id"] as! String
        user.first_name = userJsonObj["first_name"] as! String
        user.last_name = userJsonObj["last_name"] as! String
        user.email = userJsonObj["email"] as! String
        user.phone = userJsonObj["phone"] as! String
        user.profile_pic = userJsonObj["profile_pic"] as! String
        user.rating = userJsonObj["rating"] as! String
        
        var requestModel : RequestModel! = nil
        requestModel = RequestModel()
        requestModel.request_status = requestJsonObj["request_status"] as! Bool
        requestModel.request_time = requestJsonObj["request_time"] as! String
        requestModel.user_lng = requestJsonObj["user_lng"] as! String
        requestModel.user_lat = requestJsonObj["user_lat"] as! String
        requestModel.user_id = requestJsonObj["user_id"] as! String
        requestModel._id = requestJsonObj["_id"] as! String
        requestModel.address = requestJsonObj["address"] as! String
        requestModel.service_note = requestJsonObj["service_note"] as! String
        
        user.latitude = Double(requestModel.user_lat)
        user.longitude = Double(requestModel.user_lng)
        user.address = requestModel.address
        requestModel.user = user
        
        for (index,element) in vendorIdsJsonObj.enumerated() {
            var id : String = vendorIdsJsonObj[index] as! String;
            requestModel.vendorsId.append(id)
        }
        
        var servicesModel : ServicesModel! = nil
        servicesModel = ServicesModel()
        servicesModel._id = serviceJsonObj["_id"] as! String
        servicesModel.service_name = serviceJsonObj["service_name"] as! String
        servicesModel.service_detail = serviceJsonObj["service_detail"] as! String
        servicesModel.service_image = serviceJsonObj["service_image"] as! String
        servicesModel.service_price = serviceJsonObj["service_price"] as! String
        requestModel.service = servicesModel
        
        if isRangedVendorForRequest(requestModel: requestModel)
        {
            if requestLists?.count == 0
            {
                maxColourIndex = 0
            }
            requestModel.colorIndex = maxColourIndex
            requestModel.distanceTime = "Fetching..."
            requestLists?.append(requestModel)
            setRequestInList()
            DispatchQueue.main.async(execute: {() -> Void in
                self.playSound()
                self.showRequestMarker(requestId: requestModel._id,pos: LatLng.init(latitude: Double(requestModel.user_lat)!, longitude:Double(requestModel.user_lng)!))
                self.getEstimatedTime(reqId: requestModel._id, desLet: requestModel.user_lat, desLng: requestModel.user_lng)
            })
        }
    }
    
    func cancelRequest(requestId: String)
    {
        if SharedClass.user.status
        {
            acceptedRequestID = nil
            DispatchQueue.main.async(execute: {() -> Void in
                if self.requestMarkersMap != nil
                {
                    if self.requestMarkersMap.keys.contains(requestId)
                    {
                        self.requestMarkersMap[requestId]?.map = nil
                        self.requestMarkersMap.removeValue(forKey: requestId)
                        for (index,element) in (self.requestLists?.enumerated())! {
                            if self.requestLists?[index]._id == requestId
                            {
                                self.requestLists?.remove(at: index)
                                self.collectionView.reloadData()
                                break
                            }
                        }
                    }
                    if self.selectedRequestId != nil && self.selectedRequestId == requestId
                    {
                        self.selectedRequestId = nil
                        self.hideRequestListView()
                    }
                    if self.requestMarkersMap.count == 0
                    {
                        self.collectionView.isHidden = true
                        self.requestMarkersMap = nil
                        self.requestLists?.removeAll()
                        self.hideRequestListView()
                    }
                }
            })
        }
    }
    
    func isRangedVendorForRequest(requestModel : RequestModel) -> Bool
    {
        for (index,element) in requestModel.vendorsId.enumerated()
        {
            if requestModel.vendorsId[index] == SharedClass.user._id!
            {
                return true
            }
        }
        return false
    }
    
    func setRequestInList()
    {
        DispatchQueue.main.async(execute: {() -> Void in
            self.collectionView.isHidden = false
            self.collectionView.reloadData()
            self.myTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
                self.collectionView.reloadData()
            }
        })
    }
    
    func showRequestMarker(requestId : String , pos : LatLng)
    {
        let position = CLLocationCoordinate2D(latitude: Double(pos.latitude), longitude: Double(pos.longitude))
        let marker : GMSMarker! = GMSMarker(position: position)
        let colorObj : ColorsModel = SharedClass.colors[maxColourIndex]
        let r : Double = colorObj.rgb.r!
        let g : Double = colorObj.rgb.g!
        let b : Double = colorObj.rgb.b!
        let image : UIImage = Helper.imageWithImage(image: UIImage(named: "request_marker")!, scaledToSize: CGSize(width: 30.0, height: 30.0)).tint(with: UIColor(red:CGFloat(r/255.0),green:CGFloat(g/255.0),blue:CGFloat(b/255.0),alpha:1.0))
        marker.icon = image
        marker.map = self.mapView
        self.mapView.animate(with: GMSCameraUpdate.setTarget(position, zoom: 13))
    
        if requestMarkersMap==nil
        {
            requestMarkersMap = [:]
        }
        requestMarkersMap[requestId] = marker
        maxColourIndex = maxColourIndex + 1
    }
    
    func getEstimatedTime(reqId: String, desLet: String,desLng:String)
    {
        let origion : String = "\(SharedClass.latitude),\(SharedClass.longitude)"
        let destination : String = "\(desLet),\(desLng)"
        if Helper.isNetConnected()
        {
            var urlString = Urls.GOOGLE_BASE_URL+Urls.GET_TIME_DURATION+"origins="+origion+"&destinations="+destination+"&sensor=true&mode=driving&key="+Constants.GOOGLE_MAP_KEY
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do
                            {
                                let json = JSON as! Dictionary<String, AnyObject>
                                let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                                print("Success : \(data))")
                                let googleDistanceModel : GoogleDistanceModel = try JSONDecoder().decode(GoogleDistanceModel.self, from: data!)
                                if googleDistanceModel.status == "OK"
                                {
                                    var estimateTime : String = (googleDistanceModel.rows[0].elements[0].duration!.text)!;
                                    var estimateDistance : String = (googleDistanceModel.rows[0].elements[0].distance!.text)!;
                                    let timeDistace = "\(estimateTime) / \(estimateDistance)"
                                    
                                    DispatchQueue.main.async(execute: {() -> Void in
                                        var index : Int = 0
                                        for request in self.requestLists! {
                                            if request._id == reqId
                                            {
                                                self.requestLists?[index].distanceTime = timeDistace
                                                self.collectionView.reloadData()
                                                break
                                            }
                                            index = index + 1
                                        }
                                        
                                        
                                    })
                                }
                            }
                            catch let jsonErr{
                                print(jsonErr)
                            }
                            break
                        case .failure(let error):
                            print("Request failed with error: \(error)")
                    }
            }
        }
    }
    
    func updateRequestStatus(requestId:String, vendorId:String, requestStatus:String)
    {
        ServerCalls.updateRequestStatus(requestId: requestId, vendorId: vendorId, requestStatus: requestStatus, responceCallback: self)
    }

    func removeAllRequestMarker()
    {
        self.mapView.clear()
        if requestMarkersMap != nil
        {
            requestMarkersMap.removeAll()
            requestMarkersMap = nil
        }
        hideRequestListView()
    }
    
    func unbindLocationService()
    {
        prevLatLng = nil
        currLatLng = nil
        userDetailView.isHidden = true
        floatButton.isHidden = true
    }
    
    func onAcceptRequest(requestId:String)
    {
        selectedRequestId = requestId
        acceptedRequestID = requestId
        DispatchQueue.main.async(execute: {() -> Void in
            Helper.showToast(controller: self, message: "Accepted")
        })
        updateRequestStatus(requestId: requestId, vendorId: SharedClass.user._id!, requestStatus: "true")
    }
    
    func playSound(){
        do {
            let sound = Bundle.main.path(forResource: "tune", ofType: "mp3")
            try player = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: sound!) as URL)
            player.prepareToPlay()
            player.play()
        } catch let error {
            print(error.localizedDescription)
        }
        
    }
    
    // MARK: CollectionView Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return (requestLists?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : RequestCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RequestCell
        
        /*if self.requestLists?[indexPath.row].service!.service_note == nil && self.requestLists?[indexPath.row].service!.service_note.isEmpty == true
        {
            cell.noteIconBg.isHidden = true
            cell.noteIcon.isHidden = true
        }
        else
        {
            cell.noteIconBg.isHidden = false
            cell.noteIcon.isHidden = false
        }*/
        cell.serviceIcon.sd_setImage(with: URL(string: (self.requestLists?[indexPath.row].service!.service_image!)!), placeholderImage: UIImage(named: "image_placeholder.png"))
        cell.cellView.cornerRadius = 10.0
        let clrIndex : Int = (self.requestLists?[indexPath.row].colorIndex!)!
        print("clrIndex : \(clrIndex)")
        let colorObj : ColorsModel = SharedClass.colors[clrIndex]
        let r : Double = colorObj.rgb.r!
        let g : Double = colorObj.rgb.g!
        let b : Double = colorObj.rgb.b!
        cell.topView.backgroundColor = UIColor(red:CGFloat(r/255.0),green:CGFloat(g/255.0),blue:CGFloat(b/255.0),alpha:1.0)
        cell.serviceName.text = self.requestLists?[indexPath.row].service.service_name
        cell.minDistanceText.text = self.requestLists?[indexPath.row].distanceTime
        cell.addressText.text = self.requestLists?[indexPath.row].address
        cell.requestId = (self.requestLists?[indexPath.row]._id)!!
        cell.delegate = self
        cell.serviceIcon.image = cell.serviceIcon.image!.withRenderingMode(.alwaysTemplate).tint(with: .white)
        return cell
    }
    
    // MARK: Response Callback Delegates
    func onSuccessResponce(obj: Any, resonseType: Int, apiName: String , status : Int) {
        if resonseType == Constants.API
        {
            if apiName ==  Urls.CHANGE_VENDOR_STATUS
            {
                DispatchQueue.main.async(execute: {() -> Void in
                    if SharedClass.user.status
                    {
                        SharedClass.user.status = false
                        if self.requestLists != nil && self.requestLists!.count > 0
                        {
                            self.StopGettingRequest()
                        }
                        Helper.ShowErrorSneakbar(controller: self, message: "You are Offline now")
                    }
                    else
                    {
                        SharedClass.user.status = true
                        Helper.ShowSuccessSneakbar(controller: self, message: "You are Online now")
                    }
                })
            }
            else if apiName ==  Urls.FINISH_TRIP
            {
                do
                {
                    let json = obj as! Dictionary<String, AnyObject>
                    let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                    print("Success : \(data))")
                    tripModel = try JSONDecoder().decode(TripModel.self, from: data!)
                    self.createStaticMapImageLink(pathString: tripModel.trip_path!)

                }
                catch let Jsonerr
                {
                    print(Jsonerr)
                }
                
            }
            else if apiName == Urls.GET_ADDRESS
            {
                do
                {
                    let dictionary = obj as! Dictionary<String, AnyObject>
                    let data = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
                    print("Success : \(data))")
                    let address : GoogleAddressModel = try JSONDecoder().decode(GoogleAddressModel.self, from: data!)
                    if address.status == "OK"
                    {
                        let streetAddress : String = address.results[0].formatted_address
                        SharedClass.address = streetAddress
                        ServerCalls.setVendorLocation(vendorId: "\(SharedClass.user._id!)", lat: "\(SharedClass.latitude)", lng: "\(SharedClass.longitude)", address: "\(SharedClass.address!)", responceCallback: self)
                    }
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }
            else if apiName == Urls.SET_VENDOR_LOCATION
            {
                DispatchQueue.main.async(execute: {() -> Void in
                    Helper.showToast(controller: self, message: "Location Update")
                })
            }
            else if apiName == Urls.GET_PREVIOUS_TRIPS
            {
                do
                {
                    let dictionary = obj as! Dictionary<String, AnyObject>
                    let data = dictionary["array"] as! NSArray
                    print("Success : \(data))")
                    for trip: Any in data
                    {
                        let json = trip as! Dictionary<String, AnyObject>
                        let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                        tripModel = try JSONDecoder().decode(TripModel.self, from: data!)
                        print(tripModel)
                        getUser(userId:tripModel.user_id)
                    }
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }
            else if apiName == Urls.GET_USER
            {
                do
                {
                    let json = obj as! Dictionary<String, AnyObject>
                    let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                    print("Success : \(data))")
                    
                    user = User()
                    
                    user._id = json["_id"] as! String
                    user.first_name = json["first_name"] as! String
                    user.last_name = json["last_name"] as! String
                    user.email = json["email"] as! String
                    user.phone = json["phone"] as! String
                    user.profile_pic = json["profile_pic"] as! String
                    user.rating = json["rating"] as! String
                    user.latitude = Double(self.tripModel.source_lat_lng.components(separatedBy: ",")[0])!
                    user.longitude = Double(self.tripModel.source_lat_lng.components(separatedBy: ",")[1])!
                    user.address = self.tripModel.source_address!
                    
                    DispatchQueue.main.async(execute: {() -> Void in
                        SharedClass.latitude = Double(self.tripModel!.destination_lat_lng!.components(separatedBy: ",")[0])!
                        SharedClass.longitude = Double(self.tripModel!.destination_lat_lng!.components(separatedBy: ",")[1])!
                        
                        var userLatLng : LatLng = LatLng.init(latitude: self.user.latitude, longitude: self.user.longitude)
                        var vendorLatLng : LatLng = LatLng.init(latitude: SharedClass.latitude, longitude: SharedClass.longitude)
                        
                        self.drawPathBetweenUserVendor(tripPath: self.tripModel.trip_path!, userLoc: userLatLng, vendorLoc: vendorLatLng, user: self.user, showAlternativePath: false)
                    })
                    
                    
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }
            else if apiName == Urls.GET_PATH
            {
                do
                {
                    let data = obj as! Dictionary<String, AnyObject>
                    print("Response : \(data))")
                    if data["status"] as! String == "OK"
                    {
                        let routeArray = data["routes"] as! NSArray
                        var routes : Dictionary<String, AnyObject> = routeArray[0] as! Dictionary<String, AnyObject>
                        let overviewPolylines : Dictionary<String, AnyObject> = routes["overview_polyline"] as! Dictionary<String, AnyObject>
                        var encodedString : String = overviewPolylines["points"] as! String
                        mPathPolygonPoints = GMSPolyline(path: GMSPath.init(fromEncodedPath: encodedString))
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.mPathPolygonPoints.strokeColor = Colors.DARK_GREEN
                            self.mPathPolygonPoints.strokeWidth = 3
                            self.mPathPolygonPoints.geodesic = true
                            self.mPathPolygonPoints.map = self.mapView
                            self.creatTrip(path: data,pathString: encodedString)
                            self.finishServiceBtn.isHidden = false
                        })
                    }
                    else
                    {
                        DispatchQueue.main.async(execute: {() -> Void in
                            Helper.showToast(controller: self, message: "Path Not Found")
                        })
                    }
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }
            else if apiName == Urls.CREATE_TRIP
            {
                do
                {
                    let json = obj as! Dictionary<String, AnyObject>
                    let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                    print("Success : \(json))")
                    tripModel = try JSONDecoder().decode(TripModel.self, from: data!)
                    print(tripModel)
                    sendTripPathToUser(tripId: tripModel._id!)
                    DispatchQueue.main.async(execute: {() -> Void in
                        Helper.showToast(controller: self, message: "Trip Created")
                    })
                    SharedClass.user.trip_id = tripModel._id!
                    cancelRequest(requestId: acceptedRequestID)
                    if SharedClass.isInTestingMode!
                    {
                        launchSandboxTesting()
                    }
                    
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }
            else if apiName == Urls.SET_TRIP_ROUTE
            {
                DispatchQueue.main.async(execute: {() -> Void in
                    Helper.showToast(controller: self, message: "Route sent to user")
                })
            }
            /*else if apiName == Urls.GET_TIME_DURATION
            {
                do
                {
                    let json = obj as! Dictionary<String, AnyObject>
                    let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                    print("Success : \(data))")
                    let googleDistanceModel : GoogleDistanceModel = try JSONDecoder().decode(GoogleDistanceModel.self, from: data!)
                    if googleDistanceModel.status == "OK"
                    {
                        var estimateTime : String = (googleDistanceModel.rows[0].elements[0].duration!.text)!;
                        var estimateDistance : String = (googleDistanceModel.rows[0].elements[0].distance!.text)!;
                        let timeDistace = "\(estimateTime) / \(estimateDistance)"
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            // need to Update Request List but how can i get reqId ? and reloadCollectionView here
                        })
                    }
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }*/
            else if apiName == Urls.UPDATE_REQUEST_STATUS
            {
                do
                {
                    let json = obj as! Dictionary<String, AnyObject>
                    let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                    print("Success : \(data))")
                    if json.keys.contains("status")
                    {
                        Helper.showFailureDialog(title: "Error", message: "Somethine went wrong, Please try again", duration : 1.0)
                    }
                    else
                    {
                        var userJsonObj : Dictionary<String, AnyObject> = json["user"] as! Dictionary<String, AnyObject>
                        var serviceJsonObj : Dictionary<String, AnyObject> = json["service"] as! Dictionary<String, AnyObject>
                        var requestJsonObj : Dictionary<String, AnyObject> = json["serviceRequests"] as! Dictionary<String, AnyObject>
                        
                        user = User()
                        user._id = userJsonObj["_id"] as! String
                        user.first_name = userJsonObj["first_name"] as! String
                        user.last_name = userJsonObj["last_name"] as! String
                        user.email = userJsonObj["email"] as! String
                        user.phone = userJsonObj["phone"] as! String
                        user.profile_pic = userJsonObj["profile_pic"] as! String
                        user.rating = userJsonObj["rating"] as! String
                        
                        var requestModel : RequestModel! = nil
                        requestModel = RequestModel()
                        requestModel.vendorsId.append(requestJsonObj["vendor_id"] as! String)
                        requestModel.request_status = requestJsonObj["request_status"] as! Bool
                        requestModel.request_time = requestJsonObj["request_time"] as! String
                        requestModel.user_lng = requestJsonObj["user_lng"] as! String
                        requestModel.user_lat = requestJsonObj["user_lat"] as! String
                        requestModel.user_id = requestJsonObj["user_id"] as! String
                        requestModel._id = requestJsonObj["_id"] as! String
                        requestModel.address = requestJsonObj["address"] as! String
                        
                        user.latitude = Double(requestModel.user_lat)
                        user.longitude = Double(requestModel.user_lng)
                        user.address = requestModel.address
                        requestModel.user = user
                
                        var servicesModel : ServicesModel! = nil
                        servicesModel = ServicesModel()
                        servicesModel._id = serviceJsonObj["_id"] as! String
                        servicesModel.service_name = serviceJsonObj["service_name"] as! String
                        servicesModel.service_detail = serviceJsonObj["service_detail"] as! String
                        servicesModel.service_image = serviceJsonObj["service_image"] as! String
                        servicesModel.service_price = serviceJsonObj["service_price"] as! String
                        requestModel.service = servicesModel
                        
                        SharedClass.user.service_status = "Busy"
                        
                        if requestLists == nil
                        {
                            requestLists = []
                        }
                        else
                        {
                            requestLists?.removeAll()
                        }
                        requestModel.colorIndex = 0
                        requestModel.distanceTime = "Fetching..."
                        requestLists?.append(requestModel)
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.removeAllRequestMarker()
                            print("Request List Size : "+self.requestLists!.count.description)
                            var userLatLng : LatLng = LatLng.init(latitude: Double(requestModel.user_lat!)!, longitude: Double(requestModel.user_lng!)!)
                            var vendorLatLng : LatLng = LatLng.init(latitude: SharedClass.latitude, longitude: SharedClass.longitude)
                            self.drawPathBetweenUserVendor(tripPath: "", userLoc: userLatLng, vendorLoc: vendorLatLng, user: requestModel.user!, showAlternativePath: false)
                        })
                    }
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }
            else if apiName == Urls.SHORTEN_URL
            {
                let json = obj as! Dictionary<String, AnyObject>
                let shortUrl : String = json["id"] as! String
                self.tripModel.map_image = shortUrl
                self.navView.isHidden = true
                let vc = ReviewServiceVC(nibName: "ReviewServiceVC",bundle: nil)
                vc.tripModel = self.tripModel
                vc.delegate = self
                navigationController?.pushViewController(vc,animated: true)
            }
        }
        else if resonseType == Constants.DIALOG
        {
            do
            {
                let json = obj as! Bool
                print("Success : \(json))")
                if json
                {
                    
                }
            }
            catch let jsonErr{
                print(jsonErr)
            }
        }
    }
    
    func onFailureResponce(obj: Any, resonseType: Int, apiName: String , status : Int) {
        Helper.hideLoadingDialog()
        if resonseType == Constants.API
        {
            if status == Constants.STATUS_300
            {
                Helper.showFailureDialog(title: "Error", message: "Authentication Failed", duration : 1.0)
            }
            else //if status == Constants.STATUS_0 || status == Constants.STATUS_400 || status == Constants.STATUS_500
            {
                let error = obj as! String
                print("Failure : \(error)")
                Helper.showFailureDialog(title: "Error", message: error, duration : 1.0)
            }
        }
        else if resonseType == Constants.NET_ISSUE
        {
            Helper.showFailureDialog(title: "Faiure", message: "Net Connection Not Found", duration : 1.0)
        }
    }
    
    // MARK: Location Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])  {
        let userLocation:CLLocation = locations[0] as CLLocation
        let coordinations = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,longitude: userLocation.coordinate.longitude)
        if !SharedClass.isInTestingMode!
        {
            if vendorMarker != nil
            {
                if prevLatLng == nil
                {
                    prevLatLng = LatLng.init(latitude: coordinations.latitude, longitude: coordinations.longitude)
                    currLatLng = LatLng.init(latitude: coordinations.latitude, longitude: coordinations.longitude)
                }
                else
                {
                    previousLatLng = LatLng.init(latitude: currentLatLng.latitude, longitude: currentLatLng.longitude)
                    currLatLng = LatLng.init(latitude: coordinations.latitude, longitude: coordinations.longitude)
                }
                self.animateVendorMove(startPosition: prevLatLng, finalPosition: currLatLng)
                appDelegate.socketManager?.SendVendorPosition(vendorId: SharedClass.user._id!, latitude: "\(coordinations.latitude)", longitude: "\(coordinations.longitude)")
            }
        }
    }
    
    // MARK: Request OK Button Delegate
    func acceptButtonTapped(message:String) {
        self.onAcceptRequest(requestId: message)
    }
    
    func noteButtonTapped(message:String) {
        
    }
    
    func initMaterialFab()
    {
        floatButton.addItem("Send Sms", icon: UIImage(named: "sms")!, handler: { item in
            let alert = UIAlertController(title: "Hey", message: "Send Sms...", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Me too", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            self.floatButton.close()
        })
        floatButton.addItem("Make a call", icon: UIImage(named: "phone")!, handler: { item in
            let alert = UIAlertController(title: "Hey", message: "Make a call...", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Me too", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            self.floatButton.close()
        })
        floatButton.addItem("Chat with him", icon: UIImage(named: "chat_icon")!, handler: { item in
            self.floatButton.close()
            self.navView.isHidden = true
            
            self.chatVC.otherUserId = self.user._id
            self.chatVC.otherUserName = self.user.first_name + " " + self.user.last_name
            self.chatVC.tripId = SharedClass.user.trip_id
            
            self.navigationController?.pushViewController(self.chatVC,animated: true)
        })
        floatButton.addItem("Start Navigation", icon: UIImage(named: "navigation")!, handler: { item in
            let alert = UIAlertController(title: "Hey", message: "Start Navigation...", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Me too", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            self.floatButton.close()
        })
        floatButton.isHidden = true
    }
    
}
