//
//  ProfileVC.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 19/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit
import ImagePicker

class ProfileVC: UIViewController , ImagePickerDelegate , ResponceCallback{
   
    @IBOutlet weak var backBtn: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    let navView = UIView()
    var editingEnable : Bool = false
    var editBtn = UIButton()
    var header = UILabel()
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var updateBtn: UILabel!
    @IBOutlet weak var camerabtn: UIImageView!
    var file : AnyObject? = nil
    var imageUrl : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationsBar()
        sideMenuController?.isLeftViewEnabled = false
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        let result = "Width = "+screenWidth.description+" , " + "Height = "+screenHeight.description
        print(result)
        if(screenWidth==320.0 && screenHeight==568.0)   // iPhone SE
        {
            scrollView.contentInset=UIEdgeInsets(top:0.0,left:0.0,bottom:100.0,right:0.0);
        }
        else if(screenWidth==375.0 && screenHeight==667.0)   // iPhone 8
        {
            scrollView.contentInset=UIEdgeInsets(top:0.0,left:0.0,bottom:10.0,right:0.0);
        }
        // Do any additional setup after loading the view.
        applyGestures()
        populateData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setNavigationsBar()
    {
        navView.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "icon_back_arrow"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(backButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 8, y: 4, width: 35, height: 35)
        
        header = UILabel.init()
        header.text = "Profile"
        header.font = UIFont(name: "Calibri", size: 24.0)
        header.textColor = Colors.WHITE
        header.textAlignment = NSTextAlignment.center
        header.frame = CGRect(x: 0, y: 0, width: self.navigationController!.navigationBar.bounds.width, height: self.navigationController!.navigationBar.bounds.height)
        
        editBtn = UIButton.init(type: .custom)
        editBtn.setImage(UIImage(named: "edit_mode"), for: UIControlState.normal)
        editBtn.addTarget(self, action: #selector(editButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        editBtn.frame = CGRect(x: self.navigationController!.navigationBar.bounds.width - 40, y: 5, width: 30 , height: 30)
        
        navView.addSubview(button)
        navView.addSubview(header)
        navView.addSubview(editBtn)
        self.navigationController!.navigationBar.addSubview(navView)
    }
    
    func applyGestures()
    {
        let updateClick = UITapGestureRecognizer(target: self, action: #selector(updateButtonClicked))
        updateBtn.addGestureRecognizer(updateClick)
        let cameraClick = UITapGestureRecognizer(target: self, action: #selector(cameraButtonClicked))
        camerabtn.addGestureRecognizer(cameraClick)
    }
    
    func populateData()
    {
        profilePic.maskCircle(anyImage: profilePic.image!)
        
        updateBtn.isHidden = true
        camerabtn.isHidden = true
        if SharedClass.user?.profile_pic != nil
        {
            profilePic.sd_setImage(with: URL(string: (SharedClass.user?.profile_pic)!), placeholderImage: UIImage(named: "user_placeholder.png"))
            imageUrl = SharedClass.user!.profile_pic!
        }
        firstName.text = SharedClass.user?.first_name
        lastName.text = SharedClass.user?.last_name
        email.text = SharedClass.user?.email
        phone.text = SharedClass.user?.phone.description
        updateEditing(enable: false)
    }
    
    func updateEditing(enable : Bool)
    {
        firstName.isEnabled = enable
        lastName.isEnabled = enable
        email.isEnabled = enable
        phone.isEnabled = enable
    }
    
    @objc func cameraButtonClicked()
    {
        var config = Configuration()
        config.doneButtonTitle = "Finish"
        config.noImagesTitle = "Sorry! There are no images here!"
        config.recordLocation = false
        config.allowVideoSelection = true
        
        let imagePicker = ImagePickerController(configuration: config)
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func updateButtonClicked()
    {
        editingEnable = false
        updateEditing(enable: false)
        updateBtn.isHidden = true
        editBtn.isHidden = true
        camerabtn.isHidden = true
        header.text = "Profile"
        updateProfile()
    }
    
    //MARK: Custom Action
    @objc func backButtonClicked(sender: AnyObject?)
    {
        navView.removeFromSuperview()
        navigationController?.popViewController(animated: true)
    }
    
    @objc func editButtonClicked(sender: AnyObject?)
    {
        editingEnable = true
        updateEditing(enable: true)
        editBtn.isHidden = true
        updateBtn.isHidden = false
        camerabtn.isHidden = false
        header.text = "Edit Profile"
    }

    // MARK: ImagePicker Delegates
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
        imagePicker.dismiss(animated: true, completion: nil)
        profilePic.image = images[0]
        file = images[0] as! UIImage
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Responce callback Delegates
    func onSuccessResponce(obj: Any, resonseType: Int, apiName: String , status : Int) {
        if resonseType == Constants.API
        {
            if apiName == Urls.UPLOAD_IMAGE
            {
                do
                {
                    let json = obj as! String
                    print("\(Urls.CLOUD_BASE_URL)\(json)")
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.imageUrl = "\(Urls.CLOUD_BASE_URL)\(json)"
                        print("Success : \(self.imageUrl)")
                        self.updateData(pic:self.imageUrl)
                    })
                }
                catch let jsonErr{
                    print(jsonErr)
                }
            }
            else if apiName == Urls.EDIT_PROFILE
            {
                Helper.hideLoadingDialog()
                DispatchQueue.main.async(execute: {() -> Void in
                    SharedClass.user.first_name = self.firstName.text!
                    SharedClass.user.last_name = self.lastName.text!
                    SharedClass.user.profile_pic = self.imageUrl
                    SharedClass.user.phone = self.phone.text! //Int64(self.phone.text!)
                    Helper.showSuccessDialog(title: "Success", message: "Profile Updated Successfully", duration : 1.0)
                    if SharedClass.accountType.intValue == Constants.END_USER.intValue
                    {
                        let mainViewController = self.sideMenuController!
                        let navigationController = mainViewController.rootViewController as! UINavigationController
                        let drawerVc = self.sideMenuController?.leftViewController as! UserNavDrawerVC
                        drawerVc.setData()
                    }
                    else
                    {
                        let mainViewController = self.sideMenuController!
                        let navigationController = mainViewController.rootViewController as! UINavigationController
                        let drawerVc = self.sideMenuController?.leftViewController as! VendorNavDrawerVC
                        drawerVc.setData()
                    }
                })
            }
        }
    }
    
    func onFailureResponce(obj: Any, resonseType: Int, apiName: String , status : Int) {
        Helper.hideLoadingDialog()
        if resonseType == Constants.API
        {
            Helper.hideLoadingDialog()
            if status == Constants.STATUS_300
            {
                Helper.showFailureDialog(title: "Error", message: "Authentication Failed", duration : 1.0)
            }
            else //if status == Constants.STATUS_0 || status == Constants.STATUS_400 || status == Constants.STATUS_500
            {
                let error = obj as! String
                print("Failure : \(error)")
                Helper.showFailureDialog(title: "Error", message: error, duration : 1.0)
            }
        }
        else if resonseType == Constants.NET_ISSUE
        {
            Helper.showFailureDialog(title: "Faiure", message: "Net Connection Not Found", duration : 1.0)
        }
    }
    
    func updateProfile()
    {
        Helper.showLoadingDialog(message: "Please Wait...")
        if file != nil
        {
            uploadImage()
        }
        else
        {
            updateData(pic:SharedClass.user.profile_pic)
        }
    }
    
    func uploadImage()
    {
        ServerCalls.uploadImage(id: (SharedClass.user?._id)!, file: file as! UIImage, responceCallback: self)
    }
    
    func updateData(pic:String)
    {
        let fname : String = firstName.text!
        let lname : String = lastName.text!
        let phoneNum : String = phone.text!
        if fname.isEmpty
        {
            Helper.ShowErrorSneakbar(controller: self, message: "Enter First Name")
        }
        else if lname.isEmpty
        {
            Helper.ShowErrorSneakbar(controller: self, message: "Enter Last Name")
        }
        else if phoneNum.isEmpty
        {
            Helper.ShowErrorSneakbar(controller: self, message: "Enter Phone Number")
        }
        else
        {
            ServerCalls.editProfile(firstName: fname, lastname: lname, phone: phoneNum, profileLink: pic, responceCallback: self)
        }
    }
}
