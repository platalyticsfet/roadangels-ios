//
//  AppDelegate.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 16/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import GoogleSignIn
import FacebookCore
import FacebookLogin
import SocketIO
import SDWebImage
import Stripe

extension UIImageView {
    public func maskCircle(anyImage: UIImage) {
        self.contentMode = UIViewContentMode.scaleAspectFill
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = false
        self.clipsToBounds = true
        self.image = anyImage
    }
}

struct MyProfileRequest: GraphRequestProtocol {
    struct Response: GraphResponseProtocol {
        init(rawResponse: Any?) {
            // Decode JSON from rawResponse into other properties here.
        }
    }
    
    var graphPath = "/me"
    var parameters: [String : Any]? = ["fields": "id, name"]
    var accessToken = AccessToken.current
    var httpMethod: GraphRequestHTTPMethod = .GET
    var apiVersion: GraphAPIVersion = .defaultVersion
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate{

    
    var window: UIWindow?
    var nc: UINavigationController?
    var rootVC: UIViewController!
    var locationManager = CLLocationManager()
    var socketManager : MySocketManager?
    var seconds = 1 //This variable will hold a starting value of seconds.
    var timer = Timer()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // Strip Init
        STPPaymentConfiguration.shared().publishableKey = "pk_test_croXJbJN01XA2ubt7Fd6qSy0"

        getMyLocation()
        Thread.sleep(forTimeInterval: 2.0)
    
        googleServicesInitialization()
        generateColors()
        
        IQKeyboardManager.sharedManager().enable = true
        SharedClass.isInTestingMode = false
        
        if SharedClass.isInTestingMode!
        {
            //Urls.CLOUD_BASE_URL = "http://192.168.23.222:" + Urls.SOCKET_PORT;       // Local Server - Usman's Machine
            Urls.CLOUD_BASE_URL = "http://192.168.23.41:" + Urls.SOCKET_PORT;        // Local Server - Ali's Machine
            //Urls.CLOUD_BASE_URL = "http://192.168.23.183:" + Urls.SOCKET_PORT;     // Local Server - MAC
        }
        else
        {
            Urls.CLOUD_BASE_URL = "http://122.129.79.68:" + Urls.SOCKET_PORT;        // Cloud 8 Server URL - Public
        }
        
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
        
        Authentication()
        runTimer()
        return true
    }
    
    func getMyLocation()
    {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])  {
        let userLocation:CLLocation = locations[0] as CLLocation
        let coordinations = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,longitude: userLocation.coordinate.longitude)
        SharedClass.latitude = coordinations.latitude
        SharedClass.longitude = coordinations.longitude
    }
    
    func googleServicesInitialization()
    {
        GMSServices.provideAPIKey(Constants.GOOGLE_MAP_KEY)
        GMSPlacesClient.provideAPIKey(Constants.GOOGLE_MAP_KEY)
    }
    
    func generateColors()
    {
        if SharedClass.colors.count == 0
        {
            if let path = Bundle.main.path(forResource: "colors", ofType: "json") {
                do {
                    let jsonData = try NSData(contentsOfFile: path, options: NSData.ReadingOptions.mappedIfSafe)
                    do {
                        let jsonResult: NSDictionary = try JSONSerialization.jsonObject(with: jsonData as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if let colors : [NSDictionary] = jsonResult["color"] as? [NSDictionary] {
                            for color: NSDictionary in colors {
                                let json = color as! Dictionary<String, AnyObject>
                                let data = try? JSONSerialization.data(withJSONObject: json, options: [])
                                var colorModel : ColorsModel = try JSONDecoder().decode(ColorsModel.self, from: data!)
                                SharedClass.colors.append(colorModel)
                            }
                        }
                    } catch let err { print(err) }
                } catch let err { print(err) }
            }
        }
    }
    
    func setNavBarAndRootController(vcType : Int)
    {
        UINavigationBar.appearance().tintColor = Colors.THEME_BLUE
        UINavigationBar.appearance().barTintColor = Colors.THEME_BLUE
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        BarButtonItemAppearance.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: Colors.CLEAR], for: .normal)
        window = UIWindow(frame:UIScreen.main.bounds)
        if vcType == 0
        {
            rootVC = UserTypeVC(nibName: "UserTypeVC", bundle: Bundle.main)
            nc = UINavigationController(rootViewController: rootVC)
            nc?.interactivePopGestureRecognizer?.isEnabled = false
            self.window?.rootViewController = nc
        }
        else
        {
            rootVC = AuthVC(nibName: "AuthVC", bundle: Bundle.main)
            nc = UINavigationController(rootViewController: rootVC)
            nc?.interactivePopGestureRecognizer?.isEnabled = false
            self.window?.rootViewController = nc
        }
    }

    func Authentication()
    {
        var email : String = UserDefault.getEmailAddress()
        var password : String = UserDefault.getPassword()
        if email != "" && password != ""
        {
            SharedClass.accountType = UserDefault.getLoginAs() as NSNumber
            setNavBarAndRootController(vcType: 1)
        }
        else
        {
            setNavBarAndRootController(vcType: 0)
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        if (socketManager?.isConnected())!
        {
            socketManager?.socketDisconnect()
            timer.invalidate()
        }
    }

    func application(_ application: UIApplication,open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,sourceApplication: sourceApplication,annotation: annotation)
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }
    
    func runTimer()
    {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(keepSocketConnectionAlive)), userInfo: nil, repeats: true)
    }
    
    @objc func keepSocketConnectionAlive() {
        if socketManager != nil
        {
            if (socketManager?.isConnected())!
            {
                socketManager?.KeepConnectionAlive()
            }
        }
    }
}

