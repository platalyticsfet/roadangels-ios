//
//  ServerCalls.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 28/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

/*
    Fold: command-alt-left arrow
    Unfold: command-alt-right arrow
*/

import Foundation
import Alamofire

class ServerCalls
{
    // MARK: Cloud Server Calls
    
    static func signIn(email: String, password:String, responceCallback : ResponceCallback)
    {
        if Helper.isNetConnected()
        {
            let urlString = Urls.CLOUD_BASE_URL+Urls.LOGIN+"/"+email+"/"+password+"/"+SharedClass.accountType.stringValue
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
            {
                response in switch response.result
                {
                    case .success(let JSON):
                        do {
                            let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                            print("Response : \(json)")
                            if json!["status"] as! Int! == Constants.STATUS_200
                            {
                                responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.LOGIN , status : Constants.STATUS_0)
                            }
                            else if json!["status"] as! Int! == Constants.STATUS_300
                            {
                                responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.LOGIN , status : Constants.STATUS_300)
                            }
                            else
                            {
                                responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.LOGIN , status : Constants.STATUS_500)
                            }
                        } catch let error {
                            responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.LOGIN , status : Constants.STATUS_500)
                        }
                    case .failure(let error):
                        responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.LOGIN , status : Constants.STATUS_0)
                }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.LOGIN , status : Constants.STATUS_0)
        }
    }
    
    static func register(firstname : String, lastname : String, email: String, mobile : String, password:String, picUrl : String, responceCallback : ResponceCallback)
    {
        if Helper.isNetConnected()
        {
            let urlString = Urls.CLOUD_BASE_URL+Urls.REGISTER
            print("API Url = \(urlString)")
            let params : [String : Any] = ["details" : ["firstname":firstname,"lastname":lastname,"phone":mobile,"email":email, "password":password,"profilepic":picUrl, "type":SharedClass.accountType.stringValue]]
            print("Params : \(params)")
            let headers : HTTPHeaders = [
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
            Alamofire.request(urlString,method: .post,parameters: params, encoding:JSONEncoding.default, headers: headers).responseJSON
                {
                    (response) in
                    print("JSCON RESPONSE \(response)")
                    switch response.result
                    {
                    case .success(let JSON):
                        do {
                            let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                            print("Response : \(json)")
                            if json!["status"] as! Int! == Constants.STATUS_200
                            {
                                responceCallback.onSuccessResponce(obj: json!["message"] as! String, resonseType: Constants.API,apiName: Urls.REGISTER , status : Constants.STATUS_0)
                            }
                            else if json!["status"] as! Int! == Constants.STATUS_300
                            {
                                responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.REGISTER , status : Constants.STATUS_300)
                            }
                            else
                            {
                                responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.REGISTER , status : Constants.STATUS_500)
                            }
                        } catch let error {
                            responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.REGISTER , status : Constants.STATUS_500)
                        }
                    case .failure(let error):
                        responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.REGISTER , status : Constants.STATUS_0)
                    }
            }
        }
        else
        {
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.REGISTER , status : Constants.STATUS_0)
        }
    }

    static func setVendorLocation(vendorId : String, lat : String, lng : String, address : String,responceCallback : ResponceCallback)
    {
        if Helper.isNetConnected()
        {
            var urlString = "\(Urls.CLOUD_BASE_URL + Urls.SET_VENDOR_LOCATION)/\(vendorId)/\(lat)/\(lng)/\(address)/\(SharedClass.user!.auth_key!)"
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.SET_VENDOR_LOCATION , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.SET_VENDOR_LOCATION , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.SET_VENDOR_LOCATION , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.SET_VENDOR_LOCATION , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.SET_VENDOR_LOCATION , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.SET_VENDOR_LOCATION , status : Constants.STATUS_0)
        }
    }
    
    static func createTrip(tripModel:TripModel,responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            do
            {
                let urlString = Urls.CLOUD_BASE_URL+Urls.CREATE_TRIP
                print("API Url = \(urlString)")
                let params : [String : Any] = ["trip" : tripModel.toJSON()]
                print("Params : \(params)")
                let headers : HTTPHeaders = [
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                ]
                Alamofire.request(urlString,method: .post,parameters: params, encoding:JSONEncoding.default, headers: headers).responseJSON
                    {
                        response in switch response.result
                        {
                            case .success(let JSON):
                                do {
                                    let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                    print("Response : \(json)")
                                    if json!["status"] as! Int! == Constants.STATUS_200
                                    {
                                        responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.CREATE_TRIP , status : Constants.STATUS_0)
                                    }
                                    else if json!["status"] as! Int! == Constants.STATUS_300
                                    {
                                        responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.CREATE_TRIP , status : Constants.STATUS_300)
                                    }
                                    else
                                    {
                                        responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.CREATE_TRIP , status : Constants.STATUS_500)
                                    }
                                } catch let error {
                                    responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.CREATE_TRIP , status : Constants.STATUS_500)
                                }
                            case .failure(let error):
                                responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.CREATE_TRIP , status : Constants.STATUS_0)
                        }
                }
            }
            catch let jsonErr{
                print(jsonErr)
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.CREATE_TRIP , status : Constants.STATUS_0)
        }
    }
    
    static func finishTrip(tripId:String ,responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            var urlString = "\(Urls.CLOUD_BASE_URL + Urls.FINISH_TRIP)/\(tripId)/\(Helper.getCurrentMillis())/\(SharedClass.user!._id!)/\(SharedClass.user!.auth_key!)"
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.FINISH_TRIP , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.FINISH_TRIP , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.FINISH_TRIP , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.FINISH_TRIP , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.FINISH_TRIP , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.FINISH_TRIP , status : Constants.STATUS_0)
        }
    }
    
    static func getNearByVendors( lat : String, lng : String, range : String,responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            var urlString = "\(Urls.CLOUD_BASE_URL + Urls.GET_NEARBY_VENDOR)/\(lat)/\(lng)/\(range)/\(SharedClass.user!._id!)/\(SharedClass.user!.auth_key!)"
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.GET_NEARBY_VENDOR , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.GET_NEARBY_VENDOR , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.GET_NEARBY_VENDOR , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.GET_NEARBY_VENDOR , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.GET_NEARBY_VENDOR , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.GET_NEARBY_VENDOR , status : Constants.STATUS_0)
        }
    }
    
    static func sendRequest(serviceId:String,note: String ,lat:String ,lng:String ,address:String ,requestTime:String ,range:String ,responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            let urlString = Urls.CLOUD_BASE_URL+Urls.SEND_REQUEST
            print("API Url = \(urlString)")
            let params : [String : Any] = ["request" : ["service_id":serviceId,"service_note":note,"user_id":SharedClass.user!._id,"lat":lat, "lng":lng,"address":address,"request_time":requestTime,"range":range,"auth_key":SharedClass.user!.auth_key]]
            print("Params : \(params)")
            let headers : HTTPHeaders = [
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
            
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .post,parameters: params, encoding:JSONEncoding.default, headers: headers).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.SEND_REQUEST , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.SEND_REQUEST , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.SEND_REQUEST , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.SEND_REQUEST , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.SEND_REQUEST , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.SEND_REQUEST , status : Constants.STATUS_0)
        }
    }
    
    static func updateRequestStatus(requestId:String ,vendorId:String ,requestStatus:String ,responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            var urlString = Urls.CLOUD_BASE_URL+Urls.UPDATE_REQUEST_STATUS+"/"+requestId+"/"+vendorId+"/"+requestStatus.description+"/"+SharedClass.user!.auth_key
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.UPDATE_REQUEST_STATUS , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.UPDATE_REQUEST_STATUS , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.UPDATE_REQUEST_STATUS , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.UPDATE_REQUEST_STATUS , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.UPDATE_REQUEST_STATUS , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.UPDATE_REQUEST_STATUS , status : Constants.STATUS_0)
        }
    }
    
    static func cancelRequest(requestId:String ,responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            var urlString = ("\(Urls.CLOUD_BASE_URL+Urls.CANCEL_REQUEST+requestId)/\(SharedClass.user._id!)/\(SharedClass.user.auth_key!)")
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["message"] as! String, resonseType: Constants.API,apiName: Urls.CANCEL_REQUEST , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.CANCEL_REQUEST , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.CANCEL_REQUEST , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.CANCEL_REQUEST , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.CANCEL_REQUEST , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.CANCEL_REQUEST , status : Constants.STATUS_0)
        }
    }
    
    static func setTripRoute(tripId:String ,routeObj: AnyObject ,responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            let urlString = Urls.CLOUD_BASE_URL+Urls.SET_TRIP_ROUTE
            print("API Url = \(urlString)")
            let params : [String : Any] = ["path" : routeObj,"tripId":tripId,"id":SharedClass.user._id!,"auth_key":SharedClass.user.auth_key!]
            print("Params : \(params)")
            let headers : HTTPHeaders = [
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
            Alamofire.request(urlString,method: .post,parameters: params, encoding:JSONEncoding.default, headers: headers).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["message"] as! String, resonseType: Constants.API,apiName: Urls.SET_TRIP_ROUTE , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.SET_TRIP_ROUTE , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.SET_TRIP_ROUTE , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.SET_TRIP_ROUTE , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.SET_TRIP_ROUTE , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.SET_TRIP_ROUTE , status : Constants.STATUS_0)
        }
    }
    
    static func getAllServices(responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            var urlString = ("\(Urls.CLOUD_BASE_URL+Urls.GET_ALL_SERVICES+SharedClass.user._id!)/\(SharedClass.user.auth_key!)")
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.GET_ALL_SERVICES , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.GET_ALL_SERVICES , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.GET_ALL_SERVICES , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.GET_ALL_SERVICES , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.GET_ALL_SERVICES , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.GET_ALL_SERVICES , status : Constants.STATUS_0)
        }
    }
    
    static func changeVendorStatus(vendorId:String ,status:Bool ,responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            var urlString = ("\(Urls.CLOUD_BASE_URL+Urls.CHANGE_VENDOR_STATUS)/\(vendorId)/\(status)/\(SharedClass.user.auth_key!)")
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["message"] as! String, resonseType: Constants.API,apiName: Urls.CHANGE_VENDOR_STATUS , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.CHANGE_VENDOR_STATUS , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.CHANGE_VENDOR_STATUS , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.CHANGE_VENDOR_STATUS , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.CHANGE_VENDOR_STATUS , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.CHANGE_VENDOR_STATUS , status : Constants.STATUS_0)
        }
    }
    
    static func getPreviousTrips(type:String ,id:String ,authKey:String ,responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            var urlString = ("\(Urls.CLOUD_BASE_URL+Urls.GET_PREVIOUS_TRIPS+type)/\(id)/\(authKey)")
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.GET_PREVIOUS_TRIPS , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.GET_PREVIOUS_TRIPS , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.GET_PREVIOUS_TRIPS , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.GET_PREVIOUS_TRIPS , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.GET_PREVIOUS_TRIPS , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.GET_PREVIOUS_TRIPS , status : Constants.STATUS_0)
        }
    }
    
    static func getVendor(vendorId:String , responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            var urlString = ("\(Urls.CLOUD_BASE_URL+Urls.GET_VENDOR+vendorId)/\(SharedClass.user._id!)/\(SharedClass.user.auth_key!)")
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.GET_VENDOR , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.GET_VENDOR , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.GET_VENDOR , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.GET_VENDOR , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.GET_VENDOR , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.GET_VENDOR , status : Constants.STATUS_0)
        }
    }
    
    static func getUser(userId:String , responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            var urlString = ("\(Urls.CLOUD_BASE_URL+Urls.GET_USER+userId)/\(SharedClass.user._id!)/\(SharedClass.user.auth_key!)")
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.GET_USER , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.GET_USER , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.GET_USER , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.GET_USER , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.GET_USER , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.GET_USER , status : Constants.STATUS_0)
        }
    }
    
    static func logout(type:String ,id:String ,responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            var urlString = "\(Urls.CLOUD_BASE_URL + Urls.LOGOUT)/\(id)/\(type)/\(SharedClass.user!.auth_key!)"
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["message"] as! String, resonseType: Constants.API,apiName: Urls.LOGOUT , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.LOGOUT , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.LOGOUT , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.LOGOUT , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.LOGOUT , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.LOGOUT , status : Constants.STATUS_0)
        }
    }
    
    static func editProfile(firstName:String ,lastname:String ,phone:String ,profileLink:String ,responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            let urlString = Urls.CLOUD_BASE_URL+Urls.EDIT_PROFILE
            print("API Url = \(urlString)")
            let params : [String : Any] = ["user" : ["first_name":firstName,"last_name":lastname,"phone":phone,"profile_pic":profileLink, "id":SharedClass.user!._id!,"auth_key":SharedClass.user!.auth_key!, "type":SharedClass.accountType.stringValue]]
            print("Params : \(params)")
            let headers : HTTPHeaders = [
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
            Alamofire.request(urlString,method: .post,parameters: params, encoding:JSONEncoding.default, headers: headers).responseJSON
                {
                    (response) in
                    print("JSCON RESPONSE \(response)")
                    switch response.result
                    {
                    case .success(let JSON):
                        do {
                            let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                            print("Response : \(json)")
                            if json!["status"] as! Int! == Constants.STATUS_200
                            {
                                responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.EDIT_PROFILE , status : Constants.STATUS_0)
                            }
                            else if json!["status"] as! Int! == Constants.STATUS_300
                            {
                                responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.EDIT_PROFILE , status : Constants.STATUS_300)
                            }
                            else
                            {
                                responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.EDIT_PROFILE , status : Constants.STATUS_500)
                            }
                        } catch let error {
                            responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.EDIT_PROFILE , status : Constants.STATUS_500)
                        }
                    case .failure(let error):
                        responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.EDIT_PROFILE , status : Constants.STATUS_0)
                    }
            }
        }
        else
        {
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.EDIT_PROFILE , status : Constants.STATUS_0)
        }
    }
    
    static func uploadImage(id:String ,file: UIImage ,responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            let urlString = Urls.CLOUD_BASE_URL+Urls.UPLOAD_IMAGE
            let params : [String : Any] = ["id" : SharedClass.user!._id!]
            let headers: HTTPHeaders = [
                "Content-type": "multipart/form-data"
            ]
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in params
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                multipartFormData.append(UIImageJPEGRepresentation(file, 0.5)!, withName: "img", fileName: "image.png", mimeType: "image/png")
                
            }, to:urlString,method: .post, headers: headers)
            { (result) in
                switch result {
                case .success(let upload,_,_ ):
                    upload.uploadProgress(closure: { (progress) in
                        //Print progress
                        let total : Int = Int(progress.totalUnitCount)
                        let completed : Int = Int(progress.completedUnitCount)
                        let percent : Float = ( Float(completed) / Float(total) ) * 100.0
                        print("Uploading : \(percent) %")
                    })
                    upload.responseJSON
                        { response in
                            switch response.result
                            {
                            case .success(let JSON):
                                do {
                                    let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                    print("Response : \(json)")
                                    if json!["status"] as! Int! == Constants.STATUS_200
                                    {
                                        responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.UPLOAD_IMAGE , status : Constants.STATUS_0)
                                    }
                                    else if json!["status"] as! Int! == Constants.STATUS_300
                                    {
                                        responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.UPLOAD_IMAGE , status : Constants.STATUS_300)
                                    }
                                    else
                                    {
                                        responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.UPLOAD_IMAGE , status : Constants.STATUS_500)
                                    }
                                } catch let error {
                                    responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.UPLOAD_IMAGE , status : Constants.STATUS_500)
                                }
                            case .failure(let error):
                                responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.UPLOAD_IMAGE , status : Constants.STATUS_0)
                            }
                    }
                case .failure(let encodingError):
                    print("Error : \(encodingError)")
                    break
                }
            }
        }
        else
        {
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.UPLOAD_IMAGE , status : Constants.STATUS_0)
        }
    }
    
    static func changePassword(oldpassword:String, newpassword:String ,responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            var urlString = "\(Urls.CLOUD_BASE_URL + Urls.CHANGE_PASSWORD)\(SharedClass.user!._id!)/\(oldpassword)/\(newpassword)/\(SharedClass.accountType.stringValue)/\(SharedClass.user!.auth_key!)"
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["message"] as! String, resonseType: Constants.API,apiName: Urls.CHANGE_PASSWORD , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.CHANGE_PASSWORD , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.CHANGE_PASSWORD , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.CHANGE_PASSWORD , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.CHANGE_PASSWORD , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.CHANGE_PASSWORD , status : Constants.STATUS_0)
        }
    }
    
    static func setRating(model:TripModel, rating:String,review:String, responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            let urlString = Urls.CLOUD_BASE_URL+Urls.SET_RATING
            print("API Url = \(urlString)")
            
            var byId : String! = "";
            var forId : String! = "";
            if  SharedClass.accountType == Constants.END_USER  {
                byId = SharedClass.user!._id!
                forId = model.vendor_id!
            } else {
                byId = SharedClass.user!._id!
                forId = model.user_id!
            }
            
            let params : [String : Any] = ["rate":["trip_id":"\(model._id!)",  "by_id":byId, "for_id":forId,"type":"\(SharedClass.accountType)", "auth_key":SharedClass.user!.auth_key!, "date":"\(Helper.getCurrentMillis().description)", "rating":"\(rating)", "review":"\(review)"]]
            print("Params : \(params)")
            let headers : HTTPHeaders = [
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
            Alamofire.request(urlString,method: .post,parameters: params, encoding:JSONEncoding.default, headers: headers).responseJSON
                {
                    (response) in
                    print("JSCON RESPONSE \(response)")
                    switch response.result
                    {
                    case .success(let JSON):
                        do {
                            let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                            print("Response : \(json)")
                            if json!["status"] as! Int! == Constants.STATUS_200
                            {
                                responceCallback.onSuccessResponce(obj: json!["message"] as! String, resonseType: Constants.API,apiName: Urls.SET_RATING , status : Constants.STATUS_0)
                            }
                            else if json!["status"] as! Int! == Constants.STATUS_300
                            {
                                responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.SET_RATING , status : Constants.STATUS_300)
                            }
                            else
                            {
                                responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.SET_RATING , status : Constants.STATUS_500)
                            }
                        } catch let error {
                            responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.SET_RATING , status : Constants.STATUS_500)
                        }
                    case .failure(let error):
                        responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.SET_RATING , status : Constants.STATUS_0)
                    }
            }
        }
        else
        {
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.SET_RATING , status : Constants.STATUS_0)
        }
    }
    
    static func getNearestVendor(lat:String ,lng:String ,range:String ,responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            var urlString = "\(Urls.CLOUD_BASE_URL + Urls.GET_NEAREST_VENDOR)/\(lat)/\(lng)/\(range)/\(SharedClass.user!._id!)/\(SharedClass.user!.auth_key!)"
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.GET_NEAREST_VENDOR , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.GET_NEAREST_VENDOR , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.GET_NEAREST_VENDOR , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.GET_NEAREST_VENDOR , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.GET_NEAREST_VENDOR , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.GET_NEAREST_VENDOR , status : Constants.STATUS_0)
        }
    }
    
    static func forgetPassword(email:String ,type:String ,responceCallback :ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            var urlString = "\(Urls.CLOUD_BASE_URL + Urls.FORGET_PASSWORD)\(email)/\(type)"
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.FORGET_PASSWORD , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.FORGET_PASSWORD , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.FORGET_PASSWORD , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.FORGET_PASSWORD , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.FORGET_PASSWORD , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.FORGET_PASSWORD , status : Constants.STATUS_0)
        }
    }
    
    static func codeValidation(email:String ,type:String ,code:String ,responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            var urlString = "\(Urls.CLOUD_BASE_URL + Urls.CODE_VALIDATION)\(email)/\(code)/\(type)"
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.CODE_VALIDATION , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.CODE_VALIDATION , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.CODE_VALIDATION , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.CODE_VALIDATION , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.CODE_VALIDATION , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.CODE_VALIDATION , status : Constants.STATUS_0)
        }
    }
    
    static func updatePassword(email:String ,type:String ,password:String ,responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            var urlString = "\(Urls.CLOUD_BASE_URL + Urls.UPDATE_PASSWORD)\(email)/\(type)/\(password)"
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.UPDATE_PASSWORD , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.UPDATE_PASSWORD , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.UPDATE_PASSWORD , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.UPDATE_PASSWORD , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.UPDATE_PASSWORD , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.UPDATE_PASSWORD , status : Constants.STATUS_0)
        }
    }
    
    static func updateService(id:String , url:String , responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            var urlString = "\(Urls.CLOUD_BASE_URL + Urls.UPDATE_SERVICE)"
            
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            let params = ["url" : ["id":id,"long_url":url]]
            print("Params : \(params)")
            let headers : HTTPHeaders = [
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]

            Alamofire.request(urlString,method: .post,parameters: params, encoding:JSONEncoding.default, headers: headers).responseJSON
                {
                    response in switch response.result
                    {
                        case .success(let JSON):
                            do {
                                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                                print("Response : \(json)")
                                if json!["status"] as! Int! == Constants.STATUS_200
                                {
                                    responceCallback.onSuccessResponce(obj: json!["message"] as! String, resonseType: Constants.API,apiName: Urls.UPDATE_SERVICE , status : Constants.STATUS_0)
                                }
                                else if json!["status"] as! Int! == Constants.STATUS_300
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.UPDATE_SERVICE , status : Constants.STATUS_300)
                                }
                                else
                                {
                                    responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.UPDATE_SERVICE , status : Constants.STATUS_500)
                                }
                            } catch let error {
                                responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.UPDATE_SERVICE , status : Constants.STATUS_500)
                            }
                        case .failure(let error):
                            responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.UPDATE_SERVICE , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.UPDATE_SERVICE , status : Constants.STATUS_0)
        }
    }
    
    static func verifyNumber(type: String, phone:String, responceCallback : ResponceCallback)
    {
        if Helper.isNetConnected()
        {
            let urlString = Urls.CLOUD_BASE_URL+Urls.VERIFY_MOBILE+"/"+type+"/"+phone
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                    case .success(let JSON):
                        do {
                            let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                            print("Response : \(json)")
                            if json!["status"] as! Int! == Constants.STATUS_200
                            {
                                responceCallback.onSuccessResponce(obj: json!["message"] as! String, resonseType: Constants.API,apiName: Urls.VERIFY_MOBILE , status : Constants.STATUS_0)
                            }
                            else if json!["status"] as! Int! == Constants.STATUS_300
                            {
                                responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.VERIFY_MOBILE , status : Constants.STATUS_300)
                            }
                            else
                            {
                                responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.VERIFY_MOBILE , status : Constants.STATUS_500)
                            }
                        } catch let error {
                            responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.VERIFY_MOBILE , status : Constants.STATUS_500)
                        }
                    case .failure(let error):
                        responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.VERIFY_MOBILE , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.VERIFY_MOBILE , status : Constants.STATUS_0)
        }
    }
    
    static func verifyCode(type: String, phone:String, code:String, responceCallback : ResponceCallback)
    {
        if Helper.isNetConnected()
        {
            let urlString = Urls.CLOUD_BASE_URL+Urls.VERIFY_CODE+"/"+type+"/"+phone+"/"+code
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                    case .success(let JSON):
                        do {
                            let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                            print("Response : \(json)")
                            if json!["status"] as! Int! == Constants.STATUS_200
                            {
                                responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.VERIFY_CODE , status : Constants.STATUS_0)
                            }
                            else if json!["status"] as! Int! == Constants.STATUS_300
                            {
                                responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.VERIFY_CODE , status : Constants.STATUS_300)
                            }
                            else
                            {
                                responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.VERIFY_CODE , status : Constants.STATUS_500)
                            }
                        } catch let error {
                            responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.VERIFY_CODE , status : Constants.STATUS_500)
                        }
                    case .failure(let error):
                        responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.VERIFY_CODE , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.VERIFY_CODE , status : Constants.STATUS_0)
        }
    }
    
    static func getPreviousChat(tripId : String, responceCallback : ResponceCallback)
    {
        if Helper.isNetConnected()
        {
            let urlString = Urls.CLOUD_BASE_URL+Urls.GET_CHAT+"/"+tripId
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                    case .success(let JSON):
                        do {
                            let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                            print("Response : \(json)")
                            if json!["status"] as! Int! == Constants.STATUS_200
                            {
                                responceCallback.onSuccessResponce(obj: json!["data"] as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.GET_CHAT , status : Constants.STATUS_0)
                            }
                            else if json!["status"] as! Int! == Constants.STATUS_300
                            {
                                responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.GET_CHAT , status : Constants.STATUS_300)
                            }
                            else
                            {
                                responceCallback.onFailureResponce(obj: json!["message"], resonseType: Constants.API,apiName: Urls.GET_CHAT , status : Constants.STATUS_500)
                            }
                        } catch let error {
                            responceCallback.onFailureResponce(obj: error as! String, resonseType: Constants.API,apiName: Urls.GET_CHAT , status : Constants.STATUS_500)
                        }
                    case .failure(let error):
                        responceCallback.onFailureResponce(obj: "Server Error Found !", resonseType: Constants.API,apiName: Urls.GET_CHAT , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.GET_CHAT , status : Constants.STATUS_0)
        }
    }
    
    // MARK: Google Server Server Calls
    static func getTimeDuration( origion:String!, destination:String!,responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            var urlString = Urls.GOOGLE_BASE_URL+Urls.GET_TIME_DURATION+"origins="+origion+"&destinations="+destination+"&sensor=true&mode=driving&key="+Constants.GOOGLE_MAP_KEY
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                    case .success(let JSON):
                        responceCallback.onSuccessResponce(obj: JSON as! NSDictionary, resonseType: Constants.API,apiName: Urls.GET_TIME_DURATION , status : Constants.STATUS_0)
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        responceCallback.onFailureResponce(obj: error as NSError, resonseType: Constants.API,apiName: Urls.GET_TIME_DURATION , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.GET_TIME_DURATION , status : Constants.STATUS_0)
        }
    }
    
    static func getAddress(lat : String ,lng : String, responceCallback : ResponceCallback)
    {
        if Helper.isNetConnected()
        {
            let urlString = Urls.GOOGLE_BASE_URL+Urls.GET_ADDRESS+"latlng="+lat+","+lng+"&sensor=true"
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                    case .success(let JSON):
                        switch JSON
                        {
                            case is String:
                                responceCallback.onFailureResponce(obj: JSON as Any, resonseType: Constants.API,apiName: Urls.GET_ADDRESS , status : Constants.STATUS_0)
                            case is Dictionary<String, AnyObject>:
                                responceCallback.onSuccessResponce(obj: JSON as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.GET_ADDRESS , status : Constants.STATUS_0)
                            default:
                                responceCallback.onSuccessResponce(obj: JSON as Any, resonseType: Constants.API,apiName: Urls.GET_ADDRESS , status : Constants.STATUS_0)
                        }
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        responceCallback.onFailureResponce(obj: error as NSError, resonseType: Constants.API,apiName: Urls.GET_ADDRESS , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.GET_ADDRESS , status : Constants.STATUS_0)
        }
    }
    
    static func getPathFromGoogle(origion : String , destination : String, showAlternativePath : Bool , responceCallback : ResponceCallback)
    {
        if Helper.isNetConnected()
        {
            var urlString = Urls.GOOGLE_BASE_URL + Urls.GET_PATH+"origin="+origion+"&destination="+destination+"&sensor=true&mode=driving&alternatives=false&key="+Constants.GOOGLE_MAP_KEY
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print("API Url = \(urlString)")
            Alamofire.request(urlString,method: .get).responseJSON
                {
                    response in switch response.result
                    {
                    case .success(let JSON):
                        switch JSON
                        {
                            case is String:
                                responceCallback.onSuccessResponce(obj: JSON as! String, resonseType: Constants.API,apiName: Urls.GET_PATH , status : Constants.STATUS_0)
                            case is Dictionary<String, AnyObject>:
                                responceCallback.onSuccessResponce(obj: JSON as! Dictionary<String, AnyObject>, resonseType: Constants.API,apiName: Urls.GET_PATH , status : Constants.STATUS_0)
                            case is NSArray:
                                responceCallback.onSuccessResponce(obj: JSON as! NSArray, resonseType: Constants.API,apiName: Urls.GET_PATH , status : Constants.STATUS_0)
                            case is Data:
                                responceCallback.onSuccessResponce(obj: JSON as! Data, resonseType: Constants.API,apiName: Urls.GET_PATH , status : Constants.STATUS_0)
                            default:
                                responceCallback.onSuccessResponce(obj: JSON as Any, resonseType: Constants.API,apiName: Urls.GET_PATH , status : Constants.STATUS_0)
                        }
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        responceCallback.onFailureResponce(obj: error as NSError, resonseType: Constants.API,apiName: Urls.GET_PATH , status : Constants.STATUS_0)
                    }
            }
        }
        else{
            responceCallback.onFailureResponce(obj: "" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.GET_PATH , status : Constants.STATUS_0)
        }
    }
    
    static func shortenUrl(url:String , responceCallback: ResponceCallback )
    {
        if Helper.isNetConnected()
        {
            let urlString = Urls.SHORTEN_URL_BASE_URL+Urls.SHORTEN_URL
            print("API Url = \(urlString)")
            let params : [String : Any] = ["longUrl": url]
            print("Params : \(params)")
            let headers : HTTPHeaders = [
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
            Alamofire.request(urlString,method: .post,parameters: params, encoding:JSONEncoding.default, headers: headers).responseJSON
                {
                    (response) in
                    print("JSCON RESPONSE \(response)")
                    switch response.result
                    {
                    case .success(let JSON):
                        switch JSON
                        {
                        case is String:
                            responceCallback.onFailureResponce(obj: JSON as Any, resonseType: Constants.API,apiName: Urls.SHORTEN_URL , status : Constants.STATUS_0)
                        case is Dictionary<String, AnyObject>:
                            responceCallback.onSuccessResponce(obj: JSON , resonseType: Constants.API,apiName: Urls.SHORTEN_URL , status : Constants.STATUS_0)
                        default:
                            responceCallback.onSuccessResponce(obj: JSON as Any, resonseType: Constants.API,apiName: Urls.SHORTEN_URL , status : Constants.STATUS_0)
                        }
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        responceCallback.onFailureResponce(obj: error as NSError, resonseType: Constants.API,apiName: Urls.SHORTEN_URL , status : Constants.STATUS_0)
                    }
            }
        }
        else
        {
            responceCallback.onFailureResponce(obj: "Internet Connection Not Found !" as Any, resonseType: Constants.NET_ISSUE,apiName: Urls.SHORTEN_URL , status : Constants.STATUS_0)
        }
        
    }
}
