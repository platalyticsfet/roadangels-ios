//
//  Urls.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 28/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import Foundation

class Urls
{
    static var SOCKET_PORT : String = "8082"
    static var CLOUD_BASE_URL : String = ""
    static var GOOGLE_BASE_URL : String = "https://maps.googleapis.com/"
    static var SHORTEN_URL_BASE_URL : String = "https://www.googleapis.com/"
    
    // MARK: GET Request
    static var LOGIN : String = "/login"
    static var GET_ALL_VENDORS : String = "/getAllVendors"
    static var SET_VENDOR_LOCATION : String = "/setVendorLocation"
    static var GET_VENDOR_LOCATION : String = "/getVendorLocation/{vendor_id}"
    static var FINISH_TRIP : String = "/finishTrip"
    static var GET_NEARBY_VENDOR : String = "/getNearbyVendors"
    static var UPDATE_REQUEST_STATUS : String = "/updateRequestStatus"
    static var CANCEL_REQUEST : String = "/cancelRequest/"
    static var GET_ALL_SERVICES : String = "/getServices/"
    static var CHANGE_VENDOR_STATUS : String = "/setVendorStatus"
    static var GET_PREVIOUS_TRIPS : String = "/getTrips/"
    static var GET_VENDOR : String = "/getVendor/"
    static var GET_USER : String = "/getUser/"
    static var LOGOUT : String = "/logout"
    static var CHANGE_PASSWORD : String = "/changePassword/"
    static var GET_NEAREST_VENDOR : String = "/getNearestVendor"
    static var FORGET_PASSWORD : String = "/forgetPassword/"
    static var CODE_VALIDATION : String = "/codeValidation/"
    static var UPDATE_PASSWORD : String = "/updatePassword/"
    static var GET_ADDRESS : String = "/maps/api/geocode/json?"
    static var GET_PATH : String = "/maps/api/directions/json?"
    static var GET_TIME_DURATION : String = "/maps/api/distancematrix/json?"
    static var UPDATE_SERVICE : String = "/updateTrip"
    static var VERIFY_MOBILE : String = "/verifyPhone"
    static var VERIFY_CODE : String = "/verifyCode"
    static var GET_CHAT : String = "/getChats"

    // MARK: POST Request
    static var CREATE_TRIP : String = "/createTrip"
    static var SEND_REQUEST : String = "/requestVendor"
    static var SET_TRIP_ROUTE : String = "/setTripRoute"
    static var REGISTER : String = "/register"
    static var EDIT_PROFILE : String = "/editProfile"
    static var UPLOAD_IMAGE : String = "/uploadImage"
    static var SET_RATING : String = "/setRating"
    static var SHORTEN_URL : String = "/urlshortener/v1/url?key=\(Constants.GOOGLE_MAP_KEY)"
    
}
