//
//  Colors.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 06/03/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import Foundation
import UIKit

class Colors
{
    static var WHITE : UIColor = UIColor.white
    static var BLACK : UIColor = UIColor.black
    static var RED : UIColor = UIColor.red
    static var CLEAR : UIColor = UIColor.clear
    static var LIGHT_GRAY : UIColor = UIColor.lightGray
    static var LIGHT_GRAY_1 : UIColor = UIColor(red:212.0/255.0,green:212.0/255.0,blue:212.0/255.0,alpha:1.0)
    static var LIGHT_GRAY_2 : UIColor = UIColor(red:242.0/255.0,green:242.0/255.0,blue:242.0/255.0,alpha:1.0)
    static var PURPLE_1 : UIColor = UIColor(red:66.0/255.0, green:69.0/255.0, blue:112.0/255.0, alpha:1.0)
    static var DARK_GREEN : UIColor = UIColor(red:0.0/255.0,green:128.0/255.0,blue:0.0/255.0,alpha:1.0)
    static var GRAY_1 : UIColor = UIColor(red:130.0/255.0, green:147.0/255.0, blue:168.0/255.0, alpha:1.0)
    static var THEME_BLUE : UIColor = UIColor(red:40.0/255.0, green:47.0/255.0, blue:147.0/255.0, alpha:1.0)
    static var DARK_GRAY : UIColor = UIColor(red:54.0/255.0,green:63.0/255.0,blue:69.0/255.0,alpha:1.0)
    static var TRANSPARENT : UIColor = UIColor(red:0.0/255.0,green:0.0/255.0,blue:0.0/255.0,alpha:0.0)
    static var LIGHT_GREEN : UIColor = UIColor(red:60.0/255.0,green:179.0/255.0,blue:113.0/255.0,alpha:1.0)
    static var LIGHT_PURPLE : UIColor = UIColor(red:204.0/255.0,green:207.0/255.0,blue:247.0/255.0,alpha:1.0)
    
    static var DARK_RED : UIColor = UIColor(red:255.0/255.0,green:69.0/255.0,blue:0.0/255.0,alpha:1.0)
    
    
    
}
