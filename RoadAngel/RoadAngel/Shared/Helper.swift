//
//  Helper.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 28/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import Foundation
import UIKit
import SwiftMessages
import SwiftOverlays

class Helper
{
    static func showYesNoDialog(viewController: UIViewController,title : String, message : String,responseCallback : ResponceCallback) -> Void
    {
        let refreshAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            responseCallback.onSuccessResponce(obj: "" as NSObject,resonseType: Constants.DIALOG,apiName: "" , status : Constants.STATUS_0)
        }))
        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
            responseCallback.onFailureResponce(obj: "" as NSObject, resonseType: Constants.DIALOG,apiName: "" , status : Constants.STATUS_0)
        }))
        viewController.present(refreshAlert, animated: true, completion: nil)
    }
    
    static func showAlertDialog(viewController: UIViewController,title : String, message : String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    static func showSuccessDialog(title : String, message : String , duration : Double)
    {
        var config = SwiftMessages.Config()
        config.duration = .seconds(seconds: duration)
        let view = MessageView.viewFromNib(layout:.cardView)
        view.configureTheme(.success)
        view.configureDropShadow()
        view.button?.isHidden = true
        view.configureContent(title: title, body: message)
        SwiftMessages.show(config: config,view: view)
    }
    
    static func showFailureDialog(title : String, message : String , duration : Double)
    {
        var config = SwiftMessages.Config()
        config.duration = .seconds(seconds: duration)
        let view = MessageView.viewFromNib(layout:.cardView)
        view.configureTheme(.error)
        view.configureDropShadow()
        view.button?.isHidden = true
        view.configureContent(title: title, body: message)
        SwiftMessages.show(config: config,view: view)
    }
    
    static func showLoadingDialog(message : String)
    {
        SwiftOverlays.showBlockingWaitOverlayWithText(message)
    }
    
    static func hideLoadingDialog()
    {
        SwiftOverlays.removeAllBlockingOverlays()
    }
    
    static func isNetConnected() -> Bool
    {
        if Reachability.isConnectedToNetwork(){
            return true
        }else{
            return false
        }
    }
    
    static func getDistance(source: LatLng , vendor : LatLng ) -> Double
    {
        let lat1 : Double = source.latitude
        let lat2 : Double = vendor.latitude
        let lon1 : Double = source.longitude
        let lon2 : Double = vendor.longitude
        let theta : Double = lon1 - lon2
        var dist : Double = sin(deg2rad(deg: lat1))
        * sin(deg2rad(deg:lat2))
        + cos(deg2rad(deg:lat1))
        * cos(deg2rad(deg:lat2))
        * cos(deg2rad(deg:theta))
        dist = acos(dist)
        dist = rad2deg(rad:dist)
        dist = dist * 60 * 1.1515
        return dist
    }
    
    static func deg2rad(deg:Double) -> Double {
        return (deg * Double.pi / 180.0);
    }
    
    static func rad2deg(rad:Double) -> Double {
        return (rad * 180.0 / Double.pi);
    }
    
    static func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in: CGRect(x:0, y:0, width:newSize.width, height:newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    static func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    static func showToast(controller : UIViewController, message : String) {
        let toastLabel = UILabel(frame: CGRect(x: controller.view.frame.size.width/2 - 75, y: controller.view.frame.size.height-100, width: 170, height: 40))
        toastLabel.backgroundColor = Colors.BLACK.withAlphaComponent(0.6)
        toastLabel.textColor = Colors.WHITE
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Calibri", size: 18.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        controller.view.addSubview(toastLabel)
        UIView.animate(withDuration: 0.5, delay: 2.0, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    static func ShowErrorSneakbar(controller : UIViewController, message : String) {
        let toastLabel = UILabel(frame: CGRect(x: 0, y: controller.view.frame.size.height-40, width: controller.view.frame.size.width, height: 40))
        toastLabel.backgroundColor = Colors.RED
        toastLabel.textColor = Colors.WHITE
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Calibri", size: 16.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.clipsToBounds  =  true
        controller.view.addSubview(toastLabel)
        UIView.animate(withDuration: 0.5, delay: 2.0, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    static func ShowSuccessSneakbar(controller : UIViewController, message : String) {
        let toastLabel = UILabel(frame: CGRect(x: 0, y: controller.view.frame.size.height-40, width: controller.view.frame.size.width, height: 40))
        toastLabel.backgroundColor = Colors.DARK_GREEN
        toastLabel.textColor = Colors.WHITE
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Calibri", size: 16.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.clipsToBounds  =  true
        controller.view.addSubview(toastLabel)
        UIView.animate(withDuration: 0.5, delay: 2.0, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    static func getFormatedData_dd_MM_yyyy__hh_mm__AA(dateInMilliseconds : CLong) -> String
    {
        let date = Date(timeIntervalSince1970: TimeInterval(dateInMilliseconds/1000))
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd/MM/yyyy hh:mm a"  //""MMM dd YYYY hh:mm a"
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        print( "Date is \(dateString)")
        return dateString
    }
    
    static func getFormatedData_MMMM_DD_YYYY_HH_MM_A(dateInMilliseconds : CLong) -> String
    {
        let date = Date(timeIntervalSince1970: TimeInterval(dateInMilliseconds/1000))
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM dd, yyyy hh:mm a"  //""MMM dd YYYY hh:mm a"
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        print( "Date is \(dateString)")
        return dateString
    }
    
    static func getFormatedData_HH_mm_ss(dateInMilliseconds : CLong) -> String
    {
        let date = Date(timeIntervalSince1970: TimeInterval(dateInMilliseconds/1000))
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "hh:mm a"  //""MMM dd YYYY hh:mm a"
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        print( "Date is \(dateString)")
        return dateString
    }
    
    static func makeGoogleImageURL (tripModel:TripModel) -> String{
        
        let imageString : String = "https://maps.googleapis.com/maps/api/staticmap?size=600x250&format=png&sensor=false&markers=color:0xfe9f00%7C\(tripModel.source_lat_lng!)&markers=color:0x3CB371%7C\(tripModel.destination_lat_lng!)&maptype=roadmap&style=element:labels|visibility:off&path=weight:4%7Ccolor:0xd68c0e%7Cenc:\(tripModel.trip_path!)"
        
        return imageString
    }
    
}

