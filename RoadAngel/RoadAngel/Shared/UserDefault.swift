//
//  UserDefaults.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 01/01/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import Foundation

class UserDefault
{
    static let defaults:UserDefaults = UserDefaults.standard
    
    private static var PREF_USER : String = "try_pref_user";
    private static var LATITUDE : String  = "latitude";
    private static var LONGITUDE : String  = "longitude";
    private static var EMAIL : String  = "email";
    private static var PASSWORD : String  = "password";
    private static var LOGIN_AS : String  = "loginAs";
    private static var INDEX : String  = "index";
    
    static func saveUserLocation(lat:String , lng:String)
    {
        defaults.set(lat, forKey: LATITUDE)
        defaults.set(lng, forKey: LONGITUDE)
    }
    
    static func saveEmailAddress(email:String)
    {
        defaults.set(email, forKey: EMAIL)
    }
    
    static func savePassword(password:String)
    {
        defaults.set(password, forKey: PASSWORD)
    }
    
    static func loginAs()
    {
        defaults.set(SharedClass.accountType.intValue , forKey: LOGIN_AS)
    }
    
    public static func saveIndex(index : Int) {
        defaults.set(index,forKey: INDEX )
    }
    
    static func getPassword() -> String
    {
        if defaults.object(forKey: PASSWORD ) != nil
        {
            return defaults.string(forKey: PASSWORD )!
        }
        return ""
    }
    
    static func getEmailAddress() -> String
    {
        if defaults.object(forKey: EMAIL ) != nil
        {
            return defaults.string(forKey: EMAIL )!
        }
        return ""
    }
    
    static func getUserLatitude() -> String
    {
        if defaults.object(forKey: LATITUDE ) != nil
        {
            return defaults.string(forKey: LATITUDE )!
        }
        return ""
    }
    
    static func getUserLongitude() -> String
    {
        if defaults.object(forKey: LONGITUDE ) != nil
        {
            return defaults.string(forKey: LONGITUDE )!
        }
        return ""
    }
    
    static func getLoginAs() -> Int
    {
        if defaults.object(forKey: LOGIN_AS ) != nil
        {
            return defaults.integer(forKey: LOGIN_AS )
        }
        return -1
    }
    
    public static func getIndex() -> Int
    {
        if defaults.object(forKey: INDEX ) != nil
        {
            return defaults.integer(forKey: INDEX)
        }
        return 0
    }
    
    static func clearAll()
    {
        defaults.removeObject(forKey: INDEX)
        defaults.removeObject(forKey: EMAIL)
        defaults.removeObject(forKey: PASSWORD)
    }
}
