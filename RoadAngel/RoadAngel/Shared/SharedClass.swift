//
//  SharedClass.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 23/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import Foundation

class SharedClass {
    
    //MARK: Properties
    static var latitude : Double = 0.0
    static var longitude : Double = 0.0
    static var address : String?;
    static var accountType : NSNumber = 1;
    static var user : User! = nil
    static var isInTestingMode : Bool?;
    static var colors = [ColorsModel]()
    static var allServices = [ServicesModel]()

    static func clearAll() {
        user = nil;
        allServices.removeAll()
        accountType = 1;
    }
    
}
