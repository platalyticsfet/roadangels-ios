//
//  Constants.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 23/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import Foundation

class Constants
{
    static var GOOGLE_MAP_KEY : String = "AIzaSyD09rZjHmYusaOGhweGZ7wvkZOXsThsh00"
    static var TRIP : NSNumber = 0
    static var END_USER : NSNumber = 1
    static var VENDOR : NSNumber = 2
    static var API : Int = 3
    static var DIALOG : Int = 4
    static var NET_ISSUE : Int = 5
    static var JSONARRAY_TYPE : NSNumber = 6
    static var JSONOBJ_TYPE : NSNumber = 7
    static var AUTH_ERROR : NSNumber = 8
    static var STRING : NSNumber = 9
    static var ERROR_RESPONSE : String = "ErrorOccured"
    static var ERROR : String = "error"
    static var DATA : String  = "data"
    static var SOCKET_EXCEPTION : String  = "java.net.SocketException"
    static var SOCKET_TIME_EXCEPTION : String  = "java.net.SocketTimeoutException"
    static var SOCKET_EVENT_CONNECTED : String  = "eventConnected"
    static var SEND_VENDOR_LOCATION : String  = "sendVendorLocation"
    static var SERVICE_REQUEST : String  = "serviceRequest"
    static var ACCEPT_REQUEST : String  = "acceptRequest"
    static var CANCEL_REQUEST : String  = "cancelRequest"
    static var HIDE_REQUEST : String  = "hideRequest"
    static var FINISH_TRIP : String  = "finishTrip"
    static var TRIP_ROUTE : String  = "tripRoute"
    static var GET_VENDOR_LOCATION : String  = "getVendorLocation"
    static var VENDOR_LOCATION : String  = "vendorLocation"
    static var USER_LOCATION : String  = "userLocation"
    static var TEST_SOCKET : String  = "testSocket"
    static var PATH : String  = "path"
    static var CONNECTION_ALIVE : String  = "connectionAlive"
    static var SEND_REQUEST_TO_VENDOR : String  = "sendRequestToVendor"
    static var AUTHENTICATION_ERROR : String  = "Authentication Failed"
    static var RECIEVE_NEW_MESSAGE : String  = "ReciveNewMessage"
    static var SEND_NEW_MESSAGE : String  = "SendNewMessage"

    static var STATUS_0 : Int = 0;
    static var STATUS_200 : Int = 200;   // Success
    static var STATUS_300 : Int = 300;   // Authentication Error
    static var STATUS_400 : Int = 400;   // Something Not Found
    static var STATUS_500 : Int = 500;   // Failure
}
