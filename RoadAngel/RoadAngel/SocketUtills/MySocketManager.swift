//
//  SocketManager.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 01/01/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import Foundation
import SocketIO

var socketManager : AnyObject?
var socket : SocketIOClient?
var listener : SocketMessageListerner?

class MySocketManager
{

    func setListener( mylistener : SocketMessageListerner)
    {
        listener = mylistener
    }
    
    func connectSocketManager()
    {
        socketManager = SocketManager(socketURL: URL(string: Urls.CLOUD_BASE_URL)!, config: [.log(false), .compress, .forceNew(false), .reconnects(true), .secure(false), .reconnectWait(1000)])
        socket = socketManager?.defaultSocket
        
        socket?.on(clientEvent: .connect) {data, ack in
            print("Socket : socket connected")
        }
        
        socket?.on(clientEvent: .disconnect) {data, ack in
            print("Socket : socket dis-connected")
        }
        
        socket?.on(clientEvent: .error) {data, ack in
            //print("Socket : socket error")
        }
        
        socket?.on(Constants.SOCKET_EVENT_CONNECTED) {data, ack in
            print("Socket : "+Constants.SOCKET_EVENT_CONNECTED)
        }
        
        socket?.on(Constants.CONNECTION_ALIVE) {data, ack in
            //print("Socket : "+Constants.CONNECTION_ALIVE)
        }
        
        socket?.on(Constants.SERVICE_REQUEST) {data, ack in
            print("Socket : "+Constants.SERVICE_REQUEST)
            if SharedClass.accountType == Constants.VENDOR && listener != nil
            {
                listener?.onGetMessageAck(event: Constants.SERVICE_REQUEST, objectType: Constants.JSONARRAY_TYPE.intValue, obj: data)
            }
        }
        
        socket?.on(Constants.ACCEPT_REQUEST) {data, ack in
            print("Socket : "+Constants.ACCEPT_REQUEST)
            if listener != nil
            {
                listener?.onGetMessageAck(event: Constants.ACCEPT_REQUEST, objectType: Constants.JSONARRAY_TYPE.intValue, obj: data)
            }
        }
        
        socket?.on(Constants.CANCEL_REQUEST) {data, ack in
            print("Socket : "+Constants.CANCEL_REQUEST)
            if SharedClass.accountType == Constants.VENDOR && listener != nil
            {
                listener?.onGetMessageAck(event: Constants.CANCEL_REQUEST, objectType: Constants.JSONARRAY_TYPE.intValue, obj: data)
            }
        }
        
        socket?.on(Constants.FINISH_TRIP) {data, ack in
            print("Socket : "+Constants.FINISH_TRIP)
            if SharedClass.accountType == Constants.END_USER && listener != nil
            {
                listener?.onGetMessageAck(event: Constants.FINISH_TRIP, objectType: Constants.JSONOBJ_TYPE.intValue, obj: data)
            }
        }
        
        socket?.on(Constants.TRIP_ROUTE) {data, ack in
            print("Socket : "+Constants.TRIP_ROUTE)
            if SharedClass.accountType == Constants.END_USER && listener != nil
            {
                listener?.onGetMessageAck(event: Constants.TRIP_ROUTE, objectType: Constants.STRING.intValue, obj: data)
            }
        }
        
        socket?.on(Constants.GET_VENDOR_LOCATION) {data, ack in
            print("Socket : "+Constants.GET_VENDOR_LOCATION)
            if SharedClass.accountType == Constants.END_USER && listener != nil
            {
                listener?.onGetMessageAck(event: Constants.GET_VENDOR_LOCATION, objectType: Constants.JSONOBJ_TYPE.intValue, obj: data)
            }
        }
        
        socket?.on(Constants.HIDE_REQUEST) {data, ack in
            print("Socket : "+Constants.HIDE_REQUEST)
            if SharedClass.accountType == Constants.VENDOR && listener != nil
            {
                listener?.onGetMessageAck(event: Constants.HIDE_REQUEST, objectType: Constants.JSONOBJ_TYPE.intValue, obj: data)
            }
        }
        
        socket?.on(Constants.RECIEVE_NEW_MESSAGE) {data, ack in
            print("Socket : "+Constants.RECIEVE_NEW_MESSAGE)
            if listener != nil
            {
                listener?.onGetMessageAck(event: Constants.RECIEVE_NEW_MESSAGE, objectType: Constants.JSONOBJ_TYPE.intValue, obj: data)
            }
        }
        
        socket?.on(Constants.ERROR_RESPONSE) {data, ack in
            print("Socket : "+Constants.ERROR_RESPONSE)
            let arr = data as NSArray
            let dic = arr[0] as! Dictionary<String,AnyObject>
            let err = dic["status"] as! String
            print("Error : "+err)
            
            
        }
        
        socket?.connect()
    }
    
    func socketDisconnect()
    {
        socket?.disconnect()
    }
    
    func SendVendorPosition(vendorId:String ,latitude:String , longitude:String)
    {
        if socket == nil
        {
            return;
        }
        var dictionary = [String:AnyObject]()
        dictionary["vendor_id"] = vendorId as AnyObject
        dictionary["latitude"] = latitude as AnyObject
        dictionary["longitude"] = longitude as AnyObject
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
            socket?.emitWithAck(Constants.SEND_VENDOR_LOCATION, jsonData).timingOut(after: 0)
            {data in
                //socket.emit("update", ["amount": cur + 2.50])
            }
        } catch let error {
            print("Couldn't create json data: \(error)");
        }
    }
    
    func SendChatMessage(senderId : String,recieverId : String,authKey : String, message : String,date :String,tripId : String) {
        if socket == nil
        {
            return;
        }
        var dictionary = [String:AnyObject]()
        dictionary["to_id"] = recieverId as AnyObject
        dictionary["from_id"] = senderId as AnyObject
        dictionary["auth_key"] = authKey as AnyObject
        dictionary["type"] = SharedClass.accountType as AnyObject
        dictionary["message"] = message as AnyObject
        dictionary["date"] = date as AnyObject
        dictionary["trip_id"] = tripId as AnyObject
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
            socket?.emitWithAck(Constants.SEND_NEW_MESSAGE, jsonData).timingOut(after: 0)
            {data in
                //socket.emit("update", ["amount": cur + 2.50])
            }
        } catch let error {
            print("Couldn't create json data: \(error)");
        }
    }
    
    func SendRequestToVendor(requestId:String ,serviceId:String,serviceNote:String ,userId: String ,lat:String ,lng:String ,address:String ,requestTime:String ,range:String )
    {
        if socket == nil
        {
            return;
        }
        var dictionary = [String:AnyObject]()
        dictionary["request_id"] = requestId as AnyObject
        dictionary["service_id"] = serviceId as AnyObject
        dictionary["service_note"] = serviceNote as AnyObject
        dictionary["user_id"] = userId as AnyObject
        dictionary["lat"] = lat as AnyObject
        dictionary["lng"] = lng as AnyObject
        dictionary["address"] = address as AnyObject
        dictionary["request_time"] = requestTime as AnyObject
        dictionary["range"] = range as AnyObject
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
            socket?.emitWithAck(Constants.SEND_REQUEST_TO_VENDOR, jsonData).timingOut(after: 0)
            {data in
                //socket.emit("update", ["amount": cur + 2.50])
            }
        } catch let error {
            print("Couldn't create json data: \(error)");
        }
        

        
        
    }
    
    func KeepConnectionAlive()
    {
        if socket == nil
        {
            return;
        }
        var dictionary = [String:AnyObject]()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
            socket?.emitWithAck(Constants.CONNECTION_ALIVE, jsonData).timingOut(after: 0)
            {data in
                
            }
        } catch let error {
            print("Couldn't create json data: \(error)");
        }
    }
    
    func isConnected() -> Bool {
        return (socket?.status.active)!
    }
}
