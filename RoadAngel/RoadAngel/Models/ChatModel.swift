//
//  ChatModel.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 28/02/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import Foundation

struct ChatModel:Decodable {
    
    var _id : String!
    var message : String!
    var date : String!
    var From : UserInfo!
    var To : UserInfo!
    var __v : Int64!
    var trip_id : String!
    
    init()
    {
        
    }
    
    init(id:String, message:String , date:String , user_name:String ,user_id:String ,user_pic:String ,tripId:String )
    {
        self._id = id
        self.message = message
        self.date = date
        self.From = UserInfo()
        self.From.id = user_id;
        self.From.name = user_name;
        self.From.pic = user_pic;
        self.trip_id = tripId
    }
    
    struct UserInfo : Decodable {
        var pic : String!
        var name : String!
        var id : String!
    }
}
