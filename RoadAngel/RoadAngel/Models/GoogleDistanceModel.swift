//
//  GoogleDistanceModel.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 23/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import Foundation

struct GoogleDistanceModel : Decodable
{
    var destination_addresses : [String]
    var origin_addresses : [String]
    var rows : [Row]
    var status : String!
    
    struct Row : Decodable
    {
        var elements : [Element]
        
        struct Element : Decodable
        {
            var distance : Distance!
            var duration : Duration!
            var status : String!
            
            struct Distance : Decodable
            {
                var text : String!
                var value : Int64!
            }
            
            struct Duration : Decodable
            {
                var text : String!
                var value : Int64!
            }
        }
    }
}
