//
//  GoogleAddressModel.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 23/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import Foundation

struct GoogleAddressModel : Decodable
{
    var results : [Result]
    var status : String = ""
    
    struct Result : Decodable
    {
        var address_components : [AddressComponent]
        var formatted_address : String = ""
        var geometry : Geometry?
        var place_id : String = ""
        var types : [String]
        
        struct AddressComponent : Decodable
        {
            var long_name : String = ""
            var short_name : String = ""
            var types : [String]
        }
        
        struct Geometry : Decodable
        {
            var bounds : Bounds?
            var location : Location?
            var location_type : String = ""
            var viewport : Viewport?
            
            struct Bounds : Decodable
            {
                var northeast : Northeast
                var southwest : Southwest
                
                struct Northeast : Decodable
                {
                    var lat : Float = 0.0
                    var lng : Float = 0.0
                }
                
                struct Southwest : Decodable
                {
                    var lat : Float = 0.0
                    var lng : Float = 0.0
                }
            }
            
            struct Location : Decodable
            {
                var lat : Float = 0.0
                var lng : Float = 0.0
            }
            
            struct Viewport : Decodable
            {
                var northeast : Northeast_?
                var southwest : Southwest_?
                
                struct Northeast_  : Decodable
                {
                    var lat : Float = 0.0
                    var lng : Float = 0.0
                }
                
                struct Southwest_ : Decodable
                {
                    var lat : Float = 0.0
                    var lng : Float = 0.0
                }
            }
        }
    }
    
}
