//
//  User.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 26/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import Foundation

struct User: Decodable
{
    var _id : String!
    var phone : String!
    var password : String!
    var email : String!
    var last_name : String!
    var first_name : String!
    var __v : Int64!
    var profile_pic : String!
    var trip_id : String!
    var auth_key : String!
    var rating : String!
    var service_status : String!
    var status : Bool!
    var latitude : Double!
    var longitude : Double!
    var address : String!
    var verified : Bool!
    var code : String!

}
