//
//  ServicesModel.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 23/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import Foundation

struct ServicesModel : Decodable
{
    var _id : String! =  nil
    var service_name : String! =  nil
    var service_image : String! =  nil
    var service_detail : String! =  nil
    var service_price : String! =  nil
    init(){}
    
}
