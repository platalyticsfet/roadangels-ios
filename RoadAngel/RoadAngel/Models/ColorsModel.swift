//
//  ColorsModel.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 23/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import Foundation

struct ColorsModel : Decodable
{
    public var colorId : Int?
    public var hexString : String = ""
    public var rgb : Rgb
    public var hsl : Hsl
    public var name : String = ""
    
    struct Rgb : Decodable {
        
        var r : Double?
        var g : Double?
        var b : Double?
    }
    
    struct Hsl  : Decodable{
        var h : Double?
        var s : Double?
        var l : Double?
    }
}
