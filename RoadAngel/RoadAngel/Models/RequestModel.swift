//
//  RequestModel.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 23/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import Foundation

struct RequestModel : Decodable
{
    var _id : String! =  nil
    var vendorsId : [String] =  []
    var request_status : Bool! =  true
    var request_time : String! =  nil
    var user_lng : String! =  nil
    var user_lat : String! =  nil
    var user_id : String! =  nil
    var address : String! =  nil
    var user : User! =  nil
    var distanceTime : String! =  nil
    var colorIndex : Int! =  nil
    var service_note : String! =  nil
    var service : ServicesModel! =  nil
    init(){}
}
