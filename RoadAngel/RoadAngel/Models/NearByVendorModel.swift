//
//  NearByVendorModel.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 23/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import Foundation

struct NearByVendorModel : Decodable
{
    var __v : Int!
    var _id : String!
    var vendor_id : String!
    var latitude : String!
    var longitude : String!
    var address : String!
    
}
