//
//  TripModel.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 26/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import Foundation

struct TripModel : Codable
{
    // destination = vendor
    // source = user
    var __v : Int! = 0
    var vendor_name : String! = ""
    var service_id : String! = ""
    var service_name : String! = ""
    var service_price : String! = ""
    var _id : String! = ""
    var status : String! = ""
    
    var end_time : String! = ""
    var start_time : String! =  nil
    
    var destination_lat_lng : String! = ""
    var vendor_id : String! = ""
    var destination_address : String! = ""
    var vendor_profilepic : String! = ""
    
    var source_lat_lng : String! = ""
    var user_id : String! = ""
    var user_name : String! = ""
    var source_address : String! = ""
    var user_profilepic : String! = ""
    
    var service_icon : String! = ""
    
    var trip_path : String! = ""
    var vendor_rating : String! = ""
    var user_rating : String! = ""
    var auth_key : String! = ""
    var map_image : String! = ""
    
    init()
    {
        
    }
    
    init(dic :Dictionary<String, AnyObject>)
    {
        if dic.index(forKey: "__v") != nil
        {
            __v = dic["__v"] as! Int
        }
        if dic.index(forKey: "end_time") != nil
        {
            end_time = dic["end_time"] as! String
        }
        if dic.index(forKey: "start_time") != nil
        {
            start_time = dic["start_time"] as! String
        }
        if dic.index(forKey: "destination_lat_lng") != nil
        {
            destination_lat_lng = dic["destination_lat_lng"] as! String
        }
        if dic.index(forKey: "vendor_id") != nil
        {
            vendor_id = dic["vendor_id"] as! String
        }
        if dic.index(forKey: "destination_address") != nil
        {
            destination_address = dic["destination_address"] as! String
        }
        if dic.index(forKey: "vendor_name") != nil
        {
            vendor_name = dic["vendor_name"] as! String
        }
        if dic.index(forKey: "vendor_profilepic") != nil
        {
            vendor_profilepic = dic["vendor_profilepic"] as! String
        }
        if dic.index(forKey: "source_lat_lng") != nil
        {
            source_lat_lng = dic["source_lat_lng"] as! String
        }
        if dic.index(forKey: "user_id") != nil
        {
            user_id = dic["user_id"] as! String
        }
        if dic.index(forKey: "user_name") != nil
        {
            user_name = dic["user_name"] as! String
        }
        if dic.index(forKey: "source_address") != nil
        {
            source_address = dic["source_address"] as! String
        }
        if dic.index(forKey: "service_id") != nil
        {
            service_id = dic["service_id"] as! String
        }
        if dic.index(forKey: "service_name") != nil
        {
            service_name = dic["service_name"] as! String
        }
        if dic.index(forKey: "service_price") != nil
        {
            service_price = dic["service_price"] as! String
        }
        if dic.index(forKey: "service_icon") != nil
        {
            service_icon = dic["service_icon"] as! String
        }
        if dic.index(forKey: "_id") != nil
        {
            _id = dic["_id"] as! String
        }
        if dic.index(forKey: "user_profilepic") != nil
        {
            user_profilepic = dic["user_profilepic"] as! String
        }
        if dic.index(forKey: "status") != nil
        {
            status = dic["status"] as! String
        }
        if dic.index(forKey: "trip_path") != nil
        {
            trip_path = dic["trip_path"] as! String
        }
        if dic.index(forKey: "vendor_rating") != nil
        {
            vendor_rating = dic["vendor_rating"] as! String
        }
        if dic.index(forKey: "user_rating") != nil
        {
            user_rating = dic["user_rating"] as! String
        }
        if dic.index(forKey: "map_image") != nil
        {
            map_image = dic["map_image"] as! String
        }
    }
    
    func toJSON() -> [String: Any] {
        return [
            "__v": __v as Any,
            "end_time": end_time as Any,
            "start_time": start_time as Any,
            "destination_lat_lng": destination_lat_lng as Any,
            "vendor_id": vendor_id as Any,
            "destination_address": destination_address as Any,
            "vendor_name": vendor_name as Any,
            "vendor_profilepic": vendor_profilepic as Any,
            "source_lat_lng": source_lat_lng as Any,
            "user_id": user_id as Any,
            "user_name": user_name as Any,
            "source_address": source_address as Any,
            "service_id": service_id as Any,
            "service_name": service_name as Any,
            "service_price": service_price as Any,
            "service_icon": service_icon as Any,
            "_id": _id as Any,
            "user_profilepic": user_profilepic as Any,
            "status": status as Any,
            "trip_path": trip_path as Any,
            "vendor_rating": vendor_rating as Any,
            "user_rating": user_rating as Any,
            "auth_key": auth_key as Any,
            "map_image": map_image as Any
        ]
    }
}
