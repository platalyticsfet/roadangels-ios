//
//  LatLng.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 01/01/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import Foundation

class LatLng
{
    var latitude : Double!
    var longitude : Double!
    
    init (latitude : Double,longitude : Double)
    {
        self.latitude = latitude
        self.longitude = longitude
    }
}
