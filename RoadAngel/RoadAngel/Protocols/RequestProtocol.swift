//
//  RequestProtocol.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 02/03/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import Foundation

protocol RequestProtocol: class
{
    func acceptButtonTapped(message:String)
    func noteButtonTapped(message:String)
}
