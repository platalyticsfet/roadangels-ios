//
//  ResponceCallback.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 28/12/2017.
//  Copyright © 2017 Kamil Faheem. All rights reserved.
//

import Foundation

protocol ResponceCallback {
    func onSuccessResponce(obj : Any , resonseType : Int , apiName : String , status : Int) -> Void
    func onFailureResponce(obj : Any , resonseType : Int , apiName : String , status : Int) -> Void
}
