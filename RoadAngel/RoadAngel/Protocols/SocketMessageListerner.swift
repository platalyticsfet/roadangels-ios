//
//  SocketMessageListerner.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 01/01/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import Foundation

protocol SocketMessageListerner {
    func onGetMessageAck(event : String , objectType : Int , obj : Any) -> Void
}
