//
//  AlertDialogDelegate.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 04/01/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import Foundation

protocol AlertDialogDelegate: class
{
    func okButtonTapped(message:String)
}
