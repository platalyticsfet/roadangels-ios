//
//  ServiceAlertViewDelegate.swift
//  RoadAngel
//
//  Created by Kamil Faheem on 03/01/2018.
//  Copyright © 2018 Kamil Faheem. All rights reserved.
//

import Foundation

protocol ServiceDialogDelegate: class {
    func confirmButtonTapped( note : String)
    func cancelButtonTapped()
}
